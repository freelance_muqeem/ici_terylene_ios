//
//  UpcomingEventDetailViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/11/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class UpcomingEventDetailViewController: UIViewController {

    @IBOutlet weak var newsDescription: UITextView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var img: UIImageView!
    
//    @IBOutlet weak var shareImg: UIImageView!
   
    override func viewDidLoad() {
        super.viewDidLoad()

        address.text = BDefaults.instance.getEvents()[0].url
        newsDescription.text = BDefaults.instance.getEvents()[0].content
        eventTitle.text = BDefaults.instance.getEvents()[0].heading
        
         
         eventDate.text = " \(AppUtils.formateEvent(date:  BDefaults.instance.getEvents()[0].eventDate))\n \(BDefaults.instance.getEvents()[0].address ?? "")"
        img.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(BDefaults.instance.getEvents()[0].image!)"), placeholderImage: UIImage(named: "picture.png"))
        
//        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
//        shareImg.isUserInteractionEnabled = true
//        shareImg.addGestureRecognizer(tapGestureRecognizer)
        
        
        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
//        let tappedImage = tapGestureRecognizer.view as! UIImageView

        
        let text = "\(BDefaults.instance.getEvents()[0].heading!) \n \(AppUtils.formateEvent(date: BDefaults.instance.getEvents()[0].eventDate)) \n \(BDefaults.instance.getEvents()[0].content!)"
//        let image = UIImage(named: "Product")
//        let myWebsite = NSURL(string:"https://stackoverflow.com/users/4600136/mr-javed-multani?tab=profile")
        let shareAll = [text ]
        let activityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)

        
        
        
        // Your action
    }
    
    @IBAction func openUrl(_ sender: Any) {
        guard let url = URL(string: BDefaults.instance.getEvents()[0].url) else {
                  return //be safe
              }
              
              if #available(iOS 10.0, *) {
                  UIApplication.shared.open(url, options: [:], completionHandler: nil)
              } else {
                  UIApplication.shared.openURL(url)
              }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
