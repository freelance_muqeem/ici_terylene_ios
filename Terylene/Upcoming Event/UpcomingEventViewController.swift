//
//  UpcomingEventViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SDWebImage
class UpcomingEventViewController: UIViewController , UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            self.eventsView.register(UINib(nibName: "UpcomingTableViewCell", bundle: nil), forCellReuseIdentifier: "UpcomingTableViewCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingTableViewCell", for: indexPath) as! UpcomingTableViewCell
            
            cell.eventName.text = events[indexPath.row].heading
            
            cell.addressLbl.text = events[indexPath.row].address
            cell.dateLbl.text = AppUtils.formateEvent(date:  events[indexPath.row].eventDate)
            cell.iimageLbl.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(events[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
            cell.viewAll.isHidden = true
            cell.eventTitle.isHidden = true
            return cell
        }else{
            //            bottomCell
            let cell = tableView.dequeueReusableCell(withIdentifier: "bottomCell") as! UpcomingTop
            
            cell.eventName.text = events[indexPath.row].heading
            
            cell.addressLbl.text = events[indexPath.row].address
            cell.dateLbl.text = AppUtils.formateEvent(date:  events[indexPath.row].eventDate)
            cell.iimageLbl.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(events[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BDefaults.instance.saveEvents(user: events[indexPath.row])
        performSegue(withIdentifier: "openEventDetail", sender: self)
    }
    
    
    var events = [EventsDtoHse]()
    
    @IBOutlet weak var eventsView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        AppUtils.app.showActivityIndicatory(uiView: self.view)
        
        eventsView.delegate = self
        eventsView.dataSource = self
        NetworkManager.getEvents(completion: { (events) in
            
            AppUtils.app.hideActivityIndicatory()
            self.events = events.hse
            
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            
            
            
            
            self.events.sort{dateFormatter.date(from: $0.eventDate!)! > dateFormatter.date(from: $1.eventDate!)!
                
            }
            
            
            
            
            
            self.eventsView.reloadData()
            
            
        }) { (error) in
            AppUtils.app.hideActivityIndicatory()
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
