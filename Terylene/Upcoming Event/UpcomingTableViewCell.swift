//
//  UpcomingTableViewCell.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/11/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class UpcomingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var eventTitle: UILabel!
    @IBOutlet var iimageLbl: UIImageView!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var eventName: UILabel!
    
    @IBOutlet weak var viewAll: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class UpcomingTop : UITableViewCell{
    @IBOutlet var iimageLbl: UIImageView!
    @IBOutlet var addressLbl: UILabel!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var eventName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
