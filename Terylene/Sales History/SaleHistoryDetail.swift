//
//  SaleHistoryDetail.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 19/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
import AEXML
import RTWebService
import Alamofire

import DropDown

class SaleHistoryDetail: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    
    @IBOutlet weak var saleView: UITableView!
    
    var companies = [Company]()
    var headers = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        saleView.delegate = self
        saleView.dataSource = self
        getData(id: 0)
        // Do any additional setup after loading the view.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func getData(id : Int)  {
        
        let soap = AEXMLDocument()
        let envelope = soap.addChild(name: "soapenv:Envelope",
                                     attributes: [ "xmlns:urn":"urn:sap-com:document:sap:soap:functions:mc-style",
                                                   "xmlns:soapenv":"http://schemas.xmlsoap.org/soap/envelope/"])
        
        //let header = envelope.addChild(name: "soap:Header")
        let body = envelope.addChild(name: "soapenv:Body")
        let geoIp = body.addChild(name:"urn:ZrfcCustomerSalesData")
        
        var month = 0;
        
        if AppUtils.companies[id].time == "one"{
            month = -1
        }
        else  if AppUtils.companies[id].time == "six"{
            month = -6
        } else if AppUtils.companies[id].time == "year"{
            month = -12
        }
        
        geoIp.addChild(name: "CustomerCode", value: "\(AppUtils.companies[id].code)", attributes: [:])
        geoIp.addChild(name: "FromDate", value: "\(AppUtils.addMonth(months: month))", attributes: [:])
        geoIp.addChild(name: "ToDate", value: "\(AppUtils.addMonth(months: 0))", attributes: [:])
        
        
        
        let soapPayload = RTPayload(parameter: ["soapdata" : soap.xml], parameterEncoding: .defaultUrl)
        let req1 = RTRequest.init(requestUrl: "http://icir3p1.ici.com.pk:8000/sap/bc/srt/rfc/sap/zrfc_customer_sales_data/100/zrfc_customer_sales_data/zrfc_customer_sales_data",
                                  requestMethod: .post,
                                  header: ["language":"en",
                                           "Authorization":"Basic Y3JtYXBwOmFiY2RlZjEyIw==",
                                           "Content-Type": "text/xml"],
                                  payload: soapPayload)
        
        
        
        
        
        Alamofire.request(req1.requestUrl,
                          method: req1.requestMethod,
                          parameters: req1.requestMethod == .get ? req1.payload?.parameter : [:],
                          encoding: req1.requestMethod == .get ? (req1.payload?.parameterEncoding)! : req1.payload?.parameter?["soapdata"] as! String,
                          headers: req1.header)
            .debugLog()
            .responseString{ (response) in
                //response.debugLog()
                AppUtils.app.hideActivityIndicatory()
                
                switch response.result {
                    
                    
                    
                    
                case .success:
                    //                    print(response)
                    
                    do{
                        let xml = try AEXMLDocument(xml: response.result.value! )
                        //                                            print(xml)
                        
                        
                        AppUtils.companies[id].sales = [SaleModel]()
                        
                        
                        if    let customers = xml.root["soap-env:Body"]["n0:ZrfcCustomerSalesDataResponse"]["CustomerData"]["item"].all{
                            
                            
                            for child in customers {
                                
                                //
                                
                                let c = SaleModel()
                                
                                
                                c.CustomerName = child["CustomerName"].string
                                
                                
                                c.BillingDate = child["BillingDate"].string
                                c.ProductName = child["ProductName"].string
                                c.BilledQuantity =  child["BilledQuantity"].double ?? 0
                                c.SalesUnit = child["SalesUnit"].string
                                c.ProductSegment = child["ProductSegment"].string
                                
                                if (c.SalesUnit == "KG") {
                                    c.BilledQuantity = Double(c.BilledQuantity/1000)
                                    
                                    
                                } else if (c.SalesUnit == "LBS") {
                                    
                                    
                                    c.BilledQuantity = Double(c.BilledQuantity * 0.00045359237)
                                    
                                    
                                }else{
                                    
                                }
                                
                                
                                
                                AppUtils.companies[id].sales.append(c)
                            }
                            
                            
                            var dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "yyyy-MM-dd"
                            
                            AppUtils.companies[id].sales.sort{dateFormatter.date(from: $0.BillingDate)! < dateFormatter.date(from: $1.BillingDate)!
                                
                            }
                            
                            
                            for  a in AppUtils.companies[id].sales{
                                
                                if AppUtils.companies[id].headers.count == 0{
                                    
                                    AppUtils.companies[id].headers.append(a.ProductSegment)
                                    
                                }else{
                                    
                                    if !AppUtils.companies[id].headers.contains(a.ProductSegment){
                                        AppUtils.companies[id].headers.append(a.ProductSegment)
                                    }
                                    
                                    
                                }
                                
                                
                                
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                        }
                        
                    }catch{
                        
                    }
                    
                    
                    
                    break
                    
                case .failure(let error):
                    
                    break
                default:
                    print(response)
                }
                
                
                if (id < AppUtils.companies.count-1) {
                    self.getData(id: id + 1);
                } else {
                    self.setUpData()
                }
                
                
                
                
        }
        
        
        
        
    }
    
    
    
    
    
    func setUpData(){
        
        
        
        
        
        
        
        
        for comp in AppUtils.companies{
            
            for header in  comp.headers{
                
                var data = CompanyData()
                data.heading = header
                
                var sales = [SaleModel]()

                if comp.time != "one"{
                    
                    var a = 0
                    for s in comp.sales{
                        
                        if header.elementsEqual(s.ProductSegment)
                        {
                            
                            if a == 0{
                                
                                s.BillingDate = AppUtils.getMonth(date: s.BillingDate)
                                sales.append(s)
                            }else{
                                var f = true
                                for d in sales{
                                    
                                    if d.BillingDate == AppUtils.getMonth(date: s.BillingDate){
                                        d.BilledQuantity += s.BilledQuantity
                                        f = false
                                        break
                                    }
                                }
                                if f{
                                    s.BillingDate = AppUtils.getMonth(date: s.BillingDate)
                                    sales.append(s)
                                }
                            }
                            a += 1
                        }
    
                    }
                }else{
                    
                    for s in comp.sales{
                        
                        if header.elementsEqual(s.ProductSegment)
                        {
                            
                            sales.append(s)
                            
                            
                        }

                    }

                }
                data.sales = sales
                comp.data.append( data)
            }
        }
        
        
       self.saleView.reloadData()
    }
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppUtils.companies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaleDetailCell") as! SaleDetailCell
        
  
        
       
        
        
        if AppUtils.companies[indexPath.row].headers.count == 0{
            cell.dropBtn.isHidden = true
        }else{
            
            cell.dropBtn.isHidden = false
            cell.dropBtn.layer.borderWidth = 2
            cell.dropBtn.layer.cornerRadius = 5
            cell.dropBtn.layer.borderColor = AppUtils.darkGrey

        }
        
        self.setUpChart(chart: cell.cChart)

        
        
        cell.dropTapped = { (button:UIButton) -> Void in
            
            let dropDown = DropDown()
            
            // The view to which the drop down will appear on
            dropDown.anchorView = cell.dropBtn // UIView or UIBarButtonItem in your case that menu button
            // The list of items to display. Can be changed dynamically
            dropDown.dataSource = AppUtils.companies[indexPath.row].headers
            dropDown.customCellConfiguration = {(index: Index, item: String, cell: DropDownCell) -> Void in
                if AppUtils.companies[indexPath.row].headers[index] == ""{
                cell.optionLabel.text = "Polyester"
                }else{
                
                cell.optionLabel.text = AppUtils.companies[indexPath.row].headers[index]
                }
            }
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                if item == ""{
                cell.dropBtn.setTitle("Polyester", for: .normal)
                }
                self.setUp(index: indexPath.row, head: AppUtils.companies[indexPath.row].headers[index], cChart: cell.cChart)
                print("Selected item: \(item) at index: \(index)")
            }
            dropDown.show()
        }
        cell.cName.text = AppUtils.companies[indexPath.row].name
        cell.cAddress.text =  AppUtils.companies[indexPath.row].address
        cell.cCode.text = "Customer Code : \(AppUtils.companies[indexPath.row].code)"
        
        if AppUtils.companies[indexPath.row].headers.count > 0{
            
        
        setUp(index: indexPath.row, head: AppUtils.companies[indexPath.row].headers[0], cChart: cell.cChart)
       
            if AppUtils.companies[indexPath.row].headers[0] == ""{
            cell.dropBtn.setTitle("Polyester", for: .normal)
            }
            else{ cell.dropBtn.setTitle(AppUtils.companies[indexPath.row].headers[0], for: .normal)
            }
            
        }
        
        //                cell.cChart.xAxis.labelCount = AppUtils.companies[indexPath.row].sales.count
        
        
        cell.cChart.cornerRadius = 20
        //        cell.cChart.backgroundColor = UIColor.white
        return cell
    }
    
    func setUp(index : Int ,head : String , cChart : LineChartView )  {
        
        for h in  AppUtils.companies[index].data{
            
            
            if h.heading == head  {
                
               setData(index: index, sales: h.sales, cChart: cChart)
                
                
            }
            
        }
        
        
    }
    
    
    func setData( index : Int , sales: [SaleModel] , cChart : LineChartView )  {
        
        let values = (0..<sales.count).map { (i) -> ChartDataEntry in
            var val = 0.0
            val = Double(sales[i].BilledQuantity)
            
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        
      
        
        
        self.setData(index: index,sales: sales,values: values, chart: cChart)
        
        
    }
    
    
    
    
    
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        
        
    }
    
    
    func setUpChart(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        
        chart.setScaleEnabled(true)
        
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        xAxis.granularityEnabled = true
        xAxis.granularity = 1.0
        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
        
        
        
        
        
        
        
        
    }
    func setData(index : Int , sales: [SaleModel] , values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        //        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 1.5
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        
        
        if AppUtils.companies[index].time == "one"{
            
            let formatter = SaleFormatter( v:  sales )
            
            chart.xAxis.valueFormatter = formatter
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formatter)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
        }else{
            
            
            let formatter = SaleFormatter2( v:  sales )
            
            chart.xAxis.valueFormatter = formatter
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formatter)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
            
        }
        
        
        
        
        chart.data = data
    }
    
    
    
    
}
