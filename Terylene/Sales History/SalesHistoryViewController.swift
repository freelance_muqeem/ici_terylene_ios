//
//  SalesHistoryViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/8/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import AEXML
import RTWebService
import Alamofire

class SalesHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate {
    
    
    
    
    @IBOutlet weak var errorView: UIView!
    var companies = [Company]()
    var filtered =  [Company]()
    
  
    
    
    @IBOutlet weak var searchB: UISearchBar!
    
    
    @IBOutlet weak var customersView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if BDefaults.instance.getUsers()[0].userType == "international user"{
            
            searchB.isHidden = true
            
        }else{
        errorView.isHidden = true
        customersView.delegate = self
        customersView.dataSource = self
        searchB.delegate = self
        //international user
        customersView.keyboardDismissMode = .onDrag
        searchB.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        AppUtils.app.showActivityIndicatory(uiView: self.view)
        getData()
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    @IBAction func search(_ sender: Any) {
        
        AppUtils.companies = [Company]()
        
        
        for c in self.filtered{
            
            if c.time == ""{
                
            }else{
                
                AppUtils.companies.append(c)
                
                
            }
            
            
            
        }
        
        
        
        if AppUtils.companies.count > 0{
            
            performSegue(withIdentifier: "openSaleHistoryDetail", sender: self)
            
            
        }
        
        
        
    }
    
    
    
    func getData()  {
        
        var mobile = " "
        if BDefaults.instance.getUsers()[0].userType != "employee"{
            mobile = "\(BDefaults.instance.getUsers()[0].userPhone!)"
        }
        
        
        
        
        let soap = AEXMLDocument()
        let envelope = soap.addChild(name: "soapenv:Envelope",
                                     attributes: [ "xmlns:urn":"urn:sap-com:document:sap:soap:functions:mc-style",
                                                   "xmlns:soapenv":"http://schemas.xmlsoap.org/soap/envelope/"])
        
        let body = envelope.addChild(name: "soapenv:Body")
        let geoIp = body.addChild(name:"urn:ZrfcCustomerSales")
        geoIp.addChild(name: "MobileNo", value: mobile, attributes: [:])
        
        
        
        
        let soapPayload = RTPayload(parameter: ["soapdata" : soap.xml], parameterEncoding: .defaultUrl)
        let req1 = RTRequest.init(requestUrl: "http://icir3p1.ici.com.pk:8000/sap/bc/srt/rfc/sap/zrfc_customer_sales/100/zrfc_customer_sales/zrfc_customer_sales",
                                  requestMethod: .post,
                                  header: ["language":"en",
                                           "Authorization":"Basic Y3JtYXBwOmFiY2RlZjEyIw==",
                                           "Content-Type": "text/xml"],
                                  payload: soapPayload)
        
        
        
        
        
        Alamofire.request(req1.requestUrl,
                          method: req1.requestMethod,
                          parameters: req1.requestMethod == .get ? req1.payload?.parameter : [:],
                          encoding: req1.requestMethod == .get ? (req1.payload?.parameterEncoding)! : req1.payload?.parameter?["soapdata"] as! String,
                          headers: req1.header)
            .debugLog()
            .responseString{ (response) in
                //response.debugLog()
                AppUtils.app.hideActivityIndicatory()
                
                switch response.result {
                    
                    
                    
                    
                case .success:
                    //                    print(response)
                    
                    do{
                        let xml = try AEXMLDocument(xml: response.result.value! )
                        //                                            print(xml)
                        
                        if    let customers = xml.root["soap-env:Body"]["n0:ZrfcCustomerSalesResponse"]["CustomerData"]["item"].all{
                            
                            
                            for child in customers {
                                
                                //
                                
                                let c = Company()
                                
                                
                                c.name = child["CustomerName"].string
                                c.code = child["CustomerCode"].string
                                c.address = child["CustomerAddress"].string
                                
                                self.companies.append(c)
                            }
                            self.filtered = self.companies
                            self.customersView.reloadData()
                        }
                        
                    }catch{
                        
                    }
                    
                    
                    
                    break
                    
                case .failure(let error):
                    
                    break
                default:
                    print(response)
                }
                
                
        }
        
        
        
        
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    func Unselect(b : UIButton)  {
        //        b.cornerRadius = 20
        b.layer.cornerRadius = 15
        b.layer.borderColor = AppUtils.lightColor
        b.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        b.layer.borderWidth = 1
        
        b.setTitleColor(UIColor.black, for: .normal)
        
        
    }
    func select(b : UIButton)  {
        //        b.cornerRadius = 20
        b.layer.cornerRadius = 15
        b.layer.backgroundColor = AppUtils.lightColor
        b.setTitleColor(UIColor.white, for: .normal)
        b.layer.borderWidth = 1
    }
    
    
    @IBAction func reset(_ sender: Any) {
       resetaa()
    }
    //Mark :- search method
    
    func searchFrom(name : String)  {
        var c = [Company]()

        for item in filtered {
            
            if item.name.localizedCaseInsensitiveContains(name){
                
                c.append(item)
                
                
            }
            
            
        }
        
        filtered = c
        self.customersView.reloadData()
        
        
        
    }
    
    
    func resetaa(){
        self.filtered = [Company]()
        self.customersView.reloadData()
        
        for item in companies {
            
            
            
            item.opened = false
            item.time = ""
            
            
            
            
            
        }
        
        
        
        self.filtered = self.companies
        
        
        
        
        
        self.customersView.reloadData()
    }
    
    
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
      resetaa()
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == ""{
            self.filtered = self.companies
            self.customersView.reloadData()
        }else{
            self.searchFrom(name: searchBar.text!)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        if searchBar.text == ""{
            self.filtered = self.companies
            self.customersView.reloadData()
        }else{
            self.searchFrom(name: searchBar.text!)
        }    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if searchBar.text == ""{
            self.filtered = self.companies
            self.customersView.reloadData()
        }else{
            self.searchFrom(name: searchBar.text!)
        }
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        if searchBar.text == ""{
            self.filtered = self.companies
            self.customersView.reloadData()
        }else{
            self.searchFrom(name: searchBar.text!)
        }
    }
    
    
    
    
    
}
extension SalesHistoryViewController{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count+1;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        //asas
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "asas")!
            return cell
        }
            
        else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "customersCell") as! CustomerCell
            
            cell.customerCode.text = "\(filtered[indexPath.row-1].code)"
            cell.address.text = filtered[indexPath.row-1].address
            cell.title.text = filtered[indexPath.row-1].name
            
            if !filtered[indexPath.row-1].opened{
                
                self.filtered[indexPath.row-1].time = ""
                cell.outerHeight.constant = 0
                cell.outerView.isHidden = true
                cell.expand.setImage(UIImage(named: "down_blue")!, for: .normal)
            }else{
                
                cell.outerHeight.constant = 90
                cell.outerView.isHidden = false
                cell.expand.setImage(UIImage(named: "up_blue")!, for: .normal)
                
            }
            
            
            
            
            
            if self.filtered[indexPath.row-1].time == ""{
                Unselect(b: cell.oneMonth)
                Unselect(b: cell.sixMonth)
                Unselect(b: cell.year)
            }
            else if self.filtered[indexPath.row-1].time == "one"{
                select(b: cell.oneMonth)
                Unselect(b: cell.sixMonth)
                Unselect(b: cell.year)
            }else if self.filtered[indexPath.row-1].time == "six"{
                Unselect(b: cell.oneMonth)
                select(b: cell.sixMonth)
                Unselect(b: cell.year)
            }
            else if self.filtered[indexPath.row-1].time == "year"{
                Unselect(b: cell.oneMonth)
                select(b: cell.year)
                Unselect(b: cell.sixMonth)
            }
            
            
            
            
            cell.oneMonthButtonTapped = { (button:UIButton) -> Void in
                
                self.filtered[indexPath.row-1].time = "one"
                self.select(b: cell.oneMonth)
                self.Unselect(b: cell.sixMonth)
                self.Unselect(b: cell.year)
            }
            
            cell.sixMonthButtonTapped = { (button:UIButton) -> Void in
                
                self.filtered[indexPath.row-1].time = "six"
                self.select(b: cell.sixMonth)
                self.Unselect(b: cell.oneMonth)
                self.Unselect(b: cell.year)
                
            }
            
            cell.yearButtonTapped = { (button:UIButton) -> Void in
                
                self.filtered[indexPath.row-1].time = "year"
                self.select(b: cell.year)
                self.Unselect(b: cell.sixMonth)
                self.Unselect(b: cell.oneMonth)
                
            }
            
            
            
            
            cell.arrowButtonTapped = { (button:UIButton) -> Void in
                
                self.filtered[indexPath.row-1].opened = !self.filtered[indexPath.row-1].opened
                self.customersView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.fade)
                
                
            }
            
            
            
            return cell
        }
    }
    
    @objc func addOneMonth(sender: UITapGestureRecognizer) {
        
    }
    
}
