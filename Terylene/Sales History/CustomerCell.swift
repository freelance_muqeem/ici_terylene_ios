//
//  CustomerCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 05/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class CustomerCell: UITableViewCell {

    @IBOutlet weak var customerCode: UILabel!
    @IBOutlet weak var outerHeight: NSLayoutConstraint!
    
    @IBOutlet weak var outerView: UIView!
    
    
    
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var expand: UIButton!
    @IBOutlet weak var oneMonth: UIButton!
    @IBOutlet weak var sixMonth: UIButton!
    @IBOutlet weak var year: UIButton!
    
    
    typealias arrowButtonTappedBlock = (_ expand:UIButton) -> Void
    var arrowButtonTapped : arrowButtonTappedBlock!

   
    typealias oneMonthButton = (_ oneMonth:UIButton) -> Void
    var oneMonthButtonTapped : oneMonthButton!
    
   
    typealias sixMonthButton = (_ sixMonth:UIButton) -> Void
    var sixMonthButtonTapped : sixMonthButton!
   
    
    typealias yearButton = (_ year:UIButton) -> Void
    var yearButtonTapped : yearButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func openExpand(_ sender: Any) {
        if arrowButtonTapped != nil {
            arrowButtonTapped(sender as! UIButton)
        }
    }
    
    @IBAction func oneMonthOPen(_ sender: Any) {
        
        if oneMonthButtonTapped != nil {
            oneMonthButtonTapped(sender as! UIButton)
        }
        
        
    }
    
    
    @IBAction func sixMonthOPen(_ sender: Any) {
        if sixMonthButtonTapped != nil {
            sixMonthButtonTapped(sender as! UIButton)
        }
    }
    
    
    @IBAction func yearOPen(_ sender: Any) {
        if yearButtonTapped != nil {
            yearButtonTapped(sender as! UIButton)
        }
    }
    
    
}
