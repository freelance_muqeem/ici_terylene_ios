//
//  Companies.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 18/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
class Company {
    
    var time = ""
    var opened = false
    var code = ""
    var name = ""
    var address = ""
    var sales = [SaleModel]()
    var headers = [String]()

    var data = [CompanyData]()
}

class CompanyData{
    
    var heading = ""
    var sales = [SaleModel]()

}
