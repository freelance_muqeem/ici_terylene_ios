//
//  Sale.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 19/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
class SaleModel  {
    
    
    public var CustomerCode = 0;
    
    public var BilledQuantity = 0.0 ;
    
    
    public  var CustomerName = "";
    
    
    public  var ProductName = "";
    
    
    public  var ProductSegment = "";
    
    
    public  var BillingDate = "";
    
    public  var SalesUnit = "";
    
    
}
