//
//  SaleDetailCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 19/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts

class SaleDetailCell: UITableViewCell {
    
    @IBOutlet weak var cChart: LineChartView!
    @IBOutlet weak var cAddress: UILabel!
    @IBOutlet weak var cCode: UILabel!
    @IBOutlet weak var cName: UILabel!
    
    
    @IBOutlet weak var dropBtn: UIButton!
    
    typealias dropBtnTapped = (_ expand:UIButton) -> Void
    var dropTapped : dropBtnTapped!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func openDrop(_ sender: Any) {
        
        if dropTapped != nil {
            dropTapped(sender as! UIButton)
        }
        
    }
}
