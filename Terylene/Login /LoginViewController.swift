//
//  LoginViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/21/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import SwiftyJSON


class LoginViewController: UIViewController {
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    var loginmodel = LoginModel ()
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var emailtext: HoshiTextField!
    @IBOutlet var passwordText: HoshiTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        loginBtn.layer.cornerRadius = 10
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor.black.cgColor

        
        emailtext.returnKeyType = UIReturnKeyType.done
        passwordText.returnKeyType = UIReturnKeyType.done
        passwordText.isSecureTextEntry = true
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    ///  call Forgot Screen
    @IBAction func forgotPasswordBtn(_ sender: Any) {
        let alert = UIAlertController(title: "Forgot Password", message: "Please Enter your registered email", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Enter email"
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0] // Force unwrapping because we know it exists.
            print("Text field: \(textField!.text)")
     
        
        
            NetworkManager.forgotPassword(email: textField!.text!, completion: { (success) in
                
                 self.present(AppUtils.returnToast(string: "Check your email we have reset your password"), animated: true)
                
                
                
            }, failure: { (err) in
                self.present(AppUtils.returnErrorToast(string: "Network Error"), animated: true)
            })
        
        
        
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { [weak alert] (_) in
            
        }))

        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    
    ///  Func Call to main Screen
//    
//    func callHomeScreen()  {
//        
//        let swvc = storyboard?.instantiateViewController(withIdentifier: "SWRevealViewController") as! UIViewController
//        self.present(swvc, animated: true, completion: nil)
//        
//    }
    
    
    /// Login Btn
    
    @IBAction func loginBtn(_ sender: Any) {
        
        if(check())
        {
            appdelegate.showActivityIndicatory(uiView: self.view)
            
            NetworkManager.login(username: emailtext.text!, password: passwordText.text!, completion: { (b) in
                
                self.appdelegate.hideActivityIndicatory()
                
                if(b){
                    
//                    AppUtils.app.refresh()
                    
                    
                   AppUtils.app.startMainScreen()
                    
                    
                }
                
            }) { (s) in
                self.appdelegate.hideActivityIndicatory()
                let toast = AppUtils.returnErrorToast(string: s)
                self.present(toast, animated: true)
                
                
            }
        }
        
        
        
       // appdelegate.showActivityIndicatory(uiView: self.view)
       // fetchLoginData()
        // self.callHomeScreen()
        
        
        
    }
    
    
    func check() -> Bool {
        if(emailtext.text == nil && emailtext.text == ""){
         
            let toast = AppUtils.returnErrorToast(string: "Enter Email")
            present(toast, animated: true)
            
            return false
        }
        if(passwordText.text == nil && passwordText.text == ""){
            
            let toast = AppUtils.returnErrorToast(string: "Enter Password")
            present(toast, animated: true)
            
            return false
        }
        
        return true
    }
    
    
    
    @IBAction func cancelBtn(_ sender: Any) {
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    /// Function to chhange Keyboard Button in Done
    
    func textView(textView:LoginViewController, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            textView.resignFirstResponder()
        }
        return true
    }
    
    
    /// Sign Up Btn

    @IBAction func signUpBtn(_ sender: Any) {
    
        let signupvc = storyboard?.instantiateViewController(withIdentifier: "SignupViewController") as! UIViewController
            self.present(signupvc, animated: true, completion: nil)
    }
}



//extension LoginViewController {
//
//
//    func  fetchLoginData () {
//
//
//        let header = ["Client_Service": "AppSkNoCkQr-#$%",
//                      "Auth_Key":"ItIsfoRItIsfoR^S@fTy!@).(@^S@fTy!@).(@"
//        ]
//
//
//        let parameter =  ["email": emailtext.text! ,
//                          "password" : passwordText.text!
//        ]
//
//
//        Alamofire.request("https://half-tech.com/terelyne/Login", method: .post, parameters: parameter as Parameters, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
//            self.appdelegate.hideActivityIndicatory()
//
//            switch response.result {
//
//            case .success(_):
//                print(response)
//                let jsonResponse = JSON(response.result.value!)
//                self.loginmodel = LoginModel(response:jsonResponse)
//                if self.loginmodel.status == "True"{
//
//
//
                //     self.callHomeScreen()
//
//                } else {
//
//                    //show Alter
//                    let alertController = UIAlertController(title: "Alert", message:self.loginmodel.message
//                        , preferredStyle: UIAlertController.Style.alert)
//                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
//                    self.present(alertController, animated: true, completion: nil)
//
//
//                }
//            case .failure(let error):
//
//                    self.alertMessage(userMessage: error.localizedDescription)
//            }
//
//
//        }
//    }
//}

