//
//  ViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/20/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import ImageSlideshow


class WelcomeViewController: UIViewController {
    
    open override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var titleLbl: UILabel!
   @IBOutlet var slideshow: ImageSlideshow!
    
    let localSource = [ImageSource(imageString: "welcomeScreen1")!, ImageSource(imageString: "welcomeScreen2")]
    
    
    let titleArr = [ "Connect with Terylene" , "Grow Your Busines" ]
    
    
    override func viewDidLoad() {
        
        slideshow.currentPageChanged = { page in
           self.titleLbl.text = self.titleArr[page]
        }
    
     
        
        super.viewDidLoad()
        
        loginBtn.layer.cornerRadius = 10
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor.black.cgColor
        

        slideshow.slideshowInterval = 5.0
        slideshow.pageIndicatorPosition = .init(horizontal: .center, vertical: .under)
        slideshow.contentScaleMode = UIView.ContentMode.scaleAspectFill
        
        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.blue
        slideshow.pageIndicator = pageControl
        
       
    
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        slideshow.activityIndicator = DefaultActivityIndicator()
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        slideshow.setImageInputs(localSource as! [InputSource])
    
      // Do any additional setup after loading the view, typically from a nib.
    }
    
    
    
    
    
    @IBAction func loginBtn(_ sender: Any) {
        
        //self.present("LoginViewController", animated: true, completion: nil)
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! UIViewController
          self.present(vc, animated: true, completion: nil)
    }

    
    
    
    
}







