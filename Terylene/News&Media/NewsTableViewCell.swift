//
//  NewsTableViewCell.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/11/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class NewsTableViewCell: UITableViewCell {

    @IBOutlet var newsTitleLbl: UILabel!
    @IBOutlet var urlLbl: UILabel!
    @IBOutlet var newsImg: UIImageView!

    @IBOutlet var dateLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}




class TopNewsCell: UITableViewCell {
    
    @IBOutlet var newsTitleLbl: UILabel!
       @IBOutlet var newsImg: UIImageView!
    @IBOutlet var dateLbl: UILabel!
    @IBOutlet var urlLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
