//
//  NewsDetailViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/11/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class NewsDetailViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var url: UIButton!
    @IBOutlet weak var newsDetail: UILabel!
    @IBOutlet weak var dateNews: UILabel!
    @IBOutlet weak var newsTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

      
        url.setTitle(BDefaults.instance.getNews()[0].url, for: .normal)
        
       
        img.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(BDefaults.instance.getNews()[0].image!)"), placeholderImage: UIImage(named: "picture.png"))
        
        
        
        
        
        
        
        newsDetail.text = BDefaults.instance.getNews()[0].descriptionField
        dateNews.text = AppUtils.formateDate(date:BDefaults.instance.getNews()[0].createdAt!)
        newsTitle.text = BDefaults.instance.getNews()[0].heading
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func openUrl(_ sender: Any) {
        guard let url = URL(string: BDefaults.instance.getNews()[0].url) else {
            return //be safe
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }    }
}
