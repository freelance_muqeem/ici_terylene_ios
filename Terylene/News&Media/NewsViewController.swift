//
//  NewsViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class NewsViewController: UIViewController {
    
    var newsID =  ""
    var heading = ""
    var descriptionLbl = ""
    var createdDate = ""
    @IBOutlet var tableView: UITableView!
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    var newsmodel = NewsModel ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print (newsID)
        print (heading)
        print (descriptionLbl)
        print(createdDate)
        appdelegate.showActivityIndicatory(uiView: self.view)
        fetchNewsData()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    func callNewsdetailView() {
    
    let detailVC = NewsDetailViewController()
    self.navigationController?.pushViewController(detailVC, animated: true)
    
}
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
// Json Parsing Code in Swift


extension NewsViewController {
    
    
    func  fetchNewsData () {
        
        
     


        Alamofire.request("\(AppUtils.BASE_URL)/news",  method: .get
            , parameters: nil, encoding: URLEncoding.default, headers: AppUtils.headers).responseJSON { (response) in
                print(response)
            self.appdelegate.hideActivityIndicatory()
            
            switch response.result {
            
            case .success(_):
                print(response)
                let jsonResponse = JSON(response.result.value!)
                self.newsmodel = NewsModel(response:jsonResponse)
                if self.newsmodel.status == "True"{
                    
                    
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    
                    
                    
                    
                    self.newsmodel.news.sort{dateFormatter.date(from: $0.createdAt!)! > dateFormatter.date(from: $1.createdAt!)!
                        
                    }
                    
                    
                    
                    self.tableView.reloadData()
                    
                    
                } else {
                    
                    //show Alter
                    let alertController = UIAlertController(title: "Alert", message:self.newsmodel.message
                        , preferredStyle: UIAlertController.Style.alert)
                    alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default,handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                    
                }
            case .failure(let error):
                
                self.alertMessage(userMessage: error.localizedDescription)
            }
            
            
        }
    }
}








extension NewsViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
            return self.newsmodel.news.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        
        
        if indexPath.row == 0{
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "newsTable") as! NewsTableViewCell
            
            cell.dateLbl.text = AppUtils.formateDate(date:  newsmodel.news[indexPath.row].createdAt!)
            cell.newsTitleLbl.text = newsmodel.news[indexPath.row].heading
        
         cell.urlLbl.text = newsmodel.news[indexPath.row].url
        
        cell.newsImg.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(newsmodel.news[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
        
        
            return cell
        
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "bottomCell") as! TopNewsCell
            
            cell.dateLbl.text = AppUtils.formateDate(date:  newsmodel.news[indexPath.row].createdAt!)
            cell.newsTitleLbl.text = newsmodel.news[indexPath.row].heading
            
            cell.urlLbl.text = newsmodel.news[indexPath.row].url
            
            cell.newsImg.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(newsmodel.news[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
            
            
            return cell
        }
        
    }
    
    
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BDefaults.instance.saveNews(user: newsmodel.news[indexPath.row])
        performSegue(withIdentifier: "newsDetails", sender: self)
        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

