//
//  NewsModel.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/26/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON


class NewsModel {
    
    var status = ""
    var message = ""
    var  news = [News] ()
    
    init() {
        
        
        status = ""
        message = ""
        news = [News] ()
    }
    
    init(response : JSON) {
        self.status = response["status"].string ?? ""
        self.message = response["meassage"].string ?? ""
        for media in response["news"].arrayValue {
            news.append(News(fromJson: media))
            
        }
        
    }
}

class News  : NSObject, NSCoding{
    
    var createdAt : String!
    var descriptionField : String!
    var heading : String!
    var id : Int!
    var newDate : String!
    var status : String!
    var url : String!
    var image : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        heading = json["heading"].stringValue
        id = json["id"].intValue
        newDate = json["new_date"].stringValue
        image = json["image"].stringValue
        status = json["status"].stringValue
        url = json["url"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if heading != nil{
            dictionary["heading"] = heading
        }
        if id != nil{
            dictionary["id"] = id
        }
        if newDate != nil{
            dictionary["new_date"] = newDate
        }
        if status != nil{
            dictionary["status"] = status
        }
        if url != nil{
            dictionary["url"] = url
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        heading = aDecoder.decodeObject(forKey: "heading") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        newDate = aDecoder.decodeObject(forKey: "new_date") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
        image = aDecoder.decodeObject(forKey: "image") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if heading != nil{
            aCoder.encode(heading, forKey: "heading")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if newDate != nil{
            aCoder.encode(newDate, forKey: "new_date")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        
    }
    
}


