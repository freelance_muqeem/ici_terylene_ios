//
//  SignupModel.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/28/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class SignupModel {
    
    var status = ""
    var message = ""
    var parameters = [Parameter] ()
    
    init() {
        
        status = ""
        message = ""
        parameters = [Parameter] ()
    }
    
    init(response : JSON) {
        
        self.status = response["status"].string ?? ""
        self.message = response["meassage"].string ?? ""
        
        for para  in response["parameter"].arrayValue {
            parameters.append(Parameter(parameters: para))
        }
        
    }
}

class Parameter{
    
    var email = ""
    var password = ""
    var name = ""
    var phone = ""
    var company = ""
    
    init() {
       
    }
    
    init(parameters: JSON) {
        
        self.email = parameters["email"].string ?? "-1"
        self.password  = parameters["password"].stringValue
        self.name =  parameters["name"].stringValue
        self.phone = parameters["phone"].stringValue
        self.company = parameters["company"].stringValue
        
    }
    
}



    
  
    
    
    
    
    
    
    

