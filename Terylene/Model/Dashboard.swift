//
//  Dashboard.swift
//  Terylene
//
//  Created by Ali Ahsan on 5/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON

class DashboardModel {
    
    var status = ""
    var message = ""
    var  today = [TodayModel] ()
    var yesterday = [YesterdayModel] ()
    
    init() {
        
        
        status = ""
        message = ""
        today = [TodayModel] ()
        yesterday = [YesterdayModel] ()
    }
    
    init(response : JSON) {
        self.status = response["status"].string ?? ""
        self.message = response["meassage"].string ?? ""
        for todayUsd in response["today"].arrayValue {
            today.append(TodayModel(today:todayUsd))
        }
        
        for yesterdayUsd in response["yesterday"].arrayValue {
            yesterday.append(YesterdayModel(yesterday:yesterdayUsd))
        }
        
    }
    
    
}

class  TodayModel  {
    
    var today_usd  = ""
    var date = ""
    var today_cyn = ""
    var today_pta = ""
    var today_meg = ""
    var today_px = ""
    var today_psf_price = ""
    var today_cotton_maund = ""
    var today_nyf = ""
    var today_crude = ""
    var today_brent = ""
    var today_eur = ""
    var today_jpy = ""
    var today_gbp = ""
    var today_ici_semi = ""
    var today_import_officer = ""
    var today_fob = ""
    var today_nyf_maund = ""
    var today_nyf_pkr = ""
    var today_yarn_30_pc = ""
    var today_yarn_38_pc = ""
    var today_yarn_26_pc = ""
    var today_yarn_36_pc = ""
    var today_yarn_20_s = ""
    var today_yarn_50_pp = ""
    
    
    
    init() {
        // constructure
    }
    
    init(today: JSON) {
        
        self.today_usd = today["today_usd"].string ?? "-1"
        self.date  = today["date"].stringValue
        self.today_cyn =  today["today_cyn"].stringValue
        self.today_pta = today["today_pta"].stringValue
        self.today_meg = today["today_meg"].stringValue
        self.today_px = today["today_px"].stringValue
        self.today_psf_price = today["today_psf_price"].stringValue
        self.today_cotton_maund = today["today_cotton_maund"].stringValue
        self.today_nyf = today["today_nyf"].stringValue
        self.today_crude = today["today_crude"].stringValue
        self.today_brent = today["today_brent"].stringValue
        self.today_eur = today["today_eur"].stringValue
        self.today_jpy = today["today_jpy"].stringValue
        self.today_gbp = today["today_gbp"].stringValue
        self.today_ici_semi = today["today_ici_semi"].stringValue
        self.today_import_officer = today["today_import_officer"].stringValue
        self.today_fob = today["today_fob"].stringValue
        self.today_nyf_maund = today["today_nyf_maund"].stringValue
        self.today_nyf_pkr = today["today_nyf_pkr"].stringValue
        self.today_yarn_30_pc = today["today_yarn_30_pc"].stringValue
        self.today_yarn_38_pc = today["today_yarn_38_pc"].stringValue
        self.today_yarn_26_pc = today["today_yarn_26_pc"].stringValue
        self.today_yarn_36_pc = today["today_yarn_36_pc"].stringValue
        self.today_yarn_20_s = today["today_yarn_20_s"].stringValue
        self.today_yarn_50_pp = today["today_yarn_50_pp"].stringValue
        
    }
    
    
    
    
}


class  YesterdayModel {
    
    
    var yesterday_usd = ""
    var date = ""
    var yesterday_cyn = ""
    var yesterday_pta = ""
    var yesterday_meg = ""
    var yesterday_px = ""
    var yesterday_psf_price = ""
    var yesterday_cotton_maund = ""
    var yesterday_nyf = ""
    var yesterday_crude = ""
    var yesterday_brent = ""
    var yesterday_eur = ""
    var yesterday_jpy = ""
    var yesterday_gbp = ""
    var yesterday_ici_semi = ""
    var yesterday_import_officer = ""
    var yesterday_fob = ""
    var yesterday_nyf_maund = ""
    var yesterday_nyf_pkr = ""
    var yesterday_yarn_30_pc = ""
    var yesterday_yarn_38_pc = ""
    var yesterday_yarn_26_pc = ""
    var yesterday_yarn_36_pc = ""
    var yesterday_yarn_20_s = ""
    var yesterday_yarn_50_pp = ""
    
    
    
    init() {
        // constructure
    }
    
    init(yesterday: JSON) {
        
        self.yesterday_usd = yesterday["yesterday_usd"].string ?? "-1"
        self.date = yesterday["date"].stringValue
        self.yesterday_cyn = yesterday["yesterday_cyn"].stringValue
        self.yesterday_pta = yesterday["yesterday_pta"].stringValue
        self.yesterday_meg = yesterday["yesterday_meg"].stringValue
        self.yesterday_px = yesterday["yesterday_px"].stringValue
        self.yesterday_psf_price = yesterday["yesterday_psf_price"].stringValue
        self.yesterday_cotton_maund = yesterday["yesterday_cotton_maund"].stringValue
        self.yesterday_nyf = yesterday["yesterday_nyf"].stringValue
        self.yesterday_crude = yesterday["yesterday_crude"].stringValue
        self.yesterday_brent = yesterday["yesterday_brent"].stringValue
        self.yesterday_eur = yesterday["yesterday_eur"].stringValue
        self.yesterday_jpy = yesterday["yesterday_jpy"].stringValue
        self.yesterday_gbp = yesterday["yesterday_gbp"].stringValue
        self.yesterday_ici_semi = yesterday["yesterday_ici_semi"].stringValue
        self.yesterday_import_officer = yesterday["yesterday_import_officer"].stringValue
        self.yesterday_fob = yesterday["yesterday_fob"].stringValue
        self.yesterday_nyf_maund = yesterday["yesterday_nyf_maund"].stringValue
        self.yesterday_nyf_pkr = yesterday["yesterday_nyf_pkr"].stringValue
        self.yesterday_yarn_30_pc = yesterday["yesterday_yarn_30_pc"].stringValue
        self.yesterday_yarn_38_pc = yesterday["yesterday_yarn_38_pc"].stringValue
        self.yesterday_yarn_26_pc = yesterday["yesterday_yarn_26_pc"].stringValue
        self.yesterday_yarn_36_pc = yesterday["yesterday_yarn_36_pc"].stringValue
        self.yesterday_yarn_20_s = yesterday["yesterday_yarn_20_s"].stringValue
        self.yesterday_yarn_50_pp = yesterday["yesterday_yarn_50_pp"].stringValue
        
        
    }
    
    
    
}
