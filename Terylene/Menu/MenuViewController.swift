//
//  MenuViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/22/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate , UITableViewDataSource{
    
//    @IBOutlet weak var loginView: UIView!
    
    var menuNameArr: Array = [String]()
    var imageArr:Array = [UIImage]()
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet var tableView: UITableView!
//    @IBOutlet weak var email: UILabel!
//
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.tableView.backgroundColor  = .clear
        menuNameArr = [ "News", "Events", "Special Reports", "Contact Us", "Change Password"]
        
        
        imageArr = [UIImage(named: "newspaper")!, UIImage(named: "ic_event_white")!, UIImage(named: "ic_graph_white")!, UIImage(named: "ic_contact_white")!,UIImage(named: "ic_settings_white")!]
        
        
        emailLabel.text = BDefaults.instance.getUsers()[0].userEmail ?? ""
        
        // Do any additional setup after loading the view.
    }
    
    

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return   self.menuNameArr.count
    }
    
    
    
    @IBAction func logout(_ sender: Any) {
        
        
        
        var refreshAlert = UIAlertController(title: "Logout", message: "Are you sure you want to logout?", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            BDefaults.instance.setAppState(value: "")
            AppUtils.app.startLogin()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            print("Handle Cancel Logic here")
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
        
        
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell") as! MenuCell
        
        
        cell.menuIcon.image = imageArr[indexPath.row]
        cell.menuLbl.text! = menuNameArr[indexPath.row]
        cell.imgBg.cornerRadius = 18
        
                    return cell
        
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if menuNameArr[indexPath.row] ==  "Consumption Pattern"{
            self.performSegue(withIdentifier: "openSaleHistory", sender: self)
        }
        else if menuNameArr[indexPath.row] ==  "News" {
            
            self.performSegue(withIdentifier: "openNews", sender: self)
        }
        else if menuNameArr[indexPath.row] ==   "Events"{
            
            self.performSegue(withIdentifier: "openEvents", sender: self)
        }
        else if menuNameArr[indexPath.row] ==  "Product Catalogue "{
            
            self.performSegue(withIdentifier: "openProduct", sender: self)
        }
        else if menuNameArr[indexPath.row] ==  "Special Reports" {
            
            self.performSegue(withIdentifier: "openSpecialReports", sender: self)
        }
       
        else if menuNameArr[indexPath.row] == "Contact Us" {
            
            self.performSegue(withIdentifier: "openContactUs", sender: self)
            
        } else if menuNameArr[indexPath.row] == "Change Password" {
            
            self.performSegue(withIdentifier: "openSettings", sender: self)
            
        }
        
        
        
        
        
//        else {
//
//            self.performSegue(withIdentifier: "Home", sender: self)
//        }
}

   
}


