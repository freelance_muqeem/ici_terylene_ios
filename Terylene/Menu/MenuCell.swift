//
//  MenuTableViewCell.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/24/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    
    @IBOutlet var menuIcon: UIImageView!
    
    @IBOutlet var menuLbl: UILabel!
    @IBOutlet var imgBg: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
//    override func layoutSubviews() {
//        super.layoutSubviews()
//        
//        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 30, left: 10, bottom: 30, right: 10))
//    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    

}
