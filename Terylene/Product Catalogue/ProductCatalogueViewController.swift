//
//  ProductCatalogueViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/8/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import CenteredCollectionView
import Gemini
import MSPeekCollectionViewDelegateImplementation
class ProductCatalogueViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        BDefaults.instance.setInt(key: "productId", value: indexPath.row)
        performSegue(withIdentifier: "openProductDetail", sender: self)
        //
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productsCell") as! ProductsCell
        
        cell.title.text = self.products[indexPath.row].heading
        cell.desciption.text = self.products[indexPath.row].content
        cell.heading2.text = self.products[indexPath.row].descriptionField
        cell.img.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(self.products[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
        
        
        cell.img.layer.cornerRadius = 20
        
        //        self.productCollection.animateCell(cell)
        return cell
    }
    

    var products = [ProductDtoProduct]()
    @IBOutlet weak var productCollection: UITableView!
   let appdelegate = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

       products = BDefaults.instance.getProducts()
        
        
        if( products.count <= 0){
            self.getProducts()
        }
        
        
        self.productCollection.register(UINib(nibName: "ProductsCell", bundle: nil), forCellReuseIdentifier: "productsCell")
        
        
        productCollection.dataSource = self
        productCollection.delegate = self
        
//        productCollection.gemini
//            .customAnimation()
//            .translation(x: 0, y: 50, z: 0)
//            .rotationAngle(x: 0, y: 13, z: 0)
//            .ease(.easeOutExpo)
//            .shadowEffect(.fadeIn)
//            .maxShadowAlpha(0.3)
//
        // Do any additional setup after loading the view.
    }
    

 
    func getProducts()
    {
        appdelegate.showActivityIndicatory(uiView: self.view)

        NetworkManager.getProducts(completion: { (dto) in
            self.appdelegate.hideActivityIndicatory()

            self.products = dto.products
            self.productCollection.reloadData()
        }) { (error) in
            
            self.appdelegate.hideActivityIndicatory()

        }
    }
    
    

    @IBAction func back(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
    }
    
    func layout(withParentView parentView: UIView) -> UICollectionViewFlowLayout {
            let layout = UICollectionViewPagingFlowLayout()
            layout.itemSize = CGSize(width: parentView.bounds.width - 100, height: parentView.bounds.height - 70)
            layout.sectionInset = UIEdgeInsets(top: 0, left: 50, bottom: 0, right: 50)
            layout.minimumLineSpacing = 10
            layout.scrollDirection = .horizontal
            return layout
        
        }
    
}

extension ProductCatalogueViewController {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        productCollection.animateVisibleCells()
    }
}



enum CustomAnimationType {
    case custom1
    case custom2
    
}

//
//

//
//return products.count
