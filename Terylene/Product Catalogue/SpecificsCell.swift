//
//  SpecificsCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 18/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class SpecificsCell: UITableViewCell {

    
    @IBOutlet weak var spDetail: UILabel!
    @IBOutlet weak var spTitle: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
