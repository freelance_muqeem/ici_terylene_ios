//
//  ProductDetailController.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 22/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import WebKit
class ProductDetailController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var heading2: UILabel!
    @IBOutlet weak var heading1: UILabel!
    @IBOutlet weak var proDetail: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var specificsView: UITableView!
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    var product : ProductDtoProduct!
    
    //    @IBOutlet weak var content: UITextView!
    
    //    @IBOutlet weak var content: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        product = BDefaults.instance.getProducts()[BDefaults.instance.getInt(key: "productId")]
        
        specificsView.delegate = self
        specificsView.dataSource = self
        specificsView.cornerRadius = 20
        
        
        heading2.text = product.descriptionField
        heading1.text = product.heading
        proDetail.text = product.content
        
        
        //        wkView.loadHTMLString(product.descriptionField!, baseURL: nil)
        
        
        print(product.descriptionField)
        img.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(product.image!)"), placeholderImage:
            UIImage(named: "picture.png"))
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.viewHeight.constant = .init((self.product.specifications.count * 30)+10)
        
        print(self.specificsView.contentSize.height)
    }
    
    
   
    
    
    
    @IBAction func playVideo(_ sender: Any) {
        let url = URL(string:"\(AppUtils.Image_BASE_URL)/\(product.video!)")
//         let url = URL(string:"\(product.video!)")
        let player = AVPlayer(url: url!)
        
        
        let vc = AVPlayerViewController()
        vc.player = player
        player.isMuted = false
        
        self.present(vc, animated: true) { vc.player?.play() }
        
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return product.specifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "specificationsCell") as! SpecificsCell
        
        cell.spTitle.text = product.specifications[indexPath.row].name
        
        cell.spDetail.text = "\(product.specifications[indexPath.row].value ?? "" )"
        
        return cell
    }
    
    
    
    
}
