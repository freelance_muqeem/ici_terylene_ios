//
//  YarnDetails.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 16/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SWSegmentedControl
import Charts
class YarnDetails: UIViewController ,SWSegmentedControlDelegate{
    
    
    
    @IBOutlet weak var yar30Img: UIImageView!
    @IBOutlet weak var yar30Per: UILabel!
    @IBOutlet weak var yar30Value: UILabel!
    
    
    
    @IBOutlet weak var yar50Img: UIImageView!
    @IBOutlet weak var yar50Per: UILabel!
    @IBOutlet weak var yar50Value: UILabel!
    
    
    
    
    
  
    
    @IBOutlet weak var yar36Img: UIImageView!
    @IBOutlet weak var yar36Per: UILabel!
    @IBOutlet weak var yar36Value: UILabel!
    
    
    @IBOutlet weak var yar38Img: UIImageView!
    @IBOutlet weak var yar38Per: UILabel!
    @IBOutlet weak var yar38Value: UILabel!
    
    
    
    @IBOutlet weak var yar26Img: UIImageView!
    @IBOutlet weak var yar26Per: UILabel!
    @IBOutlet weak var yar26Value: UILabel!
    
    
    
    var selected = 0

    
    
    @IBOutlet weak var yar20Img: UIImageView!
    @IBOutlet weak var yar20Per: UILabel!
    @IBOutlet weak var yar20Value: UILabel!
    
    
    
    @IBOutlet weak var cYesterday: UILabel!
    
    
    
    
    
    @IBOutlet weak var yarn38Chart: LineChartView!
    @IBOutlet weak var yarn30Chart: LineChartView!
    @IBOutlet weak var yarn50Chart: LineChartView!
    @IBOutlet weak var yarn20Chart: LineChartView!
    @IBOutlet weak var yarn36Chart: LineChartView!
    @IBOutlet weak var yarn26Chart: LineChartView!
    
    
    
    
    
    
    
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var cArrow: UIImageView!
    @IBOutlet weak var cChanges: UILabel!
    @IBOutlet weak var cView: UIView!
    
    @IBOutlet weak var cValue: UILabel!
    @IBOutlet weak var cHeader: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var chartDate: UILabel!
    
    @IBOutlet weak var cPercent: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var sc : SWSegmentedControl!
    
    var month = BDefaults.instance.getGraphMonth()
    var week = BDefaults.instance.getGraphWeekly()
    let today = BDefaults.instance.getDashboardDaily()[0].today!
    let yesterday =  BDefaults.instance.getDashboardDaily()[0].yesterday!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chartDate.text = AppUtils.getGraphDate()
        date.text = AppUtils.getCurrentTimeShort()
        sc = SWSegmentedControl(items: ["1W", "1M"])
        sc.delegate = self
        sc.font = UIFont(name: "SFProDisplay-Semibold", size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        sc.indicatorColor = UIColor.init(cgColor: AppUtils.lightColor)
        sc.badgeColor = UIColor.init(cgColor: AppUtils.lightColor)
        
        sc.indicatorThickness = CGFloat(3)
        
        sc.frame = CGRect(x: 0, y: -3, width: tabView.frame.width, height: 44)
        tabView.addSubview(sc)
        selected = 0

        showBottomDetail()
        showDetail()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func showDetail(){
        
 
        selected = 0

        
        if BDefaults.instance.getString(key: "yarn") == "38"{
            sc.setSelectedSegmentIndex(0)
            cHeader.text = "-Yarn 38 PC"
            setYarn38()
            setYarn38(m: week)
        }else if BDefaults.instance.getString(key: "yarn") == "30"{
            setYarn30()
            cHeader.text = "-Yarn 30 PC"
            setYarn30(m: week)
            sc.setSelectedSegmentIndex(0)
        }else if BDefaults.instance.getString(key: "yarn") == "50"{
            setYarn50()
            cHeader.text = "-Yarn 50 PP"
            setYarn50(m: week)
            sc.setSelectedSegmentIndex(0)
        }else if BDefaults.instance.getString(key: "yarn") == "20"{
            setYarn20()
            cHeader.text = "-Yarn 20/S"
            setYarn20(m: week)
            sc.setSelectedSegmentIndex(0)
        }else if BDefaults.instance.getString(key: "yarn") == "36"{
            setYarn36()
            cHeader.text = "-Yarn 36 PV"
            setYarn36(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        else if BDefaults.instance.getString(key: "yarn") == "26"{
            setYarn26()
            cHeader.text = "-Yarn 26 PV"
            setYarn26(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        
        
    }
    
    
    
    func setYarn38(){
        
        cValue.text = "\(self.today.todayYarn38Pc!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc)) "
        
        
        cYesterday.text = "\(self.yesterday.yesterdayYarn38Pc!)"

        
        ///usd
        if(self.today.todayYarn38Pc > self.yesterday.yesterdayYarn38Pc){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayYarn38Pc < self.yesterday.yesterdayYarn38Pc){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn38Pc, b: self.today.todayYarn38Pc))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
        }
    }
    
    func setYarn30(){
        
        cValue.text = "\(self.today.todayYarn30Pc!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayYarn30Pc!)"

        
        if(self.today.todayYarn30Pc > self.yesterday.yesterdayYarn30Pc){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayYarn30Pc < self.yesterday.yesterdayYarn30Pc){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn30Pc, b: self.today.todayYarn30Pc))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    func setYarn50(){
        
        cValue.text = "\(self.today.todayYarn50Pp!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp)) "
        cYesterday.text = "\(self.yesterday.yesterdayYarn50Pp!)"

        if(self.today.todayYarn50Pp > self.yesterday.yesterdayYarn50Pp){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayYarn50Pp < self.yesterday.yesterdayYarn50Pp){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn50Pp, b: self.today.todayYarn50Pp))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    func setYarn20(){
        
        
        cValue.text = "\(self.today.todayYarn20S!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S)) "
        cYesterday.text = "\(self.yesterday.yesterdayYarn20S!)"

        if(self.today.todayYarn20S > self.yesterday.yesterdayYarn20S){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayYarn20S > self.yesterday.yesterdayYarn20S){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn20S, b: self.today.todayYarn20S))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    func setYarn36(){
        
        cValue.text = "\(self.today.todayYarn36Pc!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc)) "
        cYesterday.text = "\(self.yesterday.yesterdayYarn36Pc!)"

        if(self.today.todayYarn36Pc > self.yesterday.yesterdayYarn36Pc){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else  if(self.today.todayYarn36Pc < self.yesterday.yesterdayYarn36Pc){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn36Pc, b: self.today.todayYarn36Pc))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
    }
    
    func setYarn26(){
        
        cValue.text = "\(self.today.todayYarn26Pc!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc)) "
        cYesterday.text = "\(self.yesterday.yesterdayYarn26Pc!)"

        if(self.today.todayYarn26Pc > self.yesterday.yesterdayYarn26Pc){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayYarn26Pc < self.yesterday.yesterdayYarn26Pc){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn26Pc, b: self.today.todayYarn26Pc))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
    }
    
    
    
    
    func showBottomDetail(){
        
        bottomView.cornerRadius = 20
        
         yar50Value.text = "\(self.today.todayYarn50Pp!)"
         yar38Value.text = "\(self.today.todayYarn38Pc!)"
         yar36Value.text = "\(self.today.todayYarn36Pc!)"
         yar30Value.text = "\(self.today.todayYarn30Pc!)"
         yar26Value.text = "\(self.today.todayYarn26Pc!)"
         yar20Value.text = "\(self.today.todayYarn20S!)"
        
      
        if(self.today.todayYarn50Pp > self.yesterday.yesterdayYarn50Pp){
            
            
             yar50Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp))% "
             yar50Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             yar50Img.image = UIImage(named: "UpperArrow")
            self.setYarn50(chart:  yarn50Chart,color: "green")
            
        }else if(self.today.todayYarn50Pp < self.yesterday.yesterdayYarn50Pp){
            
             yar50Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn50Pp, b: self.today.todayYarn50Pp))% "
             yar50Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             yar50Img.image = UIImage(named: "down")
            self.setYarn50(chart:  yarn50Chart,color: "red")
            
            
        }else{
             yar50Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp))% "
             yar50Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             yar50Img.image = UIImage(named: "equal")
            self.setYarn50(chart:  yarn50Chart,color: "yellow")
            
        }
        
                
                
                
                
                if(self.today.todayYarn38Pc > self.yesterday.yesterdayYarn38Pc){
                    
                    
                     yar38Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc))% "
                     yar38Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                     yar38Img.image = UIImage(named: "UpperArrow")
                    self.setYarn38(chart:  yarn38Chart,color: "green")
                    
                }else if(self.today.todayYarn38Pc < self.yesterday.yesterdayYarn38Pc){
                    
                     yar38Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn38Pc, b: self.today.todayYarn38Pc))% "
                     yar38Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                     yar38Img.image = UIImage(named: "down")
                    self.setYarn38(chart:  yarn38Chart,color: "red")
                    
                    
                }else{
                    yar38Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc))% "
                    yar38Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                    yar38Img.image = UIImage(named: "equal")
                    self.setYarn38(chart:  yarn38Chart,color: "yellow")
                    
        }
                
        
        if(self.today.todayYarn36Pc > self.yesterday.yesterdayYarn36Pc){
            
            
             yar36Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc))% "
             yar36Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             yar36Img.image = UIImage(named: "UpperArrow")
            self.setyarn36(chart:  yarn36Chart,color: "green")
            
        }else if(self.today.todayYarn36Pc < self.yesterday.yesterdayYarn36Pc){
            
             yar36Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn36Pc, b: self.today.todayYarn36Pc))% "
             yar36Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             yar36Img.image = UIImage(named: "down")
            self.setyarn36(chart:  yarn36Chart,color: "red")
            
            
        }else{
            
             yar36Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc))% "
             yar36Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             yar36Img.image = UIImage(named: "equal")
            self.setyarn36(chart:  yarn36Chart,color: "yellow")
            
            
        }
        
                
                
                
                
                
        
        if(self.today.todayYarn30Pc > self.yesterday.yesterdayYarn30Pc){
            
            
             yar30Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc))% "
             yar30Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             yar30Img.image = UIImage(named: "UpperArrow")
            self.setYarn30(chart:  yarn30Chart,color: "green")
            
        }else if(self.today.todayYarn30Pc < self.yesterday.yesterdayYarn30Pc){
            
             yar30Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn30Pc, b: self.today.todayYarn30Pc))% "
             yar30Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             yar30Img.image = UIImage(named: "down")
            self.setYarn30(chart:  yarn30Chart,color: "red")
            
            
        }else{
            
            
             yar30Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc))% "
             yar30Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             yar30Img.image = UIImage(named: "equal")
            self.setYarn30(chart:  yarn30Chart,color: "yellow")
            
            
        }

                
                
        
        if(self.today.todayYarn26Pc > self.yesterday.yesterdayYarn26Pc){
            
            
             yar26Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc))% "
             yar26Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             yar26Img.image = UIImage(named: "UpperArrow")
            self.setYarn26(chart:  yarn26Chart,color: "green")
            
        }else if(self.today.todayYarn26Pc < self.yesterday.yesterdayYarn26Pc){
            
             yar26Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn26Pc, b: self.today.todayYarn26Pc))% "
             yar26Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             yar26Img.image = UIImage(named: "down")
            self.setYarn26(chart:  yarn26Chart,color: "red")
            
            
        }else{
            
             yar26Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc))% "
             yar26Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             yar26Img.image = UIImage(named: "equal")
            self.setYarn26(chart:  yarn26Chart,color: "yellow")
            
            
            
        }
        
        
                
                
                
                
        
        
        if(self.today.todayYarn20S > self.yesterday.yesterdayYarn20S){
            
            
             yar20Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S))% "
             yar20Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             yar20Img.image = UIImage(named: "UpperArrow")
            self.setYarn20(chart:  yarn20Chart,color: "green")
            
        }else  if(self.today.todayYarn20S < self.yesterday.yesterdayYarn20S){
            
             yar20Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn20S, b: self.today.todayYarn20S))% "
             yar20Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             yar20Img.image = UIImage(named: "down")
            self.setYarn20(chart:  yarn20Chart,color: "red")
            
            
        }else{
            
             yar20Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S))% "
             yar20Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             yar20Img.image = UIImage(named: "equal")
            self.setYarn20(chart:  yarn20Chart,color: "yellow")
            
            
            
        }
                
            
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
//
    func segmentedControl(_ control: SWSegmentedControl, didSelectItemAtIndex index: Int) {

        
        selected = index
        
        if BDefaults.instance.getString(key: "yarn") == "38"{

            if(index == 0){
                setYarn38(m: week)

            }else{
                if(month.count > 0){

                setYarn38(m: month)
                }            }

        }else if BDefaults.instance.getString(key: "yarn") == "30"{
            if(index == 0){
                setYarn30(m: week)

            }else{
                if(month.count > 0){

                setYarn30(m: month)
                }            }
        }else if BDefaults.instance.getString(key: "yarn") == "50"{
            if(index == 0){
                setYarn50(m: week)

            }else{
                if(month.count > 0){

                setYarn50(m: month)
                }            }
        }else if BDefaults.instance.getString(key: "yarn") == "20"{
            if(index == 0){
                setYarn20(m: week)

            }else{
                if(month.count > 0){

                setYarn20(m: month)
                }            }
        }else if BDefaults.instance.getString(key: "yarn") == "36"{
            if(index == 0){
                setYarn36(m: week)

            }else{
                if(month.count > 0){

                setYarn36(m: month)
                }            }
        }
        else if BDefaults.instance.getString(key: "yarn") == "26"{
            if(index == 0){
                setYarn26(m: week)
                
            }else{
                if(month.count > 0){

                setYarn26(m: month)
                }            }
        }


        print("did select \(index)")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setYarn38(m : [GraphDtoWeek] ){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].yarn38Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setYarn30(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].yarn30Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setYarn50(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].yarn50Pp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    func setYarn20(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].yarn20S)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setYarn36(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].yarn36Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setYarn26(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].yarn26Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    
    
    
    func setUpChart(chart : LineChartView){
        
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = false
        
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        xAxis.labelCount = 4

        
        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
      
        
        
        
        
    }
   
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        if selected == 0 {
            
            
            let formater = ChartFormatter( v: self.week )
            
            self.chart.xAxis.valueFormatter = formater
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        else{
            
            let formater = ChartFormatter( v: AppUtils.month )
            self.chart.xAxis.valueFormatter = formater
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        
        
        
        chart.data = data
        
        
        
    }
    
    
    func setYarn50(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn50Pp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    func setYarn38(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn38Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    func setyarn36(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn36Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    
    func setYarn30(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn30Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    func setYarn26(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn26Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    func setYarn20(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn20S)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
        
    }
    
    
    
    
    
   
    func setYarn50(chart : LineChartView){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn50Pp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
    }
    func setYarn38(chart : LineChartView){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn38Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
         setUpChart(chart: chart)
        setData(values, chart: chart)
    }
    func setyarn36(chart : LineChartView){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn36Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
         setUpChart(chart: chart)
        setData(values, chart: chart)
    }
    
    
    func setYarn30(chart : LineChartView){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn30Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
         setUpChart(chart: chart)
        setData(values, chart: chart)
    }
    
    func setYarn26(chart : LineChartView){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn26Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
         setUpChart(chart: chart)
        setData(values, chart: chart)
    }
    
    func setYarn20(chart : LineChartView){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn20S)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
         setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func openYarn38(_ sender: Any) {
        
        BDefaults.instance.setString(key: "yarn", value: "38")
        showDetail()
    }
    @IBAction func openYarn30(_ sender: Any) {
        BDefaults.instance.setString(key: "yarn", value: "30")
        showDetail()
    }
    @IBAction func openYarn50(_ sender: Any) {
        BDefaults.instance.setString(key: "yarn", value: "50")
        showDetail()
    }
    @IBAction func openYarn20(_ sender: Any) {
        BDefaults.instance.setString(key: "yarn", value: "20")
        showDetail()
    }
    @IBAction func openYarn36(_ sender: Any) {
        BDefaults.instance.setString(key: "yarn", value: "36")
        showDetail()
    }
    
    @IBAction func openYarn26(_ sender: Any) {
        BDefaults.instance.setString(key: "yarn", value: "26")
        showDetail()
    }
    
    
    
}
