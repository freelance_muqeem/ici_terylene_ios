//
//  CurrencyDetail.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 15/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
import SWSegmentedControl
class CurrencyDetail: UIViewController ,SWSegmentedControlDelegate{
    
    
    
    @IBOutlet weak var usdPkrView: UIView!
    @IBOutlet weak var usdPkrValue: UILabel!
    
    @IBOutlet weak var usdPkrPer: UILabel!
    
    @IBOutlet weak var usdPkrImg: UIImageView!
    //////CNY PKr
    @IBOutlet weak var cnyPkrView: UIView!
    
    
    @IBOutlet weak var cnyPkrValue: UILabel!
    
    @IBOutlet weak var cnyPkrPer: UILabel!
    
    @IBOutlet weak var cnyPkrImg: UIImageView!
    ////////GBP
    
    @IBOutlet weak var gbpView: UIView!
    
    @IBOutlet weak var gbpValue: UILabel!
    
    @IBOutlet weak var gbpImg: UIImageView!
    @IBOutlet weak var gbpPerc: UILabel!
    
    ////////EUR
    
    @IBOutlet weak var eurView: UIView!
    
    @IBOutlet weak var eurValue: UILabel!
    
    @IBOutlet weak var eurPercent: UILabel!
    
    @IBOutlet weak var eurImg: UIImageView!
    
    /////////JPY
    @IBOutlet weak var jpyView: UIView!
    
    @IBOutlet weak var jpyValue: UILabel!
    
    @IBOutlet weak var jpyPerc: UILabel!
    
    @IBOutlet weak var jpyImg: UIImageView!
    
    
    
    @IBOutlet weak var cYesterday: UILabel!
    
    
    var selected = 0
    
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var cArrow: UIImageView!
    @IBOutlet weak var cChanges: UILabel!
    @IBOutlet weak var cView: UIView!
    
    @IBOutlet weak var cValue: UILabel!
    @IBOutlet weak var cHeader: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var chartDate: UILabel!
    
    @IBOutlet weak var cPercent: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var sc : SWSegmentedControl!
    
   
    let today = BDefaults.instance.getDashboardDaily()[0].today!
    let yesterday =  BDefaults.instance.getDashboardDaily()[0].yesterday!
    var month = BDefaults.instance.getGraphMonth()
     var week = BDefaults.instance.getGraphWeekly()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chartDate.text = AppUtils.getGraphDate()
        date.text = AppUtils.getCurrentTimeShort()
        sc = SWSegmentedControl(items: ["1W", "1M"])
        sc.delegate = self
        sc.font = UIFont(name: "SFProDisplay-Semibold", size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        sc.indicatorColor = UIColor.init(cgColor: AppUtils.lightColor)
        sc.badgeColor = UIColor.init(cgColor: AppUtils.lightColor)
        
        sc.indicatorThickness = CGFloat(3)
        
        sc.frame = CGRect(x: 0, y: -3, width: tabView.frame.width, height: 44)
        tabView.addSubview(sc)
        showDetail()
        selected = 0
        setUpChart(chart: chart)
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        showDetailGraph()
        
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                self.showBottomDetail()
            }}
        
        
    }
    
    
    
    func showDetail(){
        selected = 0
        if BDefaults.instance.getString(key: "currency") == "usd"{
            self.sc.setSelectedSegmentIndex(0)
            self.cHeader.text = "PKR-USD"
            self.setUsd()
            
          
        }else if BDefaults.instance.getString(key: "currency") == "cny"{
            self.setCny()
            self.cHeader.text = "PKR-CNY"
            //            self.setCny(m: self.week)
            self.sc.setSelectedSegmentIndex(0)
        }else if BDefaults.instance.getString(key: "currency") == "gbp"{
            self.setGbp()
            self.cHeader.text = "PKR-GBP"
            //            self.setGbp(m: self.week)
            self.sc.setSelectedSegmentIndex(0)
        }else if BDefaults.instance.getString(key: "currency") == "eur"{
            self.setEur()
            self.cHeader.text = "PKR-EUR"
            //            self.setEur(m: self.week)
            self.sc.setSelectedSegmentIndex(0)
        }else if BDefaults.instance.getString(key: "currency") == "jpy"{
            self.setJpy()
            self.cHeader.text = "PKR-JPY"
            //            self.setJpy(m: self.week)
            self.sc.setSelectedSegmentIndex(0)
        }
        
        
        
    }
    
    
    func showDetailGraph(){
        
        if BDefaults.instance.getString(key: "currency") == "usd"{
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    self.setUsd(m: self.week)
                }}
        }else if BDefaults.instance.getString(key: "currency") == "cny"{
            
            self.setCny(m: self.week)
            
        }else if BDefaults.instance.getString(key: "currency") == "gbp"{
            
            self.setGbp(m: self.week)
            
        }else if BDefaults.instance.getString(key: "currency") == "eur"{
            
            self.setEur(m: self.week)
            
        }else if BDefaults.instance.getString(key: "currency") == "jpy"{
            
            self.setJpy(m: self.week)
            
        }
        
        
        
    }
    
    
    
    
    func setUsd(){
        
        cValue.text = "\(self.today.todayUsd!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayUsd, b: self.yesterday.yesterdayUsd)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayUsd!)"
        
        
        ///usd
        if((self.today.todayUsd > self.yesterday.yesterdayUsd) ){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayUsd, b: self.yesterday.yesterdayUsd))% "
            
            
            
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            
            
        }else if self.today.todayUsd < self.yesterday.yesterdayUsd {
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayUsd, b: self.today.todayUsd))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayUsd, b: self.yesterday.yesterdayUsd))% "
            
            
            
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
        }
    }
    
    func setCny(){
        
        cValue.text = "\(self.today.todayCyn!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayCyn!)"
        
        
        if(self.today.todayCyn > self.yesterday.yesterdayCyn ){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else   if self.today.todayCyn < self.yesterday.yesterdayCyn {
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCyn, b: self.today.todayCyn))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)

        }
        
    }
    func setGbp(){
        
        cValue.text = "\(self.today.todayGbp!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp)) "
        cYesterday.text = "\(self.yesterday.yesterdayGbp!)"
        
        
        if(self.today.todayGbp > self.yesterday.yesterdayGbp){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else  if(self.today.todayGbp < self.yesterday.yesterdayGbp){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayGbp, b: self.today.todayGbp))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    func setEur(){
        
        
        cValue.text = "\(self.today.todayEur!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayEur, b: self.yesterday.yesterdayEur)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayEur!)"
        
        
        if(self.today.todayEur > self.yesterday.yesterdayEur) {
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayEur, b: self.yesterday.yesterdayEur))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else  if(self.today.todayEur < self.yesterday.yesterdayEur){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayEur, b: self.today.todayEur))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayEur, b: self.yesterday.yesterdayEur))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    func setJpy(){
        
        cValue.text = "\(self.today.todayJpy!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy)) "
        cYesterday.text = "\(self.yesterday.yesterdayJpy!)"
        
        
        if(self.today.todayJpy > self.yesterday.yesterdayJpy) {
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else   if(self.today.todayJpy < self.yesterday.yesterdayJpy){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayJpy, b: self.today.todayJpy))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
    }
    
    
    func showBottomDetail(){
        
        bottomView.cornerRadius = 20
        
        usdPkrValue.text = "\(self.today.todayUsd!)"
        cnyPkrValue.text = "\(self.today.todayCyn!)"
        eurValue.text = "\(self.today.todayEur!)"
        jpyValue.text = "\(self.today.todayJpy!)"
        gbpValue.text = "\(self.today.todayGbp!)"
        
        
        ///usd
        if(self.today.todayUsd > self.yesterday.yesterdayUsd){
            
            
            
             usdPkrPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayUsd, b: self.yesterday.yesterdayUsd))% "
             usdPkrPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             usdPkrView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             usdPkrImg.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayUsd < self.yesterday.yesterdayUsd){
            
             usdPkrPer.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayUsd, b: self.today.todayUsd))% "
             usdPkrPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             usdPkrView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             usdPkrImg.image = UIImage(named: "down")
            
            
        }else{
            
             usdPkrPer.text = " -\(AppUtils.returnPercentage(a:  self.today.todayUsd, b:self.yesterday.yesterdayUsd))% "
             usdPkrPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             usdPkrView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             usdPkrImg.image = UIImage(named: "equal")
            
            
        }
        
        ///CNY
        
        if(self.today.todayCyn > self.yesterday.yesterdayCyn){
            
            
             cnyPkrPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn))% "
             cnyPkrPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             cnyPkrView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             cnyPkrImg.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayCyn < self.yesterday.yesterdayCyn){
            
             cnyPkrPer.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCyn, b: self.today.todayCyn))% "
             cnyPkrPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             cnyPkrView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             cnyPkrImg.image = UIImage(named: "down")
            
            
        } else{
             cnyPkrPer.text = " \(AppUtils.returnPercentage(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn))% "
             cnyPkrPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             cnyPkrView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             cnyPkrImg.image = UIImage(named: "equal")
        }
        
        //////GBP
        
        if(self.today.todayGbp > self.yesterday.yesterdayGbp ){
            
            
             gbpPerc.text = " +\(AppUtils.returnPercentage(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp))% "
             gbpPerc.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             gbpView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             gbpImg.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayGbp < self.yesterday.yesterdayGbp ){
            
             gbpPerc.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayGbp, b: self.today.todayGbp))% "
             gbpPerc.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             gbpView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             gbpImg.image = UIImage(named: "down")
            
            
        }else{
             gbpPerc.text = " +\(AppUtils.returnPercentage(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp))% "
             gbpPerc.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             gbpView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             gbpImg.image = UIImage(named: "equal")
            
        }
        
        //////EUR
        if(self.today.todayEur > self.yesterday.yesterdayEur){
            
            
             eurPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayEur, b: self.yesterday.yesterdayEur))% "
             eurPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             eurView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             eurImg.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayEur < self.yesterday.yesterdayEur){
            
             eurPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayEur, b: self.today.todayEur))% "
             eurPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             eurView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             eurImg.image = UIImage(named: "down")
            
            
        }else{
            
             eurPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayEur, b: self.yesterday.yesterdayEur))% "
             eurPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             eurView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             eurImg.image = UIImage(named: "equal")
        }
        
        
        //////JPY
        if(self.today.todayJpy > self.yesterday.yesterdayJpy ){
            
            
             jpyPerc.text = " +\(AppUtils.returnPercentage(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy))% "
             jpyPerc.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             jpyView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             jpyImg.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayJpy < self.yesterday.yesterdayJpy ){
            
             jpyPerc.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayJpy, b: self.today.todayJpy))% "
             jpyPerc.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             jpyView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             jpyImg.image = UIImage(named: "down")
            
            
        }else{
             jpyPerc.text = " \(AppUtils.returnPercentage(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy))% "
             jpyPerc.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             jpyView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             jpyImg.image = UIImage(named: "equal")
            
        }
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func segmentedControl(_ control: SWSegmentedControl, didSelectItemAtIndex index: Int) {
        
        
        selected = index
        
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                if BDefaults.instance.getString(key: "currency") == "usd"{
                    
                    if(index == 0){
                       

                        self.setUsd(m: self.week)
                        
                    }else{
                        self.setUsd(m: self.month)
                       

                    }
                    
                }else if BDefaults.instance.getString(key: "currency") == "cny"{
                    if(index == 0){
                        self.setCny(m: self.week)
                        
                    }else{
                        self.setCny(m: self.month)
                    }
                }else if BDefaults.instance.getString(key: "currency") == "gbp"{
                    if(index == 0){
                        self.setGbp(m: self.week)
                        
                    }else{
                        self.setGbp(m: self.month)
                    }
                }else if BDefaults.instance.getString(key: "currency") == "eur"{
                    if(index == 0){
                        self.setEur(m: self.week)
                        
                    }else{
                        self.setEur(m: self.month)
                    }
                }else if BDefaults.instance.getString(key: "currency") == "jpy"{
                    if(index == 0){
                        self.setJpy(m: self.week)
                        
                    }else{
                        self.setJpy(m: self.month)
                    }
                }
                
            }}
        
        print("did select \(index)")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setUsd(m : [GraphDtoWeek] ){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].usd)
            return ChartDataEntry(x: Double(i), y: val)
            
//            ChartDataEntry(x: T##Double, y: <#T##Double#>)
        }
        
        setData(values, chart: chart)
        
    }
    func setCny(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].cny)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
//        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setEur(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].eur)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
//        setUpChart(chart: chart)
        setData(values, chart: chart)
        
        
        
    }
    
    func setJpy(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].jpy)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
//        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setGbp(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].gbp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
//        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    @IBAction func openUsd(_ sender: Any) {
        
        BDefaults.instance.setString(key: "currency", value: "usd")
        showDetail()
        showDetailGraph()
    }
    @IBAction func openCny(_ sender: Any) {
        BDefaults.instance.setString(key: "currency", value: "cny")
        showDetail()
        showDetailGraph()
    }
    @IBAction func openGbp(_ sender: Any) {
        BDefaults.instance.setString(key: "currency", value: "gbp")
        showDetail()
        showDetailGraph()
    }
    @IBAction func openEur(_ sender: Any) {
        BDefaults.instance.setString(key: "currency", value: "eur")
        showDetail()
        showDetailGraph()
    }
    @IBAction func openJpy(_ sender: Any) {
        BDefaults.instance.setString(key: "currency", value: "jpy")
        showDetail()
        showDetailGraph()
    }
    
    
    
    func setUpChart(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = false
        
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelCount = 4
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        
        
        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
     
        
        
        
        
        
        
    }
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        if selected == 0 {
            
            
            let formater = ChartFormatter( v: self.week )
            
         self.chart.xAxis.valueFormatter = formater
        
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
        
        }
        else{
            
            let formater = ChartFormatter( v: self.month )
         self.chart.xAxis.valueFormatter = formater
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
        
        }
        
        
        chart.data = data
        
        
        
    }
    
    
    
    
    
    
    
    
}
