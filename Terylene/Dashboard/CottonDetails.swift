//
//  CottonDetails.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 16/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SWSegmentedControl
import Charts
class CottonDetails: UIViewController ,SWSegmentedControlDelegate{
    
    
    
    @IBOutlet weak var nyfImg: UIImageView!
    @IBOutlet weak var nyfPercent: UILabel!
    @IBOutlet weak var nyfValue: UILabel!
    @IBOutlet weak var cottonImg: UIImageView!
    @IBOutlet weak var cottonPercent: UILabel!
    @IBOutlet weak var cottonValue: UILabel!
    var selected = 0

    @IBOutlet weak var cYesterday: UILabel!
    
    
    @IBOutlet weak var nyfChart: LineChartView!
    @IBOutlet weak var cottonChart: LineChartView!
    
    
    
    
    
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var cArrow: UIImageView!
    @IBOutlet weak var cChanges: UILabel!
    @IBOutlet weak var cView: UIView!
    
    @IBOutlet weak var cValue: UILabel!
    @IBOutlet weak var cHeader: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var chartDate: UILabel!
    
    @IBOutlet weak var cPercent: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var sc : SWSegmentedControl!
    
    var month = BDefaults.instance.getGraphMonth()
    var week = BDefaults.instance.getGraphWeekly()
    let today = BDefaults.instance.getDashboardDaily()[0].today!
    let yesterday =  BDefaults.instance.getDashboardDaily()[0].yesterday!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chartDate.text = AppUtils.getGraphDate()
        date.text = AppUtils.getCurrentTimeShort()
        sc = SWSegmentedControl(items: ["1W", "1M"])
        sc.delegate = self
        sc.font = UIFont(name: "SFProDisplay-Semibold", size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        sc.indicatorColor = UIColor.init(cgColor: AppUtils.lightColor)
        sc.badgeColor = UIColor.init(cgColor: AppUtils.lightColor)
        
        sc.indicatorThickness = CGFloat(3)
        
        sc.frame = CGRect(x: 0, y: -3, width: tabView.frame.width, height: 44)
        tabView.addSubview(sc)
        selected = 0

        showBottomDetail()
        showDetail()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func showDetail(){
        selected = 0

        if BDefaults.instance.getString(key: "cotton") == "cotton"{
            sc.setSelectedSegmentIndex(0)
            cHeader.text = "-DOMESTIC"
            setCotton()
            setCotton(m: week)
        }else if BDefaults.instance.getString(key: "cotton") == "nyf"{
            setNyf()
            cHeader.text = "-NYF"
            setNyf(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        
        
    }
    
    
    
    func setCotton(){
        
        cValue.text = "\(self.today.todayCottonMaund!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayCottonMaund!)"

        
        
        ///Wti
        if(self.today.todayCottonMaund > self.yesterday.yesterdayCottonMaund){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayCottonMaund > self.yesterday.yesterdayCottonMaund){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCottonMaund, b: self.today.todayCottonMaund))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
        }
    }
    
    func setNyf(){
        
        cValue.text = "\(self.today.todayNyfMaund!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayNyfMaund!)"

        
        if(self.today.todayNyfMaund > self.yesterday.yesterdayNyfMaund){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayNyfMaund < self.yesterday.yesterdayNyfMaund){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayNyfMaund, b: self.today.todayNyfMaund))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    
    
    func showBottomDetail(){
        
        bottomView.cornerRadius = 20
        
         nyfValue.text = "\(self.today.todayNyfPkr!)"
         cottonValue.text = "\(self.today.todayCottonMaund!)"

       
        if(self.today.todayNyfMaund > self.yesterday.yesterdayNyfMaund) {
            
            
             nyfPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund))% "
             nyfPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             nyfImg.image = UIImage(named: "UpperArrow")
            self.setNyf(chart:  nyfChart,color: "green")
            
        }else  if(self.today.todayNyfMaund < self.yesterday.yesterdayNyfMaund){
            
             nyfPercent.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayNyfMaund, b: self.today.todayNyfMaund))% "
             nyfPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             nyfImg.image = UIImage(named: "down")
            self.setNyf(chart:  nyfChart,color: "red")
            
            
        }else{
             nyfPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund))% "
             nyfPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             nyfImg.image = UIImage(named: "equal")
            self.setNyf(chart:  nyfChart,color: "yellow")
        }
        
                
                
        
        if(self.today.todayCottonMaund > self.yesterday.yesterdayCottonMaund){
            
            
             cottonPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund))% "
             cottonPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             cottonImg.image = UIImage(named: "UpperArrow")
            self.setCotton(chart:  cottonChart,color: "green")
            
        }else if(self.today.todayCottonMaund < self.yesterday.yesterdayCottonMaund){
            
             cottonPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCottonMaund, b: self.today.todayCottonMaund))% "
             cottonPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             cottonImg.image = UIImage(named: "down")
            self.setCotton(chart:  cottonChart,color: "red")
            
            
        }else{
             cottonPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund))% "
             cottonPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             cottonImg.image = UIImage(named: "equal")
            self.setCotton(chart:  cottonChart,color: "yellow")
            
        }
        
        
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func segmentedControl(_ control: SWSegmentedControl, didSelectItemAtIndex index: Int) {
        selected = index

        if BDefaults.instance.getString(key: "cotton") == "cotton"{
            
            if(index == 0){
                setCotton(m: week)
                
            }else{
                if(month.count > 0){
                setCotton(m: month)
                }
            }
            
        }else if BDefaults.instance.getString(key: "cotton") == "nyf"{
            if(index == 0){
                setNyf(m: week)
                
            }else{
                if(month.count > 0){

                setNyf(m: month)
                }            }
        }
        print("did select \(index)")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setCotton(m : [GraphDtoWeek] ){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].cottonMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setNyf(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].nyfMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    @IBAction func openNyf(_ sender: Any) {
        
        BDefaults.instance.setString(key: "cotton", value: "nyf")
        showDetail()
    }
    @IBAction func openCotton(_ sender: Any) {
        BDefaults.instance.setString(key: "cotton", value: "cotton")
        showDetail()
    }
    
    
    
    
  
    func setUpChart(chart : LineChartView){
        
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = false
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        
        
        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        xAxis.labelCount = 4

        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
        
   
        
        
        
        
        
    }
    
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        if selected == 0 {
            
            
            let formater = ChartFormatter( v: self.week )
            
            self.chart.xAxis.valueFormatter = formater
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        else{
            
            let formater = ChartFormatter( v: AppUtils.month )
            self.chart.xAxis.valueFormatter = formater
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        
        
        
        chart.data = data
        
        
        
    }
    
    
    
    
    
    func setNyf(chart : LineChartView,color : String){
        
        AppUtils.setUpChartLittle(chart: chart)
        
        let values = (0..<month.count).map { (i) -> ChartDataEntry in
            let val = Double(month[i].nyfPkr)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    func setCotton(chart : LineChartView,color:  String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].cottonMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    
    
    
}
