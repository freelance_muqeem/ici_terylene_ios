//
//  FiberDetail.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 16/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SWSegmentedControl
import Charts
class FiberDetail: UIViewController ,SWSegmentedControlDelegate{
    
    
    @IBOutlet weak var cYesterday: UILabel!
    
    
    @IBOutlet weak var iciImg: UIImageView!
    @IBOutlet weak var iciPer: UILabel!
    @IBOutlet weak var iciValue: UILabel!
    var selected = 0

    
    
    @IBOutlet weak var chinaImg: UIImageView!
    @IBOutlet weak var chinaPer: UILabel!
    @IBOutlet weak var chinaValue: UILabel!
    
    
    
    
    
 
    
    @IBOutlet weak var importImg: UIImageView!
    @IBOutlet weak var importPer: UILabel!
    @IBOutlet weak var importValue: UILabel!
    
    
    
    
    
    
    
    
    
    @IBOutlet weak var importChart: LineChartView!
    @IBOutlet weak var chinaChart: LineChartView!
    @IBOutlet weak var iciChart: LineChartView!
    
    
    
    
    
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var cArrow: UIImageView!
    @IBOutlet weak var cChanges: UILabel!
    @IBOutlet weak var cView: UIView!
    
    @IBOutlet weak var cValue: UILabel!
    @IBOutlet weak var cHeader: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var chartDate: UILabel!
    
    @IBOutlet weak var cPercent: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var sc : SWSegmentedControl!
    
    var month = BDefaults.instance.getGraphMonth()
    var week = BDefaults.instance.getGraphWeekly()
    let today = BDefaults.instance.getDashboardDaily()[0].today!
    let yesterday =  BDefaults.instance.getDashboardDaily()[0].yesterday!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selected = 0

        chartDate.text = AppUtils.getGraphDate()
        date.text = AppUtils.getCurrentTimeShort()
        sc = SWSegmentedControl(items: ["1W", "1M"])
        sc.delegate = self
        sc.font = UIFont(name: "SFProDisplay-Semibold", size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        sc.indicatorColor = UIColor.init(cgColor: AppUtils.lightColor)
        sc.badgeColor = UIColor.init(cgColor: AppUtils.lightColor)
        
        sc.indicatorThickness = CGFloat(3)
        
        sc.frame = CGRect(x: 0, y: -3, width: tabView.frame.width, height: 44)
        tabView.addSubview(sc)
        
        showBottomDetail()
        showDetail()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func showDetail(){
        selected = 0

        if BDefaults.instance.getString(key: "fiber") == "ici"{
            sc.setSelectedSegmentIndex(0)
            cHeader.text = "-ICI SEMI DULL"
            setIci()
            setIci(m: week)
        }else if BDefaults.instance.getString(key: "fiber") == "china"{
            setChina()
            cHeader.text = "-CHINA PSF"
            setChina(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        else if BDefaults.instance.getString(key: "fiber") == "import"{
            setImport()
            cHeader.text = "-IMPORT OFFER"
            setImport(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        
    }
    
    
    
    func setIci(){
        
        cValue.text = "\(self.today.todayIciSemi!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayIciSemi!)"

        
        
        ///Wti
        if(self.today.todayIciSemi > self.yesterday.yesterdayIciSemi){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.today.todayIciSemi < self.yesterday.yesterdayIciSemi){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayIciSemi, b: self.today.todayIciSemi))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = "\(AppUtils.returnPercentage(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
        }
    }
    
    func setChina(){
        
        cValue.text = "\(self.today.todayChinaPsfPrice!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayChinaPsfPrice!)"

        
        if(self.today.todayChinaPsfPrice > self.yesterday.yesterdayChinaPsfPrice){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayChinaPsfPrice < self.yesterday.yesterdayChinaPsfPrice){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayChinaPsfPrice, b: self.today.todayChinaPsfPrice))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    
    func setImport(){
        
        cValue.text = "\(self.today.todayImportOffr!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayImportOffer!)"

        
        if(self.today.todayImportOffr > self.yesterday.yesterdayImportOffer){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayImportOffr < self.yesterday.yesterdayImportOffer){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayImportOffer, b: self.today.todayImportOffr))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    
    
    
    func showBottomDetail(){
        
        bottomView.cornerRadius = 20
        
        
         iciValue.text = "\(self.today.todayIciSemi!)"
         chinaValue.text = "\(self.today.todayChinaPsfPrice!)"
         importValue.text = "\(self.today.todayImportOffr!)"
        
        
       
                
        if(self.today.todayIciSemi > self.yesterday.yesterdayIciSemi){
            
            
             iciPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi))% "
             iciPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             iciImg.image = UIImage(named: "UpperArrow")
            self.setIci(chart:  iciChart,color: "green")
            
        }else if(self.today.todayIciSemi < self.yesterday.yesterdayIciSemi){
            
             iciPer.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayIciSemi, b: self.today.todayIciSemi))% "
             iciPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             iciImg.image = UIImage(named: "down")
            self.setIci(chart:  iciChart,color: "red")
            
            
        }else{
            
             iciPer.text = " \(AppUtils.returnPercentage(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi))% "
             iciPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             iciImg.image = UIImage(named: "equal")
            self.setIci(chart:  iciChart,color: "yellow")
        }
                
                
                
                
        
        
        if(self.today.todayChinaPsfPrice > self.yesterday.yesterdayChinaPsfPrice){
            
            
             chinaPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice))% "
             chinaPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             chinaImg.image = UIImage(named: "UpperArrow")
            self.setChina(chart:  chinaChart,color: "green")
            
        }else if(self.today.todayChinaPsfPrice < self.yesterday.yesterdayChinaPsfPrice){
            
             chinaPer.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayChinaPsfPrice, b: self.today.todayChinaPsfPrice))% "
             chinaPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             chinaImg.image = UIImage(named: "down")
            self.setChina(chart:  chinaChart,color: "red")
            
            
        }else{
             chinaPer.text = " \(AppUtils.returnPercentage(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice))% "
             chinaPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             chinaImg.image = UIImage(named: "equal")
            self.setChina(chart:  chinaChart,color: "yellow")
        }
                
                
        
        if(self.today.todayImportOffr > self.yesterday.yesterdayImportOffer){
            
            
             importPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer))% "
             importPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             importImg.image = UIImage(named: "UpperArrow")
            self.setImport(chart:  importChart,color: "green")
            
        }else if(self.today.todayImportOffr < self.yesterday.yesterdayImportOffer){
            
             importPer.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayImportOffer, b: self.today.todayImportOffr))% "
             importPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             importImg.image = UIImage(named: "down")
            self.setImport(chart:  importChart,color: "red")
            
            
        }else{
            
             importPer.text = " \(AppUtils.returnPercentage(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer))% "
             importPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             importImg.image = UIImage(named: "equal")
            self.setImport(chart:  importChart,color: "yellow")
            
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func segmentedControl(_ control: SWSegmentedControl, didSelectItemAtIndex index: Int) {
        selected = index

        if BDefaults.instance.getString(key: "fiber") == "ici"{
            
            if(index == 0){
                setIci(m: week)
                
            }else{                if(month.count > 0){

                setIci(m: month)
                }            }
            
        }else if BDefaults.instance.getString(key: "fiber") == "china"{
            if(index == 0){
                setChina(m: week)
                
            }else{                if(month.count > 0){

                setChina(m: month)
                }            }
        }else if BDefaults.instance.getString(key: "fiber") == "import"{
            if(index == 0){
                setImport(m: week)
                
            }else{
                if(month.count > 0){

                setImport(m: month)
                }            }
        }
        print("did select \(index)")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setIci(m : [GraphDtoWeek] ){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].iciSemi)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setChina(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].chinaPsfPrice)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setImport(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].importOffer)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    @IBAction func openIci(_ sender: Any) {
        
        BDefaults.instance.setString(key: "fiber", value: "ici")
        showDetail()
    }
    @IBAction func openChina(_ sender: Any) {
        BDefaults.instance.setString(key: "fiber", value: "china")
        showDetail()
    }
    @IBAction func openImport(_ sender: Any) {
        BDefaults.instance.setString(key: "fiber", value: "import")
        showDetail()
    }
    
    
   
    func setUpChart(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = false
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        
        xAxis.labelCount = 4

        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
        
  
        
        
        
        
    }
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        if selected == 0 {
            
            
            let formater = ChartFormatter( v: self.week )
            
            self.chart.xAxis.valueFormatter = formater
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        else{
            
            let formater = ChartFormatter( v: AppUtils.month )
            self.chart.xAxis.valueFormatter = formater
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        
        
        
        chart.data = data
        
        
        
    }
    
    
    
    
    func setIci(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].iciSemi)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    func setChina(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].chinaPsfPrice)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    func setImport(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].importOffer)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    
    
}
