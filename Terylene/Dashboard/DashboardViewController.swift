//
//  DashhboardViewController.swift
//  Terylene
//
//  Created by Hashim} on 3/25/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

//class Mobile {
//    var id = 0
//}
//class Nokia: Mobile {
//    var name = "nokia"
//}
//class Iphone: Mobile {
//    var name = "iPhone"
//}


import UIKit
import Alamofire
import SwiftyJSON
import DropDown
class DashboardViewController: UIViewController, UIDropInteractionDelegate {
    
    @IBOutlet var crudeView: UIView!
    @IBOutlet var updatedView: UIView!
    @IBOutlet var iciView: UIView!
    @IBOutlet var cottonView: UIView!
    @IBOutlet var yarnView: UIView!
    /////first section
    @IBOutlet var cnyYesterdayLbl: UILabel!
    @IBOutlet var cnyimage: UIImageView!
    @IBOutlet var cnyTodayLbl: UILabel!
    @IBOutlet var usdYesterdayLbl: UILabel!
    @IBOutlet var usdimage: UIImageView!
    @IBOutlet var usdTodayLbl: UILabel!
    @IBOutlet var usdBtn: UIButton!
    @IBOutlet var firstView: UIView!
    @IBOutlet var cnybtn: UIButton!
    
    @IBOutlet weak var usdTodayUnit: UILabel!
    
    @IBOutlet weak var usdYesterdayUnit: UILabel!
    
    @IBOutlet weak var cnyTodayUnit: UILabel!
    
    @IBOutlet weak var cnyYesterdayUnit: UILabel!
    
    /////////////crude brent
    
    @IBOutlet weak var crudeHeading: UILabel!
    
    @IBOutlet weak var brentHeading: UILabel!
    
    @IBOutlet weak var cudeToday: UILabel!
    
    @IBOutlet weak var crudeImg: UIImageView!
    
    @IBOutlet weak var crudeYesterday: UILabel!
    @IBOutlet weak var brentToday: UILabel!
    
    @IBOutlet weak var brentImg: UIImageView!
    
    
    @IBOutlet weak var brentYesterday: UILabel!
    
    ///////PTA PX MEG
    
    
    @IBOutlet weak var pxHeading: UILabel!
    
    
    @IBOutlet weak var ptaHeading: UILabel!
    
    @IBOutlet weak var megHeading: UILabel!
    
    
    
    @IBOutlet weak var pxToday: UILabel!
    
    @IBOutlet weak var pxImg: UIImageView!
    
    @IBOutlet weak var pxYesterday: UILabel!
    
    @IBOutlet weak var ptaToday: UILabel!
    
    @IBOutlet weak var ptaImg: UIImageView!
    
    @IBOutlet weak var ptaYesterday: UILabel!
    
    @IBOutlet weak var megToday: UILabel!
    
    @IBOutlet weak var megImg: UIImageView!
    
    @IBOutlet weak var megYesterday: UILabel!
    
    ///////ici / china PSF
    
    
    
    @IBOutlet weak var iciHeading: UILabel!
    
    
    @IBOutlet weak var iciToday: UILabel!
    
    @IBOutlet weak var iciYesterday: UILabel!
    
    @IBOutlet weak var chinaToday: UILabel!
    
    @IBOutlet weak var iciImg: UIImageView!
    @IBOutlet weak var chinaYesterday: UILabel!
    
    @IBOutlet weak var chinaImg: UIImageView!
    
    
    @IBOutlet weak var chinaPsf: UIButton!
    
    ///cotton Nyf cents/lb
    @IBOutlet weak var cottonHeading: UILabel!
    
    @IBOutlet weak var cottonToday: UILabel!
    
    @IBOutlet weak var cottonYesterday: UILabel!
    @IBOutlet weak var cottonImg: UIImageView!
    
    @IBOutlet weak var nyfToday: UILabel!
    
    @IBOutlet weak var nyfImg: UIImageView!
    
    @IBOutlet weak var nyfYesterday: UILabel!
    
    @IBOutlet weak var nyfBtn: UIButton!
    
    
    @IBOutlet weak var nyfUnit: UILabel!
    
    @IBOutlet weak var nyfUnit2: UILabel!
    
    ////////yarn
    
    @IBOutlet weak var yarnHeading: UILabel!
    
    @IBOutlet weak var yarn38Today: UILabel!
    @IBOutlet weak var yarn38Img: UIImageView!
    @IBOutlet weak var yarn38Yesterday: UILabel!
    
    @IBOutlet weak var yarn50Today: UILabel!
    
    @IBOutlet weak var yarn50Yesterday: UILabel!
    
    @IBOutlet weak var yarn50Img: UIImageView!
    
    @IBOutlet weak var yarn30Today: UILabel!
    
    @IBOutlet weak var yarn30Img: UIImageView!
    
    @IBOutlet weak var yarn30Yesterday: UILabel!
    
    @IBOutlet weak var yarn36Today: UILabel!
    
    @IBOutlet weak var yarn36Img: UIImageView!
    
    @IBOutlet weak var yarn36Yesterday: UILabel!
    
    
    @IBOutlet weak var yarn20Today: UILabel!
    
    @IBOutlet weak var yarn20Img: UIImageView!
    
    @IBOutlet weak var yarn20Yesterday: UILabel!
    
    @IBOutlet weak var yarn26Today: UILabel!
    
    @IBOutlet weak var yarn26Yesterday: UILabel!
    
    @IBOutlet weak var yarn26Img: UIImageView!
    
    
    var dailydata : DashboardDailyDtoToday!
    var yesterdaydata : DashboardDailyDtoYesterday!
    
    var dashboardmodel = DashboardModel()
    
    
    let dropDown = DropDown()
    let dropDown2 = DropDown()
    let dropDown4 = DropDown()
    let dropDown5 = DropDown()
    var currency1 = AppUtils.getCurrencies()
    var currency2 = AppUtils.getCurrencies()
    var section5 = AppUtils.getSection5()
    var section4 = AppUtils.getSection4()
    // let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setDropDowns()
        
        
        
        
        // UIView or UIBarButtonItem
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(notification:)), name: NSNotification.Name(rawValue: "dailyDashboard"), object: nil)
        
        
        //   appdelegate.showActivityIndicatory(uiView: self.view)
        /// First View Setting
        
        setUpView(view: firstView)
        
        iciView.addInteraction(UIDropInteraction(delegate: self))
        
        
        // Crude View Setting
        setUpView(view: crudeView)
        // update View Setting
        setUpView(view: updatedView)
        // Cotton VIew
        setUpView(view: cottonView)
        // Yarn View
        setUpView(view: yarnView)
        // ICI View
        setUpView(view: iciView)
        
        ///  Usd Btn
        /// CNY Button
        
//        AppUtils.setUpButton(btn: cnybtn)
//        AppUtils.setUpButton(btn: usdBtn)
        
        
        ///  Crude Btn
        
        /// Brent Button
        
        
        
//        AppUtils.setUpLablel(label: brentHeading)
//        AppUtils.setUpLablel(label: crudeHeading)
        
        ///  PX Btn
        
        /// PTA Button
        
        /// MEG Button
        
//        AppUtils.setUpLablel(label: megHeading)
//        AppUtils.setUpLablel(label: ptaHeading)
//        AppUtils.setUpLablel(label: pxHeading)
        /// ICI Semi Dull
//        AppUtils.setUpLablel(label: iciHeading)
        /// China PSF
//        AppUtils.setUpButton(btn: chinaPsf)
        
        /// Cotton
//        AppUtils.setUpLablel(label: cottonHeading)
        /// Nyf
//        AppUtils.setUpButton(btn: nyfBtn)
        //////////yarn
//        AppUtils.setUpLablel(label: yarnHeading)
        
        // Data func Calls
        fetchDashboardData ()
        
    }
    
    
    func setUpView(view : UIView)  {
        view.layer.masksToBounds = false
        view.layer.shadowColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        view.layer.shadowOpacity = 1
        view.layer.shadowOffset = CGSize(width: -1, height: 1)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 0
    }
    
    
    
    @objc func refresh(notification: NSNotification) {
        fetchDashboardData ()
        
    }
    
    
    /// CNY Button Functionality
    @IBAction func cnybtn(_ sender: Any) {
        dropDown2.show()
    }
    
    
    /// USD Button Functionality
    @IBAction func usdBtn(_ sender: Any) {
        dropDown.show()
    }
    
    
    
    func setDropDowns()  {
        currency1.remove(at: 1)
        currency2.remove(at: 0)
        setCNYDropDown()
        setUsdDropDown()
        setDropDown4()
        setDropDown5()
    }
    
    func setUsdDropDown(){
        
        dropDown.anchorView = usdBtn
        dropDown.dataSource = self.currency1
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.usdBtn.setTitle("\(self.currency1[index])", for: .normal)
            self.currency2 = AppUtils.getCurrencies()
            if let index = self.currency2.index(of: self.currency1[index]) {
                self.currency2.remove(at: index)
                /*
                 
                 ,"GBP - £"*/
                
                if self.dailydata != nil{
                    
                    if AppUtils.getCurrencies()[index] == "GBP - £"{
                        
                        self.usdTodayUnit.text = "GBP/PKR"
                        self.usdYesterdayUnit.text = "GBP/PKR"
                        
                        self.usdTodayLbl.text = "\(self.dailydata.todayGbp!)"
                        self.usdYesterdayLbl.text = "\(self.yesterdaydata.yesterdayGbp!)"
                        if self.dailydata.todayGbp == self.yesterdaydata.yesterdayGbp{
                            
                            self.usdimage.image = UIImage(named: "equal")
                        }else if self.dailydata.todayGbp > self.yesterdaydata.yesterdayGbp{
                            self.usdimage.image = UIImage(named: "UpperArrow")
                            
                            
                        }else{
                            self.usdimage.image = UIImage(named: "down")
                        }
                        
                    }
                        
                    else
                        
                        
                        
                        
                        if AppUtils.getCurrencies()[index] == "JPY - ¥"{
                            
                            self.usdTodayUnit.text = "JPY/PKR"
                            self.usdYesterdayUnit.text = "JPY/PKR"
                            
                            self.usdTodayLbl.text = "\(self.dailydata.todayJpy!)"
                            self.usdYesterdayLbl.text = "\(self.yesterdaydata.yesterdayJpy!)"
                            if self.dailydata.todayJpy == self.yesterdaydata.yesterdayJpy{
                                
                                self.usdimage.image = UIImage(named: "equal")
                            }else if self.dailydata.todayJpy > self.yesterdaydata.yesterdayJpy{
                                self.usdimage.image = UIImage(named: "UpperArrow")
                                
                                
                            }else{
                                self.usdimage.image = UIImage(named: "down")
                            }
                            
                        }
                            
                        else
                            
                            
                            if AppUtils.getCurrencies()[index] == "EUR - €"{
                                
                                self.usdTodayUnit.text = "EUR/PKR"
                                self.usdYesterdayUnit.text = "EUR/PKR"
                                
                                self.usdTodayLbl.text = "\(self.dailydata.todayEur!)"
                                self.usdYesterdayLbl.text = "\(self.yesterdaydata.yesterdayEur!)"
                                if self.dailydata.todayEur == self.yesterdaydata.yesterdayEur{
                                    
                                    self.usdimage.image = UIImage(named: "equal")
                                }else if self.dailydata.todayEur > self.yesterdaydata.yesterdayEur{
                                    self.usdimage.image = UIImage(named: "UpperArrow")
                                    
                                    
                                }else{
                                    self.usdimage.image = UIImage(named: "down")
                                }
                                
                            }
                                
                            else
                                
                                
                                
                                if AppUtils.getCurrencies()[index] == "CNY - ¥"{
                                    self.usdTodayUnit.text = "CNY/PKR"
                                    self.usdYesterdayUnit.text = "CNY/PKR"
                                    self.usdTodayLbl.text = "\(self.dailydata.todayCyn!)"
                                    self.usdYesterdayLbl.text = "\(self.yesterdaydata.yesterdayCyn!)"
                                    if self.dailydata.todayCyn == self.yesterdaydata.yesterdayCyn{
                                        
                                        self.usdimage.image = UIImage(named: "equal")
                                    }else if self.dailydata.todayCyn > self.yesterdaydata.yesterdayCyn{
                                        self.usdimage.image = UIImage(named: "UpperArrow")
                                        
                                        
                                    }else{
                                        self.usdimage.image = UIImage(named: "down")
                                    }
                                    
                                }
                                    
                                else if AppUtils.getCurrencies()[index] == "USD - $"{
                                    self.usdTodayUnit.text = "USD/PKR"
                                    self.usdYesterdayUnit.text = "USD/PKR"
                                    self.usdTodayLbl.text = "\(self.dailydata.todayUsd!)"
                                    self.usdYesterdayLbl.text = "\(self.yesterdaydata.yesterdayUsd!)"
                                    if self.dailydata.todayUsd == self.yesterdaydata.yesterdayUsd{
                                        
                                        self.usdimage.image = UIImage(named: "equal")
                                    }else if self.dailydata.todayUsd > self.yesterdaydata.yesterdayUsd{
                                        self.usdimage.image = UIImage(named: "UpperArrow")
                                        
                                        
                                    }else{
                                        self.usdimage.image = UIImage(named: "down")
                                    }
                                    
                    }
                    
                }
                
            }
            
            self.setCNYDropDown()
            
        }
        
    }
    
    
    
    
    func setCNYDropDown(){
        
        
        dropDown2.anchorView = cnybtn
        dropDown2.dataSource = self.currency2
        dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.cnybtn.setTitle("\(self.currency2[index])", for: .normal)
            
            self.currency1 = AppUtils.getCurrencies()
            if let index = self.currency1.index(of: self.currency2[index]) {
                self.currency1.remove(at: index)
            }
            
            if self.dailydata != nil{
                
                
                if self.currency2[index] == "GBP - £"{
                    
                    self.cnyTodayUnit.text = "GBP/PKR"
                    self.cnyYesterdayLbl.text = "GBP/PKR"
                    
                    self.cnyTodayLbl.text = "\(self.dailydata.todayGbp!)"
                    self.cnyYesterdayLbl.text = "\(self.yesterdaydata.yesterdayGbp!)"
                    if self.dailydata.todayGbp == self.yesterdaydata.yesterdayGbp{
                        
                        self.cnyimage.image = UIImage(named: "equal")
                    }else if self.dailydata.todayGbp > self.yesterdaydata.yesterdayGbp{
                        self.cnyimage.image = UIImage(named: "UpperArrow")
                        
                        
                    }else{
                        self.cnyimage.image = UIImage(named: "down")
                    }
                    
                }
                    
                else
                    
                    
                    
                    
                    if self.currency2[index] == "JPY - ¥"{
                        
                        self.cnyTodayUnit.text = "JPY/PKR"
                        self.cnyYesterdayUnit.text = "JPY/PKR"
                        
                        self.cnyTodayLbl.text = "\(self.dailydata.todayJpy!)"
                        self.cnyYesterdayLbl.text = "\(self.yesterdaydata.yesterdayJpy!)"
                        if self.dailydata.todayJpy == self.yesterdaydata.yesterdayJpy{
                            
                            self.cnyimage.image = UIImage(named: "equal")
                        }else if self.dailydata.todayJpy > self.yesterdaydata.yesterdayJpy{
                            self.cnyimage.image = UIImage(named: "UpperArrow")
                            
                            
                        }else{
                            self.cnyimage.image = UIImage(named: "down")
                        }
                        
                    }
                        
                    else
                        
                        
                        if self.currency2[index] == "EUR - €"{
                            
                            self.cnyTodayUnit.text = "EUR/PKR"
                            self.cnyYesterdayUnit.text = "EUR/PKR"
                            
                            self.cnyTodayLbl.text = "\(self.dailydata.todayEur!)"
                            self.cnyYesterdayLbl.text = "\(self.yesterdaydata.yesterdayEur!)"
                            if self.dailydata.todayEur == self.yesterdaydata.yesterdayEur{
                                
                                self.cnyimage.image = UIImage(named: "equal")
                            }else if self.dailydata.todayEur > self.yesterdaydata.yesterdayEur{
                                self.cnyimage.image = UIImage(named: "UpperArrow")
                                
                                
                            }else{
                                self.cnyimage.image = UIImage(named: "down")
                            }
                            
                        }
                            
                        else
                            
                            
                            
                            if self.currency2[index] == "CNY - ¥"{
                                self.cnyTodayUnit.text = "CNY/PKR"
                                self.cnyYesterdayUnit.text = "CNY/PKR"
                                self.cnyTodayLbl.text = "\(self.dailydata.todayCyn!)"
                                self.cnyYesterdayLbl.text = "\(self.yesterdaydata.yesterdayCyn!)"
                                if self.dailydata.todayCyn == self.yesterdaydata.yesterdayCyn{
                                    
                                    self.cnyimage.image = UIImage(named: "equal")
                                }else if self.dailydata.todayCyn > self.yesterdaydata.yesterdayCyn{
                                    self.cnyimage.image = UIImage(named: "UpperArrow")
                                    
                                    
                                }else{
                                    self.cnyimage.image = UIImage(named: "down")
                                }
                                
                            }
                                
                            else if self.currency2[index] == "USD - $"{
                                self.cnyTodayUnit.text = "USD/PKR"
                                self.cnyYesterdayUnit.text = "USD/PKR"
                                self.cnyTodayLbl.text = "\(self.dailydata.todayUsd!)"
                                self.cnyYesterdayLbl.text = "\(self.yesterdaydata.yesterdayUsd!)"
                                if self.dailydata.todayUsd == self.yesterdaydata.yesterdayUsd{
                                    
                                    self.cnyimage.image = UIImage(named: "equal")
                                }else if self.dailydata.todayUsd > self.yesterdaydata.yesterdayUsd{
                                    self.cnyimage.image = UIImage(named: "UpperArrow")
                                    
                                    
                                }else{
                                    self.cnyimage.image = UIImage(named: "down")
                                }
                                
                }
                
            }
            
            
            self.setUsdDropDown()
            
            
            
        }
        
        
        
    }
    
    
    func setDropDown4(){
        
        dropDown4.anchorView = chinaPsf
        dropDown4.dataSource = self.section4
        dropDown4.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.chinaPsf.setTitle("\(self.section4[index])", for: .normal)
            if self.dailydata != nil{
                
                if(index == 0){
                    self.setChinaPsf()
                }else{
                    self.setImportOffer()
                }
            }
            
            
        }
    }
    
    func setDropDown5(){
        
        dropDown5.anchorView = nyfBtn
        dropDown5.dataSource = self.section5
        dropDown5.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.nyfBtn.setTitle("\(self.section5[index])", for: .normal)
            if self.dailydata != nil{
                
                if(index == 0)
                {
                    self.setNyfCentsLb()
                    
                }else if(index == 1)
                {
                    self.setNyfPkrMaund()
                }else{
                    self.setNyfPkrKg()
                }
                
            }
        }
    }
    
    
    func setCrudeWtiAndBrent(){
        self.cudeToday.text = "\(self.dailydata.todayCrude!)"
        self.crudeYesterday.text = "\(self.yesterdaydata.yesterdayCrude!)"
        if self.dailydata.todayCrude == self.yesterdaydata.yesterdayCrude{
            
            self.crudeImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayCrude > self.yesterdaydata.yesterdayCrude{
            self.crudeImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.crudeImg.image = UIImage(named: "down")
        }
        
        
        
        
        self.brentToday.text = "\(self.dailydata.todayBrent!)"
        self.brentYesterday.text = "\(self.yesterdaydata.yesterdayBrent!)"
        if self.dailydata.todayBrent == self.yesterdaydata.yesterdayBrent{
            
            self.brentImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayBrent > self.yesterdaydata.yesterdayBrent{
            self.brentImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.brentImg.image = UIImage(named: "down")
        }
        
        
        
    }
    
    
    
    
    
    func setPtaPxMeg(){
        self.pxToday.text =
        String(format: "%.1f", self.dailydata.todayPx!)
        self.pxYesterday.text = "\(self.yesterdaydata.yesterdayPx!)"
        if self.dailydata.todayPx == self.yesterdaydata.yesterdayPx{
            
            self.pxImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayPx > self.yesterdaydata.yesterdayPx{
            self.pxImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.pxImg.image = UIImage(named: "down")
        }
        
        
        
        
        self.ptaToday.text = String(format: "%.1f", self.dailydata.todayPta!)
        self.ptaYesterday.text = "\(self.yesterdaydata.yesterdayPta!)"
        if self.dailydata.todayPta == self.yesterdaydata.yesterdayPta{
            
            self.ptaImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayPta > self.yesterdaydata.yesterdayPta{
            self.ptaImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.ptaImg.image = UIImage(named: "down")
        }
        
        
        
        
        self.megToday.text = String(format: "%.1f", self.dailydata.todayMeg!)
        self.megYesterday.text = "\(self.yesterdaydata.yesterdayMeg!)"
        if self.dailydata.todayMeg == self.yesterdaydata.yesterdayMeg{
            
            self.megImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayMeg > self.yesterdaydata.yesterdayMeg{
            self.megImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.megImg.image = UIImage(named: "down")
        }
        
        
    }
    
    
    func setFirstData(){
        
        self.usdTodayLbl.text = "\(self.dailydata.todayUsd ?? 0)"
        self.usdYesterdayLbl.text = "\(self.yesterdaydata.yesterdayUsd ?? 0)"
        if self.dailydata.todayUsd == self.yesterdaydata.yesterdayUsd{
            
            self.usdimage.image = UIImage(named: "equal")
        }else if self.dailydata.todayUsd > self.yesterdaydata.yesterdayUsd{
            self.usdimage.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.usdimage.image = UIImage(named: "down")
        }
        
        
        
        
        self.cnyTodayLbl.text = "\(self.dailydata.todayCyn!)"
        self.cnyYesterdayLbl.text = "\(self.yesterdaydata.yesterdayCyn!)"
        if self.dailydata.todayCyn == self.yesterdaydata.yesterdayCyn{
            
            self.cnyimage.image = UIImage(named: "equal")
        }else if self.dailydata.todayCyn > self.yesterdaydata.yesterdayCyn{
            self.cnyimage.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.cnyimage.image = UIImage(named: "down")
        }
        
        
        
        
        
        
    }
    
    
    
    func setIciSemiDull(){
        self.iciToday.text = "\(self.dailydata.todayIciSemi!)"
        self.iciYesterday.text = "\(self.yesterdaydata.yesterdayIciSemi!)"
        if self.dailydata.todayIciSemi == self.yesterdaydata.yesterdayIciSemi{
            
            self.iciImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayIciSemi > self.yesterdaydata.yesterdayIciSemi{
            self.iciImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.iciImg.image = UIImage(named: "down")
        }
    }
    func setChinaPsf(){
        self.chinaToday.text = "\(self.dailydata.todayChinaPsfPrice!)"
        self.chinaYesterday.text = "\(self.yesterdaydata.yesterdayChinaPsfPrice!)"
        if self.dailydata.todayChinaPsfPrice == self.yesterdaydata.yesterdayChinaPsfPrice{
            
            self.chinaImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayChinaPsfPrice > self.yesterdaydata.yesterdayChinaPsfPrice{
            self.chinaImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.chinaImg.image = UIImage(named: "down")
        }
    }
    
    func setImportOffer(){
        self.chinaToday.text = "\(self.dailydata.todayImportOffr!)"
        self.chinaYesterday.text = "\(self.yesterdaydata.yesterdayImportOffer!)"
        if self.dailydata.todayImportOffr == self.yesterdaydata.yesterdayImportOffer{
            
            self.chinaImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayImportOffr > self.yesterdaydata.yesterdayImportOffer{
            self.chinaImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.chinaImg.image = UIImage(named: "down")
        }
    }
    
    
    
    func setCotton(){
        self.cottonToday.text = "\(self.dailydata.todayCottonMaund!)"
        self.cottonYesterday.text = "\(self.yesterdaydata.yesterdayCottonMaund!)"
        if self.dailydata.todayCottonMaund == self.yesterdaydata.yesterdayCottonMaund{
            
            self.cottonImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayCottonMaund > self.yesterdaydata.yesterdayCottonMaund{
            self.cottonImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.cottonImg.image = UIImage(named: "down")
        }
    }
    
    
    
    func setNyfCentsLb(){
        
        
        self.nyfUnit.text = "cents/lb"
        self.nyfUnit2.text = "cents/lb"
        
        self.nyfToday.text = "\(self.dailydata.todayNyf!)"
        self.nyfYesterday.text = "\(self.yesterdaydata.yesterdayNyf!)"
        if self.dailydata.todayNyf == self.yesterdaydata.yesterdayNyf{
            
            self.nyfImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayNyf > self.yesterdaydata.yesterdayNyf{
            self.nyfImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.nyfImg.image = UIImage(named: "down")
        }
    }
    
    func setNyfPkrMaund(){
        
        self.nyfUnit.text = "PKR/maund"
        self.nyfUnit2.text = "PKR/maund"
        
        
        self.nyfToday.text = "\(self.dailydata.todayNyfMaund!)"
        self.nyfYesterday.text = "\(self.yesterdaydata.yesterdayNyfMaund!)"
        if self.dailydata.todayNyfMaund == self.yesterdaydata.yesterdayNyfMaund{
            
            self.nyfImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayNyfMaund > self.yesterdaydata.yesterdayNyfMaund{
            self.nyfImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.nyfImg.image = UIImage(named: "down")
        }
    }
    
    func setNyfPkrKg(){
        
        
        self.nyfUnit.text = "PKR/Kg"
        self.nyfUnit2.text = "PKR/Kg"
        
        self.nyfToday.text = "\(self.dailydata.todayNyfPkr!)"
        self.nyfYesterday.text = "\(self.yesterdaydata.yesterdayNyfPkr!)"
        if self.dailydata.todayNyfPkr == self.yesterdaydata.yesterdayNyfPkr{
            
            self.nyfImg.image = UIImage(named: "equal")
        }else if self.dailydata.todayNyfPkr > self.yesterdaydata.yesterdayNyfPkr{
            self.nyfImg.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.nyfImg.image = UIImage(named: "down")
        }
    }
    
    
    
    func setYarnData(){
        
        /////yarn 50
        self.yarn50Today.text = "\(self.dailydata.todayYarn50Pp!)"
        self.yarn50Yesterday.text = "\(self.yesterdaydata.yesterdayYarn50Pp!)"
        if self.dailydata.todayYarn50Pp == self.yesterdaydata.yesterdayYarn50Pp{
            
            self.yarn50Img.image = UIImage(named: "equal")
        }else if self.dailydata.todayYarn50Pp > self.yesterdaydata.yesterdayYarn50Pp{
            self.yarn50Img.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.yarn50Img.image = UIImage(named: "down")
        }
        
        
        /////yarn 38
        self.yarn38Today.text = "\(self.dailydata.todayYarn38Pc!)"
        self.yarn38Yesterday.text = "\(self.yesterdaydata.yesterdayYarn38Pc!)"
        if self.dailydata.todayYarn38Pc == self.yesterdaydata.yesterdayYarn38Pc{
            
            self.yarn38Img.image = UIImage(named: "equal")
        }else if self.dailydata.todayYarn38Pc > self.yesterdaydata.yesterdayYarn38Pc{
            self.yarn38Img.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.yarn38Img.image = UIImage(named: "down")
        }
        
        
        
        /////yarn 36
        self.yarn36Today.text = "\(self.dailydata.todayYarn36Pc!)"
        self.yarn36Yesterday.text = "\(self.yesterdaydata.yesterdayYarn36Pc!)"
        if self.dailydata.todayYarn36Pc == self.yesterdaydata.yesterdayYarn36Pc{
            
            self.yarn36Img.image = UIImage(named: "equal")
        }else if self.dailydata.todayYarn36Pc > self.yesterdaydata.yesterdayYarn36Pc{
            self.yarn36Img.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.yarn36Img.image = UIImage(named: "down")
        }
        
        
        /////yarn 30
        self.yarn30Today.text = "\(self.dailydata.todayYarn30Pc!)"
        self.yarn30Yesterday.text = "\(self.yesterdaydata.yesterdayYarn30Pc!)"
        if self.dailydata.todayYarn30Pc == self.yesterdaydata.yesterdayYarn30Pc{
            
            self.yarn30Img.image = UIImage(named: "equal")
        }else if self.dailydata.todayYarn30Pc > self.yesterdaydata.yesterdayYarn30Pc{
            self.yarn30Img.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.yarn30Img.image = UIImage(named: "down")
        }
        
        
        /////yarn 26
        self.yarn26Today.text = "\(self.dailydata.todayYarn26Pc!)"
        self.yarn26Yesterday.text = "\(self.yesterdaydata.yesterdayYarn26Pc!)"
        if self.dailydata.todayYarn26Pc == self.yesterdaydata.yesterdayYarn26Pc{
            
            self.yarn26Img.image = UIImage(named: "equal")
        }else if self.dailydata.todayYarn26Pc > self.yesterdaydata.yesterdayYarn26Pc{
            self.yarn26Img.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.yarn26Img.image = UIImage(named: "down")
        }
        
        
        
        /////yarn 20
        self.yarn20Today.text = "\(self.dailydata.todayYarn20S!)"
        self.yarn20Yesterday.text = "\(self.yesterdaydata.yesterdayYarn20S!)"
        if self.dailydata.todayYarn20S == self.yesterdaydata.yesterdayYarn20S{
            
            self.yarn20Img.image = UIImage(named: "equal")
        }else if self.dailydata.todayYarn20S > self.yesterdaydata.yesterdayYarn20S{
            self.yarn20Img.image = UIImage(named: "UpperArrow")
            
            
        }else{
            self.yarn20Img.image = UIImage(named: "down")
        }
        
        
        
        
        
    }
    
    
    
    /// NetWork Call For Api Integration
    func fetchDashboardData () {
        if(BDefaults.instance.getDashboardDaily().count > 0){
            
            self.dailydata = BDefaults.instance.getDashboardDaily()[0].today
            self.yesterdaydata = BDefaults.instance.getDashboardDaily()[0].yesterday
            
            if(BDefaults.instance.getDashboardDaily()[0].today != nil)
            {
                self.setFirstData()
                self.setCrudeWtiAndBrent()
                self.setIciSemiDull()
                self.setChinaPsf()
                self.setPtaPxMeg()
                self.setCotton()
                self.setNyfCentsLb()
                self.setYarnData()
            }
        }
        
        
    }
    
    
    @IBAction func openChinaDropDown(_ sender: Any) {
        
        dropDown4.show()
        
        
        
    }
    
    
    @IBAction func openNyf(_ sender: Any) {
        
        dropDown5.show()
    }
    
    
}



