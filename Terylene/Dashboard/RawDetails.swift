//
//  RawDetails.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 16/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SWSegmentedControl
import Charts
class RawDetails: UIViewController ,SWSegmentedControlDelegate{
    
    @IBOutlet weak var cYesterday: UILabel!
    
    @IBOutlet weak var ptaImg: UIImageView!
    @IBOutlet weak var ptaPer: UILabel!
    @IBOutlet weak var ptaValue: UILabel!
    
    var selected = 0

    
    @IBOutlet weak var megImg: UIImageView!
    @IBOutlet weak var megPer: UILabel!
    @IBOutlet weak var megValue: UILabel!
    
    
    
    
    
    
    @IBOutlet weak var pxImg: UIImageView!
    @IBOutlet weak var pxPer: UILabel!
    @IBOutlet weak var pxValue: UILabel!
    
    @IBOutlet weak var megChart: LineChartView!
    @IBOutlet weak var ptaChart: LineChartView!
    
    @IBOutlet weak var pxChart: LineChartView!
    
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var cArrow: UIImageView!
    @IBOutlet weak var cChanges: UILabel!
    @IBOutlet weak var cView: UIView!
    
    @IBOutlet weak var cValue: UILabel!
    @IBOutlet weak var cHeader: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var chartDate: UILabel!
    
    @IBOutlet weak var cPercent: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var sc : SWSegmentedControl!
    
    var month = BDefaults.instance.getGraphMonth()
    var week = BDefaults.instance.getGraphWeekly()
    let today = BDefaults.instance.getDashboardDaily()[0].today!
    let yesterday =  BDefaults.instance.getDashboardDaily()[0].yesterday!
    
    var todayRaw = BDefaults.instance.getDashboardDaily()[0].todayRaw!
    var yesterdayRaw = BDefaults.instance.getDashboardDaily()[0].yesterdayRaw!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        chartDate.text = AppUtils.getGraphDate()
        sc = SWSegmentedControl(items: ["1W", "1M"])
        sc.delegate = self
        sc.font = UIFont(name: "SFProDisplay-Semibold", size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        sc.indicatorColor = UIColor.init(cgColor: AppUtils.lightColor)
        sc.badgeColor = UIColor.init(cgColor: AppUtils.lightColor)
        
        sc.indicatorThickness = CGFloat(3)
        date.text = AppUtils.getCurrentTimeShort()

        sc.frame = CGRect(x: 0, y: -3, width: tabView.frame.width, height: 44)
        tabView.addSubview(sc)
        selected = 0

        showBottomDetail()
        showDetail()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func showDetail(){
        selected = 0

        if BDefaults.instance.getString(key: "raw") == "px"{
            sc.setSelectedSegmentIndex(0)
            cHeader.text = "-PX"
            setPx()
            setPx(m: week)
        }else if BDefaults.instance.getString(key: "raw") == "pta"{
            setPta()
            cHeader.text = "-PTA"
            setPta(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        else if BDefaults.instance.getString(key: "raw") == "meg"{
            setMeg()
            cHeader.text = "-MEG"
            setMeg(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        
    }
    
    
    
    func setPx(){
        
        cValue.text = "\(self.todayRaw.todayPx!)"
        cChanges.text = " \(AppUtils.sub(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx)) "
        
        cYesterday.text = "\(self.yesterdayRaw.yesterdayPx!)"

        
        
        ///Wti
        if(self.todayRaw.todayPx > self.yesterdayRaw.yesterdayPx){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            
            
        }else if(self.todayRaw.todayPx < self.yesterdayRaw.yesterdayPx){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayPx, b: self.todayRaw.todayPx))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            
        }
    }
    
    func setPta(){
        
        cValue.text = "\(self.todayRaw.todayPta!)"
        cChanges.text = " \(AppUtils.sub(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta)) "
        
        cYesterday.text = "\(self.yesterdayRaw.yesterdayPta!)"

        
        if(self.todayRaw.todayPta > self.yesterdayRaw.yesterdayPta){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else  if(self.todayRaw.todayPta < self.yesterdayRaw.yesterdayPta){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayPta, b: self.todayRaw.todayPta))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    
    func setMeg(){
        
        cValue.text = "\(self.todayRaw.todayMeg!)"
        cChanges.text = " \(AppUtils.sub(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg)) "
        
            cYesterday.text = "\(self.yesterdayRaw.yesterdayMeg!)"

        
        if(self.todayRaw.todayMeg > self.yesterdayRaw.yesterdayMeg){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else  if(self.todayRaw.todayMeg < self.yesterdayRaw.yesterdayMeg){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayMeg, b: self.todayRaw.todayMeg))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
    
    
    
    func showBottomDetail(){
        
        bottomView.cornerRadius = 20
         pxValue.text = "\(self.todayRaw.todayPx!)"
         ptaValue.text = "\(self.todayRaw.todayPta!)"
         megValue.text = "\(self.todayRaw.todayMeg!)"
        
       
        
     
                
                if self.todayRaw.todayPx > self.yesterdayRaw.yesterdayPx {
                    
                    
                     pxPer.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx))% "
                     pxPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                     pxImg.image = UIImage(named: "UpperArrow")
                    self.setPx(chart:  pxChart,color: "green")
                    
                }else if self.todayRaw.todayPx < self.yesterdayRaw.yesterdayPx{
                    
                     pxPer.text = "-\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayPx, b: self.todayRaw.todayPx))% "
                     pxPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                     pxImg.image =  UIImage(named: "down")
                    self.setPx(chart:  pxChart,color: "red")
                    
                    
                }else{
                     pxPer.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx))% "
                     pxPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                     pxImg.image = UIImage(named: "equal")
                    self.setPx(chart:  pxChart,color: "yellow")
                }
                
                
                
                
                if(self.todayRaw.todayPta > self.yesterdayRaw.yesterdayPta) {
                    
                    
                     ptaPer.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta))% "
                     ptaPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                     ptaImg.image = UIImage(named: "UpperArrow")
                    self.setPta(chart:  ptaChart,color: "green")
                    
                }else if(self.todayRaw.todayPta < self.yesterdayRaw.yesterdayPta){
                    
                     ptaPer.text = "-\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayPta, b: self.todayRaw.todayPta))% "
                     ptaPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                     ptaImg.image = UIImage(named: "down")
                    self.setPta(chart:  ptaChart,color: "red")
                    
                    
                }else{
                     ptaPer.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta))% "
                     ptaPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                     ptaImg.image = UIImage(named: "equal")
                    self.setPta(chart:  ptaChart,color: "yellow")
                }
                
                
                if(self.todayRaw.todayMeg > self.yesterdayRaw.yesterdayMeg){
                    
                    
                     megPer.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg))% "
                     megPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                     megImg.image = UIImage(named: "UpperArrow")
                    self.setMeg(chart:  megChart,color: "green")
                    
                }else   if(self.todayRaw.todayMeg < self.yesterdayRaw.yesterdayMeg){
                    
                     megPer.text = "-\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayMeg, b: self.todayRaw.todayMeg))% "
                     megPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                     megImg.image = UIImage(named: "down")
                    self.setMeg(chart:  megChart,color: "red")
                    
                    
                }else{
                     megPer.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg))% "
                     megPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                     megImg.image = UIImage(named: "equal")
                    self.setMeg(chart:  megChart,color: "yellow")
                }
                
        
                
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func segmentedControl(_ control: SWSegmentedControl, didSelectItemAtIndex index: Int) {
        selected = index

        if BDefaults.instance.getString(key: "raw") == "px"{
            
            if(index == 0){
                setPx(m: week)
                
            }else{
                setPx(m: month)
            }
            
        }else if BDefaults.instance.getString(key: "raw") == "pta"{
            if(index == 0){
                setPta(m: week)
                
            }else{
                setPta(m: month)
            }
        }else if BDefaults.instance.getString(key: "raw") == "meg"{
            if(index == 0){
                setMeg(m: week)
                
            }else{
                setMeg(m: month)
            }
        }
        print("did select \(index)")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setPx(m : [GraphDtoWeek] ){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].px)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setPta(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].pta)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setMeg(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].meg)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    @IBAction func openPx(_ sender: Any) {
        
        BDefaults.instance.setString(key: "raw", value: "px")
        showDetail()
    }
    @IBAction func openPta(_ sender: Any) {
        BDefaults.instance.setString(key: "raw", value: "pta")
        showDetail()
    }
    @IBAction func openMeg(_ sender: Any) {
        BDefaults.instance.setString(key: "raw", value: "meg")
        showDetail()
    }
    
    
    
   
    func setUpChart(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = false
        
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        
        xAxis.labelCount = 4

        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
      
        
        
        
        
    }
   
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        if selected == 0 {
            
            
            let formater = ChartFormatter( v: self.week )
            
            self.chart.xAxis.valueFormatter = formater
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        else{
            
            let formater = ChartFormatter( v: AppUtils.month )
            self.chart.xAxis.valueFormatter = formater
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        
        
        chart.data = data
        
        
        
    }
    
    
    
    
    func setPx(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].px)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    func setPta(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].pta)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    func setMeg(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].meg)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    
    
}
