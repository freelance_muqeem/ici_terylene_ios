//
//  PtaMegPurchaseView.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 24/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class PtaMegPurchaseView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
     @IBAction func openPurchases(_ sender: Any) {
        
        BDefaults.instance.setString(key: "navig", value: "pta")
        
        performSegue(withIdentifier: "openPurchaingview", sender: self)
     }
   /*  // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
