//
//  PtaPXCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 11/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
class PtaPXCell: UITableViewCell {
    @IBOutlet weak var lockView: UIView!
    @IBOutlet weak var lockedDate: UILabel!
    
    @IBOutlet weak var openPx: UIView!
    @IBOutlet weak var openPta: UIView!
     @IBOutlet weak var openMeg: UIView!
    
    @IBOutlet weak var openUnlock: UIButton!
    
    
    @IBOutlet weak var ptaImg: UIImageView!
    @IBOutlet weak var ptaPer: UILabel!
    @IBOutlet weak var ptaValue: UILabel!
    
    
    
    @IBOutlet weak var megImg: UIImageView!
    @IBOutlet weak var megPer: UILabel!
    @IBOutlet weak var megValue: UILabel!
    
    
    
    
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var pxImg: UIImageView!
    @IBOutlet weak var pxPer: UILabel!
    @IBOutlet weak var pxValue: UILabel!
    
    @IBOutlet weak var megChart: LineChartView!
    @IBOutlet weak var cDate: UILabel!
    @IBOutlet weak var ptaChart: LineChartView!
    
    @IBOutlet weak var pxChart: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBAction func openUnlock(_ sender: Any) {
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
