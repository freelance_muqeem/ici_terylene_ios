//
//  PtaPXCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 11/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
class CrudeCell: UITableViewCell {

    
    @IBOutlet weak var bgView :
    UIView!
    
    @IBOutlet weak var brentImg: UIImageView!
    @IBOutlet weak var brentPercent: UILabel!
    @IBOutlet weak var brentValue: UILabel!
    @IBOutlet weak var wtiImg: UIImageView!
    @IBOutlet weak var wtiPercent: UILabel!
    @IBOutlet weak var wtivalue: UILabel!
    @IBOutlet weak var brentChart: LineChartView!
    @IBOutlet weak var wtiChart: LineChartView!
    
    
    @IBOutlet weak var openBrent: UIView!
    
    @IBOutlet weak var openWti: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
