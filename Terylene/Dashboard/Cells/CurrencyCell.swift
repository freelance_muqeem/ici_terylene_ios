//
//  CurrencyCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 11/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {

    @IBOutlet weak var usdPkrView: UIView!
    @IBOutlet weak var usdPkrValue: UILabel!
    
    @IBOutlet weak var usdPkrPer: UILabel!
    
    @IBOutlet weak var usdPkrImg: UIImageView!
    //////CNY PKr
    @IBOutlet weak var cnyPkrView: UIView!
    
    
    @IBOutlet weak var cnyPkrValue: UILabel!
    
    @IBOutlet weak var cnyPkrPer: UILabel!
    
    @IBOutlet weak var cnyPkrImg: UIImageView!
    ////////GBP
    
    @IBOutlet weak var gbpView: UIView!
    
    @IBOutlet weak var gbpValue: UILabel!
    
    @IBOutlet weak var gbpImg: UIImageView!
    @IBOutlet weak var gbpPerc: UILabel!
    
    ////////EUR
    
    @IBOutlet weak var eurView: UIView!
    
    @IBOutlet weak var eurValue: UILabel!
    
    @IBOutlet weak var eurPercent: UILabel!
    
    @IBOutlet weak var eurImg: UIImageView!
    
    /////////JPY
    @IBOutlet weak var jpyView: UIView!
    
    @IBOutlet weak var jpyValue: UILabel!
    
    @IBOutlet weak var jpyPerc: UILabel!
    
    @IBOutlet weak var jpyImg: UIImageView!
    
    
    @IBOutlet weak var bgView: UIView!
    
    
    @IBOutlet weak var openUsd: UIView!
    
    @IBOutlet weak var openCny: UIView!
    
    @IBOutlet weak var openGbp: UIView!
    
    @IBOutlet weak var openEur: UIView!
    
    @IBOutlet weak var openJpy: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
