//
//  PtaPXCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 11/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
class FiberCell: UITableViewCell {

    @IBOutlet weak var openIci: UIView!
    @IBOutlet weak var openChina: UIView!
    @IBOutlet weak var openImport: UIView!
    
    
    @IBOutlet weak var iciImg: UIImageView!
    @IBOutlet weak var iciPer: UILabel!
    @IBOutlet weak var iciValue: UILabel!
    
    
    
    @IBOutlet weak var chinaImg: UIImageView!
    @IBOutlet weak var chinaPer: UILabel!
    @IBOutlet weak var chinaValue: UILabel!
    
    
    
    
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var importImg: UIImageView!
    @IBOutlet weak var importPer: UILabel!
    @IBOutlet weak var importValue: UILabel!
    
    
    
    
    
    
    
    
    
    @IBOutlet weak var importChart: LineChartView!
    @IBOutlet weak var chinaChart: LineChartView!
    @IBOutlet weak var iciChart: LineChartView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
