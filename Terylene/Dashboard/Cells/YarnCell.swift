//
//  PtaPXCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 11/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
class YarnCell: UITableViewCell {
    
    @IBOutlet weak var openYarn26: UIView!
    @IBOutlet weak var openYarn36: UIView!
    @IBOutlet weak var openYarn20: UIView!
    @IBOutlet weak var openYarn50: UIView!
    
    @IBOutlet weak var openYarn30: UIView!
    
    @IBOutlet weak var openyarn38: UIView!
    
    
    
    
    
    @IBOutlet weak var yar30Img: UIImageView!
    @IBOutlet weak var yar30Per: UILabel!
    @IBOutlet weak var yar30Value: UILabel!
    
    
    
    @IBOutlet weak var yar50Img: UIImageView!
    @IBOutlet weak var yar50Per: UILabel!
    @IBOutlet weak var yar50Value: UILabel!
    
    
    
    
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var yar36Img: UIImageView!
    @IBOutlet weak var yar36Per: UILabel!
    @IBOutlet weak var yar36Value: UILabel!
    
    
    @IBOutlet weak var yar38Img: UIImageView!
    @IBOutlet weak var yar38Per: UILabel!
    @IBOutlet weak var yar38Value: UILabel!
    
    
    
    @IBOutlet weak var yar26Img: UIImageView!
    @IBOutlet weak var yar26Per: UILabel!
    @IBOutlet weak var yar26Value: UILabel!
    
    
    
    
    
    
    @IBOutlet weak var yar20Img: UIImageView!
    @IBOutlet weak var yar20Per: UILabel!
    @IBOutlet weak var yar20Value: UILabel!
    
    
    
    
    
    
    
    @IBOutlet weak var yarn38Chart: LineChartView!
    @IBOutlet weak var yarn30Chart: LineChartView!
    @IBOutlet weak var yarn50Chart: LineChartView!
    @IBOutlet weak var yarn20Chart: LineChartView!
    @IBOutlet weak var yarn36Chart: LineChartView!
    @IBOutlet weak var yarn26Chart: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
