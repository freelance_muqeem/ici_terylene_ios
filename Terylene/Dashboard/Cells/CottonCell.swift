//
//  PtaPXCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 11/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
class CottonCell: UITableViewCell {
    
    
    
    @IBOutlet weak var openCotton: UIView!
    
    @IBOutlet weak var openNyf: UIView!
    
    @IBOutlet weak var bgView :
    UIView!
    
    @IBOutlet weak var nyfImg: UIImageView!
    @IBOutlet weak var nyfPercent: UILabel!
    @IBOutlet weak var nyfValue: UILabel!
    @IBOutlet weak var cottonImg: UIImageView!
    @IBOutlet weak var cottonPercent: UILabel!
    @IBOutlet weak var cottonValue: UILabel!
    
    
    
    @IBOutlet weak var nyfChart: LineChartView!
    @IBOutlet weak var cottonChart: LineChartView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
