//
//  DashBoardTable.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 10/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Charts
import AVKit
import AVFoundation
import CRRefresh
class DashBoardTable: UIViewController{
    
    @IBOutlet weak var main: UIScrollView!
    
    @IBOutlet weak var newsTitle: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var newsImg: UIImageView!
    @IBOutlet weak var newsView: UIView!
    @IBOutlet weak var dashboardView: UITableView!
    var month = BDefaults.instance.getGraphMonth()
    var today : DashboardDailyDtoToday!
    var yesterday : DashboardDailyDtoYesterday!
    
    var todayRaw : DashboardDailyDtoTodayRaw!
    var yesterdayRaw : DashboardDailyDtoYesterdayRaw!
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    var homePage : HomePageDto!
    
    var array = ["Currency","Crude","Raw","Fiber","Cotton","Yarn","Events"]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(refresh(notification:)), name: NSNotification.Name(rawValue: "dailyDashboard"), object: nil)
        
        
        
        if BDefaults.instance.getDashboardDaily().count > 0 && BDefaults.instance.getDashboardDaily()[0].today != nil &&  BDefaults.instance.getGraphMonth().count > 0  {
            self.today = BDefaults.instance.getDashboardDaily()[0].today
            self.yesterday = BDefaults.instance.getDashboardDaily()[0].yesterday
            
            
            self.todayRaw = BDefaults.instance.getDashboardDaily()[0].todayRaw
            self.yesterdayRaw = BDefaults.instance.getDashboardDaily()[0].yesterdayRaw
            
            self.dashboardView.delegate = self
            self.dashboardView.dataSource = self
        }else{
            appdelegate.showActivityIndicatory(uiView: self.view)
            
            getDataDashboardDaily()
        }
        
        
        
        
        
        main.cr.addHeadRefresh(animator: FastAnimator()) {
            AppUtils.app.refresh()
            
        }
        
        newsView.cornerRadius = 20
        self.dashboardView.backgroundColor = .clear
        
        //        AppUtils.app.showActivityIndicatory(uiView: self.view)
        
        //                AppUtils.app.hideActivityIndicatory()
        
        dateLabel.text = AppUtils.getMainDate()
        
        if BDefaults.instance.getAllNews().count > 0 {
            
            //            newsImg.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(BDefaults.instance.getAllNews()[0].image!)"), placeholderImage: UIImage(named: "picture.png"))
            
            newsTitle.text = BDefaults.instance.getAllNews()[0].heading
        }
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    func getDataDashboardDaily(){
        
        NetworkManager.getHomePageVideo(completion: { (dto) in
            
        }) { (S) in
            
        }
        
        
        
        NetworkManager.getDashboardDaily(completion: { (dto) in
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dailyDashboard"), object: nil)
            
            self.getDashboardMonthly()
            
            
        }) { (s) in
            self.appdelegate.hideActivityIndicatory()
            self.refreshData()
            
        }
        
        
        
        
    }
    func getDashboardMonthly(){
        NetworkManager.getDashboardMonthly(completion: { (dto) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "monthly"), object: nil)
            self.getDashboardWeekly()
            
        }) { (err) in
            self.appdelegate.hideActivityIndicatory()
            self.refreshData()
            
        }
    }
    func getDashboardWeekly(){
        NetworkManager.getDashboardWeekly(completion: { (dto) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "weekly"), object: nil)
            self.refreshData()
            self.appdelegate.hideActivityIndicatory()
            
        }) { (err) in
            self.appdelegate.hideActivityIndicatory()
            self.refreshData()
            
        }
    }
    
    
    
    
    
    
    
    @IBAction func playVideo(_ sender: Any) {
        
        
        if(BDefaults.instance.getHomeVideo().count > 0){
            //",
            let url = URL(string:"https://teryleneapp.com/\(BDefaults.instance.getHomeVideo()[0].video!)")
            let player = AVPlayer(url: url!)
            
            
            let vc = AVPlayerViewController()
            vc.player = player
            player.isMuted = false
            
            self.present(vc, animated: true) { vc.player?.play() }
            
        }
    }
    @objc func refresh(notification: NSNotification) {
        
        
        
        main.cr.endHeaderRefresh()
        refreshData()
        if BDefaults.instance.getAllNews().count > 0 {
            
            //            newsImg.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(BDefaults.instance.getAllNews()[0].image!)"), placeholderImage: UIImage(named: "picture.png"))
            
            newsTitle.text = BDefaults.instance.getAllNews()[0].heading
        }
    }
    
    func refreshData(){
        if BDefaults.instance.getDashboardDaily().count > 0{
            self.today = BDefaults.instance.getDashboardDaily()[0].today
            self.yesterday = BDefaults.instance.getDashboardDaily()[0].yesterday
            self.month = BDefaults.instance.getGraphMonth()
            self.todayRaw = BDefaults.instance.getDashboardDaily()[0].todayRaw
            self.yesterdayRaw = BDefaults.instance.getDashboardDaily()[0].yesterdayRaw
            
            self.dashboardView.delegate = self
            self.dashboardView.dataSource = self
            
            DispatchQueue.main.async {
                self.dashboardView.reloadData()
            }
        }
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func openNews(_ sender: Any) {
        performSegue(withIdentifier: "openNewsHome", sender: self)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension DashBoardTable:UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if array[indexPath.row] == "Events"{
            if BDefaults.instance.getAllEvents().count > 0 {
                BDefaults.instance.saveEvents(user: BDefaults.instance.getAllEvents()[0])
                performSegue(withIdentifier: "openEventDetails", sender: self)
            }
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        
        if(array[indexPath.row] == "Currency"){
            
            
            
            self.dashboardView.register(UINib(nibName: "CurrencyCell", bundle: nil), forCellReuseIdentifier: "currencyCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "currencyCell", for: indexPath) as! CurrencyCell
            
            
            
            cell.openUsd.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openUsd)))
            cell.openCny.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openCny)))
            cell.openEur.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openEur)))
            cell.openGbp.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openGbp)))
            cell.openJpy.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openJpy)))
            
            
            
            
            cell.usdPkrValue.text = "\(self.today.todayUsd!)"
            cell.cnyPkrValue.text = "\(self.today.todayCyn!)"
            cell.eurValue.text = "\(self.today.todayEur!)"
            cell.jpyValue.text = "\(self.today.todayJpy!)"
            cell.gbpValue.text = "\(self.today.todayGbp!)"
            
            cell.bgView.cornerRadius = 20
            cell.backgroundColor = .clear
            cell.backgroundView = UIView()
            cell.layer.backgroundColor = UIColor.clear.cgColor
            
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    
                    ///usd
                    if(self.today.todayUsd > self.yesterday.yesterdayUsd){
                        
                        
                        
                        cell.usdPkrPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayUsd, b: self.yesterday.yesterdayUsd))% "
                        cell.usdPkrPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.usdPkrView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.usdPkrImg.image = UIImage(named: "UpperArrow")
                        
                        
                    }else if(self.today.todayUsd < self.yesterday.yesterdayUsd){
                        
                        cell.usdPkrPer.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayUsd, b: self.today.todayUsd))% "
                        cell.usdPkrPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.usdPkrView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.usdPkrImg.image = UIImage(named: "down")
                        
                        
                    }else{
                        
                        cell.usdPkrPer.text = " -\(AppUtils.returnPercentage(a:  self.today.todayUsd, b:self.yesterday.yesterdayUsd))% "
                        cell.usdPkrPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.usdPkrView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.usdPkrImg.image = UIImage(named: "equal")
                        
                        
                    }
                    
                    ///CNY
                    
                    if(self.today.todayCyn > self.yesterday.yesterdayCyn){
                        
                        
                        cell.cnyPkrPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn))% "
                        cell.cnyPkrPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.cnyPkrView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.cnyPkrImg.image = UIImage(named: "UpperArrow")
                        
                        
                    }else if(self.today.todayCyn < self.yesterday.yesterdayCyn){
                        
                        cell.cnyPkrPer.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCyn, b: self.today.todayCyn))% "
                        cell.cnyPkrPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.cnyPkrView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.cnyPkrImg.image = UIImage(named: "down")
                        
                        
                    } else{
                        cell.cnyPkrPer.text = " \(AppUtils.returnPercentage(a: self.today.todayCyn, b: self.yesterday.yesterdayCyn))% "
                        cell.cnyPkrPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.cnyPkrView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.cnyPkrImg.image = UIImage(named: "equal")
                    }
                    
                    //////GBP
                    
                    if(self.today.todayGbp > self.yesterday.yesterdayGbp ){
                        
                        
                        cell.gbpPerc.text = " +\(AppUtils.returnPercentage(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp))% "
                        cell.gbpPerc.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.gbpView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.gbpImg.image = UIImage(named: "UpperArrow")
                        
                        
                    }else if(self.today.todayGbp < self.yesterday.yesterdayGbp ){
                        
                        cell.gbpPerc.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayGbp, b: self.today.todayGbp))% "
                        cell.gbpPerc.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.gbpView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.gbpImg.image = UIImage(named: "down")
                        
                        
                    }else{
                        cell.gbpPerc.text = " +\(AppUtils.returnPercentage(a: self.today.todayGbp, b: self.yesterday.yesterdayGbp))% "
                        cell.gbpPerc.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.gbpView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.gbpImg.image = UIImage(named: "equal")
                        
                    }
                    
                    //////EUR
                    if(self.today.todayEur > self.yesterday.yesterdayEur){
                        
                        
                        cell.eurPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayEur, b: self.yesterday.yesterdayEur))% "
                        cell.eurPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.eurView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.eurImg.image = UIImage(named: "UpperArrow")
                        
                        
                    }else if(self.today.todayEur < self.yesterday.yesterdayEur){
                        
                        cell.eurPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayEur, b: self.today.todayEur))% "
                        cell.eurPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.eurView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.eurImg.image = UIImage(named: "down")
                        
                        
                    }else{
                        
                        cell.eurPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayEur, b: self.yesterday.yesterdayEur))% "
                        cell.eurPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.eurView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.eurImg.image = UIImage(named: "equal")
                    }
                    
                    
                    //////JPY
                    if(self.today.todayJpy > self.yesterday.yesterdayJpy ){
                        
                        
                        cell.jpyPerc.text = " +\(AppUtils.returnPercentage(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy))% "
                        cell.jpyPerc.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.jpyView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.jpyImg.image = UIImage(named: "UpperArrow")
                        
                        
                    }else if(self.today.todayJpy < self.yesterday.yesterdayJpy ){
                        
                        cell.jpyPerc.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayJpy, b: self.today.todayJpy))% "
                        cell.jpyPerc.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.jpyView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.jpyImg.image = UIImage(named: "down")
                        
                        
                    }else{
                        cell.jpyPerc.text = " \(AppUtils.returnPercentage(a: self.today.todayJpy, b: self.yesterday.yesterdayJpy))% "
                        cell.jpyPerc.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.jpyView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.jpyImg.image = UIImage(named: "equal")
                        
                    }
                    
                }}
            
            return cell
            
        }else if (array[indexPath.row] == "Crude"){
            self.dashboardView.register(UINib(nibName: "CrudeCell", bundle: nil), forCellReuseIdentifier: "crudeCell")
            
            
            
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "crudeCell", for: indexPath) as! CrudeCell
            cell.bgView.cornerRadius = 20
            
            
            cell.openWti.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openWti)))
            cell.openBrent.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openBrent)))
            
            
            cell.brentValue.text = "\(self.today.todayBrent!)"
            cell.wtivalue.text = "\(self.today.todayWtl!)"
            
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    
                    if self.today.todayBrent > self.yesterday.yesterdayBrent {
                        
                        
                        cell.brentPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent))% "
                        cell.brentPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.brentImg.image = UIImage(named: "UpperArrow")
                        self.setBrent(chart: cell.brentChart,color: "green")
                        
                    }else if self.today.todayBrent < self.yesterday.yesterdayBrent{
                        
                        cell.brentPercent.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayBrent, b: self.today.todayBrent))% "
                        cell.brentPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.brentImg.image = UIImage(named: "down")
                        self.setBrent(chart: cell.brentChart,color: "red")
                        
                        
                    }else{
                        cell.brentPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent))% "
                        cell.brentPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.brentImg.image = UIImage(named: "equal")
                        self.setBrent(chart: cell.brentChart,color: "yellow")
                        
                    }
                    
                    
                    
                    if(self.today.todayWtl > self.yesterday.yesterdayWtl){
                        
                        
                        cell.wtiPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl))% "
                        cell.wtiPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.wtiImg.image = UIImage(named: "UpperArrow")
                        self.setWti(chart: cell.wtiChart,color: "green")
                        
                    }else if(self.today.todayWtl < self.yesterday.yesterdayWtl){
                        
                        cell.wtiPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCrude, b: self.today.todayCrude))% "
                        cell.wtiPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.wtiImg.image = UIImage(named: "down")
                        self.setWti(chart: cell.wtiChart,color: "red")
                        
                        
                    }else{
                        cell.wtiPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl))% "
                        cell.wtiPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.wtiImg.image = UIImage(named: "equal")
                        self.setWti(chart: cell.wtiChart,color: "yellow")
                    }
                    
                    
                    
                    
                }}
            
            return cell
            
        }else if (array[indexPath.row] == "Raw"){
            self.dashboardView.register(UINib(nibName: "PtaPXCell", bundle: nil), forCellReuseIdentifier: "ptaPXCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ptaPXCell", for: indexPath) as! PtaPXCell
            
            
            
            cell.openUnlock.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openUnlock)))
            
            
            cell.openPx.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openPx)))
            cell.openPta.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openPta)))
            cell.openMeg.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openMeg)))
            
            
            cell.bgView.cornerRadius = 20
            
            
            if self.todayRaw != nil && self.todayRaw.todayPx != nil{
                
                cell.pxValue.text = "\(self.todayRaw.todayPx!)"
                cell.ptaValue.text = "\(self.todayRaw.todayPta!)"
                cell.megValue.text = "\(self.todayRaw.todayMeg!)"
                
                
                if BDefaults.instance.getUsers()[0].userType == "employee"{
                    cell.lockView.isHidden = true
                    cell.cDate.text = "  \(AppUtils.addDays(d: 0))"
                }else{
                    cell.cDate.text = "As of - \(AppUtils.addDays(d: -7))"
                    cell.lockedDate.text = "As of - \(AppUtils.addDays(d: -4))"
                }
                
                
                
                
                DispatchQueue.global(qos: .background).async {
                    DispatchQueue.main.async {
                        
                        if self.todayRaw.todayPx > self.yesterdayRaw.yesterdayPx {
                            
                            
                            cell.pxPer.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx))% "
                            cell.pxPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                            cell.pxImg.image = UIImage(named: "UpperArrow")
                            self.setPx(chart: cell.pxChart,color: "green")
                            
                        }else if self.todayRaw.todayPx < self.yesterdayRaw.yesterdayPx{
                            
                            cell.pxPer.text = "-\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayPx, b: self.todayRaw.todayPx))% "
                            cell.pxPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                            cell.pxImg.image =  UIImage(named: "down")
                            self.setPx(chart: cell.pxChart,color: "red")
                            
                            
                        }else{
                            cell.pxPer.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayPx, b: self.yesterdayRaw.yesterdayPx))% "
                            cell.pxPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                            cell.pxImg.image = UIImage(named: "equal")
                            self.setPx(chart: cell.pxChart,color: "yellow")
                        }
                        
                        
                        
                        
                        if(self.todayRaw.todayPta > self.yesterdayRaw.yesterdayPta) {
                            
                            
                            cell.ptaPer.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta))% "
                            cell.ptaPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                            cell.ptaImg.image = UIImage(named: "UpperArrow")
                            self.setPta(chart: cell.ptaChart,color: "green")
                            
                        }else if(self.todayRaw.todayPta < self.yesterdayRaw.yesterdayPta){
                            
                            cell.ptaPer.text = "-\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayPta, b: self.todayRaw.todayPta))% "
                            cell.ptaPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                            cell.ptaImg.image = UIImage(named: "down")
                            self.setPta(chart: cell.ptaChart,color: "red")
                            
                            
                        }else{
                            cell.ptaPer.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayPta, b: self.yesterdayRaw.yesterdayPta))% "
                            cell.ptaPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                            cell.ptaImg.image = UIImage(named: "equal")
                            self.setPta(chart: cell.ptaChart,color: "yellow")
                        }
                        
                        
                        if(self.todayRaw.todayMeg > self.yesterdayRaw.yesterdayMeg){
                            
                            
                            cell.megPer.text = " +\(AppUtils.returnPercentage(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg))% "
                            cell.megPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                            cell.megImg.image = UIImage(named: "UpperArrow")
                            self.setMeg(chart: cell.megChart,color: "green")
                            
                        }else   if(self.todayRaw.todayMeg < self.yesterdayRaw.yesterdayMeg){
                            
                            cell.megPer.text = "-\(AppUtils.returnPercentage(a: self.yesterdayRaw.yesterdayMeg, b: self.todayRaw.todayMeg))% "
                            cell.megPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                            cell.megImg.image = UIImage(named: "down")
                            self.setMeg(chart: cell.megChart,color: "red")
                            
                            
                        }else{
                            cell.megPer.text = " \(AppUtils.returnPercentage(a: self.todayRaw.todayMeg, b: self.yesterdayRaw.yesterdayMeg))% "
                            cell.megPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                            cell.megImg.image = UIImage(named: "equal")
                            self.setMeg(chart: cell.megChart,color: "yellow")
                        }
                        
                    }}
                
            }else{
                cell.bgView.isHidden = true
                cell.isHidden = true
                
            }
            
            return cell
            
        }else if (array[indexPath.row] == "Fiber"){
            self.dashboardView.register(UINib(nibName: "FiberCell", bundle: nil), forCellReuseIdentifier: "fiberCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "fiberCell", for: indexPath) as! FiberCell
            
            
            cell.openIci.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openIci)))
            cell.openChina.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openChina)))
            cell.openImport.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openImport)))
            
            
            
            
            cell.bgView.cornerRadius = 20
            
            cell.iciValue.text = "\(self.today.todayIciSemi!)"
            cell.chinaValue.text = "\(self.today.todayChinaPsfPrice!)"
            cell.importValue.text = "\(self.today.todayImportOffr!)"
            
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    
                    
                    if(self.today.todayIciSemi > self.yesterday.yesterdayIciSemi){
                        
                        
                        cell.iciPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi))% "
                        cell.iciPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.iciImg.image = UIImage(named: "UpperArrow")
                        self.setIciSemi(chart: cell.iciChart,color: "green")
                        
                    }else if(self.today.todayIciSemi < self.yesterday.yesterdayIciSemi){
                        
                        cell.iciPer.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayIciSemi, b: self.today.todayIciSemi))% "
                        cell.iciPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.iciImg.image = UIImage(named: "down")
                        self.setIciSemi(chart: cell.iciChart,color: "red")
                        
                        
                    }else{
                        
                        cell.iciPer.text = " \(AppUtils.returnPercentage(a: self.today.todayIciSemi, b: self.yesterday.yesterdayIciSemi))% "
                        cell.iciPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.iciImg.image = UIImage(named: "equal")
                        self.setIciSemi(chart: cell.iciChart,color: "yellow")
                    }
                    
                    
                    
                    
                    if(self.today.todayChinaPsfPrice > self.yesterday.yesterdayChinaPsfPrice){
                        
                        
                        cell.chinaPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice))% "
                        cell.chinaPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.chinaImg.image = UIImage(named: "UpperArrow")
                        self.setChina(chart: cell.chinaChart,color: "green")
                        
                    }else if(self.today.todayChinaPsfPrice < self.yesterday.yesterdayChinaPsfPrice){
                        
                        cell.chinaPer.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayChinaPsfPrice, b: self.today.todayChinaPsfPrice))% "
                        cell.chinaPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.chinaImg.image = UIImage(named: "down")
                        self.setChina(chart: cell.chinaChart,color: "red")
                        
                        
                    }else{
                        cell.chinaPer.text = " \(AppUtils.returnPercentage(a: self.today.todayChinaPsfPrice, b: self.yesterday.yesterdayChinaPsfPrice))% "
                        cell.chinaPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.chinaImg.image = UIImage(named: "equal")
                        self.setChina(chart: cell.chinaChart,color: "yellow")
                    }
                    
                    
                    if(self.today.todayImportOffr > self.yesterday.yesterdayImportOffer){
                        
                        
                        cell.importPer.text = " +\(AppUtils.returnPercentage(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer))% "
                        cell.importPer.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.importImg.image = UIImage(named: "UpperArrow")
                        self.setImport(chart: cell.importChart,color: "green")
                        
                    }else if(self.today.todayImportOffr < self.yesterday.yesterdayImportOffer){
                        
                        cell.importPer.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayImportOffer, b: self.today.todayImportOffr))% "
                        cell.importPer.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.importImg.image = UIImage(named: "down")
                        self.setImport(chart: cell.importChart,color: "red")
                        
                        
                    }else{
                        
                        cell.importPer.text = " \(AppUtils.returnPercentage(a: self.today.todayImportOffr, b: self.yesterday.yesterdayImportOffer))% "
                        cell.importPer.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.importImg.image = UIImage(named: "equal")
                        self.setImport(chart: cell.importChart,color: "yellow")
                        
                    }
                    
                }}
            return cell
            
        }else if (array[indexPath.row] == "Cotton"){
            self.dashboardView.register(UINib(nibName: "CottonCell", bundle: nil), forCellReuseIdentifier: "cottonCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cottonCell", for: indexPath) as! CottonCell
            cell.bgView.cornerRadius = 20
            
            
            cell.openCotton.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openCotton)))
            cell.openNyf.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openNyf)))
            
            
            
            cell.nyfValue.text = "\(self.today.todayNyfMaund!)"
            cell.cottonValue.text = "\(self.today.todayCottonMaund!)"
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    if(self.today.todayNyfMaund > self.yesterday.yesterdayNyfMaund) {
                        
                        
                        cell.nyfPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund))% "
                        cell.nyfPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.nyfImg.image = UIImage(named: "UpperArrow")
                        self.setNyfPkrKg(chart: cell.nyfChart,color: "green")
                        
                    }else  if(self.today.todayNyfMaund < self.yesterday.yesterdayNyfMaund){
                        
                        cell.nyfPercent.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayNyfMaund, b: self.today.todayNyfMaund))% "
                        cell.nyfPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.nyfImg.image = UIImage(named: "down")
                        self.setNyfPkrKg(chart: cell.nyfChart,color: "red")
                        
                        
                    }else{
                        cell.nyfPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayNyfMaund, b: self.yesterday.yesterdayNyfMaund))% "
                        cell.nyfPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.nyfImg.image = UIImage(named: "equal")
                        self.setNyfPkrKg(chart: cell.nyfChart,color: "yellow")
                    }
                    
                    
                    
                    if(self.today.todayCottonMaund > self.yesterday.yesterdayCottonMaund){
                        
                        
                        cell.cottonPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund))% "
                        cell.cottonPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.cottonImg.image = UIImage(named: "UpperArrow")
                        self.setCotton(chart: cell.cottonChart,color: "green")
                        
                    }else if(self.today.todayCottonMaund < self.yesterday.yesterdayCottonMaund){
                        
                        cell.cottonPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCottonMaund, b: self.today.todayCottonMaund))% "
                        cell.cottonPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.cottonImg.image = UIImage(named: "down")
                        self.setCotton(chart: cell.cottonChart,color: "red")
                        
                        
                    }else{
                        cell.cottonPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayCottonMaund, b: self.yesterday.yesterdayCottonMaund))% "
                        cell.cottonPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.cottonImg.image = UIImage(named: "equal")
                        self.setCotton(chart: cell.cottonChart,color: "yellow")
                        
                    }
                }}
            
            return cell
            
        }
            
        else if array[indexPath.row] == "Events"{
            self.dashboardView.register(UINib(nibName: "UpcomingTableViewCell", bundle: nil), forCellReuseIdentifier: "UpcomingTableViewCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpcomingTableViewCell", for: indexPath) as! UpcomingTableViewCell
            if   BDefaults.instance.getAllEvents().count > 0 {
                
                cell.eventName.text = BDefaults.instance.getAllEvents()[0].heading
                
                cell.addressLbl.text = BDefaults.instance.getAllEvents()[0].address
                cell.dateLbl.text = AppUtils.formateEvent(date:  BDefaults.instance.getAllEvents()[0].eventDate)
                cell.iimageLbl.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(BDefaults.instance.getAllEvents()[0].image!)"), placeholderImage: UIImage(named: "picture.png"))
                
            } else {
                cell.isHidden = true
            }
            
            
            
            return cell
        }
            
            
        else{
            self.dashboardView.register(UINib(nibName: "YarnCell", bundle: nil), forCellReuseIdentifier: "yarnCell")
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "yarnCell", for: indexPath) as! YarnCell
            
            
            
            cell.openyarn38.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openYarn38)))
            cell.openYarn30.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openYarn30)))
            cell.openYarn36.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openYarn36)))
            
            cell.openYarn50.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openYarn50)))
            
            cell.openYarn20.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openYarn20)))
            
            cell.openYarn26.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.openYarn26)))
            
            
            cell.bgView.cornerRadius = 20
            
            cell.yar50Value.text = "\(self.today.todayYarn50Pp!)"
            cell.yar38Value.text = "\(self.today.todayYarn38Pc!)"
            cell.yar36Value.text = "\(self.today.todayYarn36Pc!)"
            cell.yar30Value.text = "\(self.today.todayYarn30Pc!)"
            cell.yar26Value.text = "\(self.today.todayYarn26Pc!)"
            cell.yar20Value.text = "\(self.today.todayYarn20S!)"
            
            DispatchQueue.global(qos: .background).async {
                DispatchQueue.main.async {
                    if(self.today.todayYarn50Pp > self.yesterday.yesterdayYarn50Pp){
                        
                        
                        cell.yar50Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp))% "
                        cell.yar50Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.yar50Img.image = UIImage(named: "UpperArrow")
                        self.setYarn50(chart: cell.yarn50Chart,color: "green")
                        
                    }else if(self.today.todayYarn50Pp < self.yesterday.yesterdayYarn50Pp){
                        
                        cell.yar50Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn50Pp, b: self.today.todayYarn50Pp))% "
                        cell.yar50Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.yar50Img.image = UIImage(named: "down")
                        self.setYarn50(chart: cell.yarn50Chart,color: "red")
                        
                        
                    }else{
                        cell.yar50Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn50Pp, b: self.yesterday.yesterdayYarn50Pp))% "
                        cell.yar50Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.yar50Img.image = UIImage(named: "equal")
                        self.setYarn50(chart: cell.yarn50Chart,color: "yellow")
                        
                    }
                    
                    
                    
                    
                    if(self.today.todayYarn38Pc > self.yesterday.yesterdayYarn38Pc){
                        
                        
                        cell.yar38Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc))% "
                        cell.yar38Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.yar38Img.image = UIImage(named: "UpperArrow")
                        self.setYarn38(chart: cell.yarn38Chart,color: "green")
                        
                    }else  if(self.today.todayYarn38Pc > self.yesterday.yesterdayYarn38Pc){
                        
                        cell.yar38Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn38Pc, b: self.today.todayYarn38Pc))% "
                        cell.yar38Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.yar38Img.image = UIImage(named: "down")
                        self.setYarn38(chart: cell.yarn38Chart,color: "red")
                        
                        
                    }else{
                        cell.yar38Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn38Pc, b: self.yesterday.yesterdayYarn38Pc))% "
                        cell.yar38Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.yar38Img.image = UIImage(named: "equal")
                        self.setYarn38(chart: cell.yarn38Chart,color: "yellow")
                        
                    }
                    
                    
                    if(self.today.todayYarn36Pc > self.yesterday.yesterdayYarn36Pc){
                        
                        
                        cell.yar36Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc))% "
                        cell.yar36Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.yar36Img.image = UIImage(named: "UpperArrow")
                        self.setyarn36(chart: cell.yarn36Chart,color: "green")
                        
                    }else if(self.today.todayYarn36Pc < self.yesterday.yesterdayYarn36Pc){
                        
                        cell.yar36Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn36Pc, b: self.today.todayYarn36Pc))% "
                        cell.yar36Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.yar36Img.image = UIImage(named: "down")
                        self.setyarn36(chart: cell.yarn36Chart,color: "red")
                        
                        
                    }else{
                        
                        cell.yar36Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn36Pc, b: self.yesterday.yesterdayYarn36Pc))% "
                        cell.yar36Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.yar36Img.image = UIImage(named: "equal")
                        self.setyarn36(chart: cell.yarn36Chart,color: "yellow")
                        
                        
                    }
                    
                    
                    
                    
                    
                    if(self.today.todayYarn30Pc > self.yesterday.yesterdayYarn30Pc){
                        
                        
                        cell.yar30Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc))% "
                        cell.yar30Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.yar30Img.image = UIImage(named: "UpperArrow")
                        self.setYarn30(chart: cell.yarn30Chart,color: "green")
                        
                    }else if(self.today.todayYarn30Pc < self.yesterday.yesterdayYarn30Pc){
                        
                        cell.yar30Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn30Pc, b: self.today.todayYarn30Pc))% "
                        cell.yar30Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.yar30Img.image = UIImage(named: "down")
                        self.setYarn30(chart: cell.yarn30Chart,color: "red")
                        
                        
                    }else{
                        
                        
                        cell.yar30Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn30Pc, b: self.yesterday.yesterdayYarn30Pc))% "
                        cell.yar30Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.yar30Img.image = UIImage(named: "equal")
                        self.setYarn30(chart: cell.yarn30Chart,color: "yellow")
                        
                        
                    }
                    
                    
                    
                    if(self.today.todayYarn26Pc > self.yesterday.yesterdayYarn26Pc){
                        
                        
                        cell.yar26Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc))% "
                        cell.yar26Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.yar26Img.image = UIImage(named: "UpperArrow")
                        self.setYarn26(chart: cell.yarn26Chart,color: "green")
                        
                    }else if(self.today.todayYarn26Pc < self.yesterday.yesterdayYarn26Pc){
                        
                        cell.yar26Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn26Pc, b: self.today.todayYarn26Pc))% "
                        cell.yar26Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.yar26Img.image = UIImage(named: "down")
                        self.setYarn26(chart: cell.yarn26Chart,color: "red")
                        
                        
                    }else{
                        
                        cell.yar26Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn26Pc, b: self.yesterday.yesterdayYarn26Pc))% "
                        cell.yar26Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.yar26Img.image = UIImage(named: "equal")
                        self.setYarn26(chart: cell.yarn26Chart,color: "yellow")
                        
                        
                        
                    }
                    
                    
                    
                    
                    
                    if(self.today.todayYarn20S > self.yesterday.yesterdayYarn20S){
                        
                        
                        cell.yar20Per.text = " +\(AppUtils.returnPercentage(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S))% "
                        cell.yar20Per.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
                        cell.yar20Img.image = UIImage(named: "UpperArrow")
                        self.setYarn20(chart: cell.yarn20Chart,color: "green")
                        
                    }else  if(self.today.todayYarn20S < self.yesterday.yesterdayYarn20S){
                        
                        cell.yar20Per.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayYarn20S, b: self.today.todayYarn20S))% "
                        cell.yar20Per.backgroundColor = UIColor(cgColor: AppUtils.redColor)
                        cell.yar20Img.image = UIImage(named: "down")
                        self.setYarn20(chart: cell.yarn20Chart,color: "red")
                        
                        
                    }else{
                        
                        cell.yar20Per.text = " \(AppUtils.returnPercentage(a: self.today.todayYarn20S, b: self.yesterday.yesterdayYarn20S))% "
                        cell.yar20Per.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
                        cell.yar20Img.image = UIImage(named: "equal")
                        self.setYarn20(chart: cell.yarn20Chart,color: "yellow")
                        
                        
                        
                    }
                    
                    
                }}
            
            //
            //            setYarn20(chart: cell.yarn20Chart)
            //            setYarn30(chart: cell.yarn30Chart)
            //            setyarn36(chart: cell.yarn36Chart)
            //            setYarn38(chart: cell.yarn38Chart)
            //            setYarn50(chart: cell.yarn50Chart)
            //            setYarn26(chart: cell.yarn26Chart)
            
            
            return cell
            
        }
        
        
    }
    
    
    
    
    
    // Mark :-
    @objc func openUnlock(sender: UITapGestureRecognizer){
        //        BDefaults.instance.setString(key: "currency", value: "usd")
        self.performSegue(withIdentifier: "openPurchasePta", sender: self)
    }
    
    
    
    
    
    @objc func openUsd(sender: UITapGestureRecognizer){
        BDefaults.instance.setString(key: "currency", value: "usd")
        self.performSegue(withIdentifier: "openCurrency", sender: self)
    }
    
    
    
    @objc func openCny(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "currency", value: "cny")
        self.performSegue(withIdentifier: "openCurrency", sender: self)
    }
    @objc func openGbp(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "currency", value: "gbp")
        self.performSegue(withIdentifier: "openCurrency", sender: self)
    }
    @objc func openEur(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "currency", value: "eur")
        self.performSegue(withIdentifier: "openCurrency", sender: self)
    }
    @objc func openJpy(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "currency", value: "jpy")
        self.performSegue(withIdentifier: "openCurrency", sender: self)
    }
    
    //      wti
    @objc func openWti(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "crude", value: "wti")
        self.performSegue(withIdentifier: "openCrude", sender: self)
    }
    @objc func openBrent(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "crude", value: "brent")
        self.performSegue(withIdentifier: "openCrude", sender: self)
    }
    
    //openRaw
    @objc func openPx(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "raw", value: "px")
        self.performSegue(withIdentifier: "openRaw", sender: self)
    }
    @objc func openPta(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "raw", value: "pta")
        self.performSegue(withIdentifier: "openRaw", sender: self)
    }
    
    @objc func openMeg(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "raw", value: "meg")
        self.performSegue(withIdentifier: "openRaw", sender: self)
    }
    
    //openFiber
    @objc func openIci(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "fiber", value: "ici")
        self.performSegue(withIdentifier: "openFiber", sender: self)
    }
    @objc func openChina(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "fiber", value: "china")
        self.performSegue(withIdentifier: "openFiber", sender: self)
    }
    
    @objc func openImport(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "fiber", value: "import")
        self.performSegue(withIdentifier: "openFiber", sender: self)
    }
    //openCotton
    
    @objc func openNyf(sender: UITapGestureRecognizer) {
        
        BDefaults.instance.setString(key: "cotton", value: "nyf")
        self.performSegue(withIdentifier: "openCotton", sender: self)
        
    }
    @objc func openCotton(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "cotton", value: "cotton")
        self.performSegue(withIdentifier: "openCotton", sender: self)
        
    }
    
    
    //openYarn
    @objc func openYarn38(sender: UITapGestureRecognizer) {
        
        BDefaults.instance.setString(key: "yarn", value: "38")
        self.performSegue(withIdentifier: "openYarn", sender: self)
    }
    @objc func openYarn30(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "yarn", value: "30")
        self.performSegue(withIdentifier: "openYarn", sender: self)
        
    }
    @objc func openYarn50(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "yarn", value: "50")
        self.performSegue(withIdentifier: "openYarn", sender: self)
        
    }
    @objc func openYarn20(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "yarn", value: "20")
        self.performSegue(withIdentifier: "openYarn", sender: self)
        
    }
    @objc func openYarn36(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "yarn", value: "36")
        self.performSegue(withIdentifier: "openYarn", sender: self)
        
    }
    
    @objc func openYarn26(sender: UITapGestureRecognizer) {
        BDefaults.instance.setString(key: "yarn", value: "26")
        self.performSegue(withIdentifier: "openYarn", sender: self)
        
    }
    
    
    
    
    func setUpChart(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = false
        chart.setScaleEnabled(false)
        chart.pinchZoomEnabled = false
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = false
        
        
        
        let xAxis = chart.xAxis
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        chart.rightAxis.enabled = false
        chart.xAxis.enabled = false
        chart.leftAxis.enabled = false
        chart.legend.enabled = false
        
        
    }
    func setData(_ values: [ChartDataEntry],color : String ,chart : LineChartView) {
        
        
        
        
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        set1.highlightEnabled = false
        
        if(color == "red"){
            set1.setColor(UIColor(cgColor: AppUtils.redColor))
        }
        else if color == "green"{
            set1.setColor(UIColor(cgColor: AppUtils.greenColor))
        }else{
            set1.setColor(UIColor(cgColor: AppUtils.yellowColor))
        }
        
        set1.lineWidth = 1
        //        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        
        let data = LineChartData(dataSet: set1)
        
        
        
        
        
        chart.data = data
        
        
        //        chart.xAxis.valueFormatter = ChartFormatter()
        
    }
    
    
    
}
extension DashBoardTable{
    
    func setWti(chart : LineChartView,color : String){
        
        setUpChart(chart: chart)
        
        let values = (0..<month.count).map { (i) -> ChartDataEntry in
            let val = Double(month[i].crude)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setData(values,color: color, chart: chart)
    }
    func setBrent(chart : LineChartView,color:  String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].brent)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setPx(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].px)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setPta(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].pta)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setMeg(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].meg)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setIciSemi(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].iciSemi)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    func setChina(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].chinaPsfPrice)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setImport(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].importOffer)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setCotton(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].cottonMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setNyfLbs(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].nyf)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setNyfPkrMaund(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].nyfMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setNyfPkrKg(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].nyfPkr)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setYarn50(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn50Pp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    func setYarn38(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn38Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    func setyarn36(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn36Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    
    func setYarn30(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn30Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setYarn26(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn26Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
    }
    
    func setYarn20(chart : LineChartView,color : String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].yarn20S)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values,color: color, chart: chart)
        
    }
    
    
    
    
    
    
}
