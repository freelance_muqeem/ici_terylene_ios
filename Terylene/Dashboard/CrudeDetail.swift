//
//  CrudeDetail.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 16/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import SWSegmentedControl
import Charts
class CrudeDetail: UIViewController ,SWSegmentedControlDelegate{
    
    @IBOutlet weak var cYesterday: UILabel!
    
  
    @IBOutlet weak var brentImg: UIImageView!
    @IBOutlet weak var brentPercent: UILabel!
    @IBOutlet weak var brentValue: UILabel!
    @IBOutlet weak var wtiImg: UIImageView!
    @IBOutlet weak var wtiPercent: UILabel!
    @IBOutlet weak var wtivalue: UILabel!
    @IBOutlet weak var brentChart: LineChartView!
    @IBOutlet weak var wtiChart: LineChartView!
    var selected = 0

    
    
    
    
    
    @IBOutlet weak var chart: LineChartView!
    @IBOutlet weak var tabView: UIView!
    @IBOutlet weak var cArrow: UIImageView!
    @IBOutlet weak var cChanges: UILabel!
    @IBOutlet weak var cView: UIView!
    
    @IBOutlet weak var cValue: UILabel!
    @IBOutlet weak var cHeader: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var chartDate: UILabel!
    
    @IBOutlet weak var cPercent: UILabel!
    @IBOutlet weak var bottomView: UIView!
    var sc : SWSegmentedControl!
    
    var month = BDefaults.instance.getGraphMonth()
    var week = BDefaults.instance.getGraphWeekly()
    let today = BDefaults.instance.getDashboardDaily()[0].today!
    let yesterday =  BDefaults.instance.getDashboardDaily()[0].yesterday!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selected = 0

        chartDate.text = AppUtils.getGraphDate()
        date.text = AppUtils.getCurrentTimeShort()
        sc = SWSegmentedControl(items: ["1W", "1M"])
        sc.delegate = self
        sc.font = UIFont(name: "SFProDisplay-Semibold", size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        sc.indicatorColor = UIColor.init(cgColor: AppUtils.lightColor)
        sc.badgeColor = UIColor.init(cgColor: AppUtils.lightColor)
        
        sc.indicatorThickness = CGFloat(3)
        
        sc.frame = CGRect(x: 0, y: -3, width: tabView.frame.width, height: 44)
        tabView.addSubview(sc)
        
        showBottomDetail()
        showDetail()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func showDetail(){
        selected = 0

        if BDefaults.instance.getString(key: "crude") == "wti"{
            sc.setSelectedSegmentIndex(0)
            cHeader.text = "-WTI"
            setWti()
            setWti(m: week)
        }else if BDefaults.instance.getString(key: "crude") == "brent"{
            setBrent()
            cHeader.text = "-BRENT"
            setBrent(m: week)
            sc.setSelectedSegmentIndex(0)
        }
        
        
    }
    
    
    
    func setWti(){
        
        cValue.text = "\(self.today.todayWtl!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayWtl!)"

        
        
        ///Wti
        if(self.today.todayWtl > self.yesterday.yesterdayWtl){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            
            
        }else  if(self.today.todayWtl < self.yesterday.yesterdayWtl){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayWtl, b: self.today.todayWtl))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
        }
    }
    
    func setBrent(){
        
        cValue.text = "\(self.today.todayBrent!)"
        cChanges.text = " \(AppUtils.sub(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent)) "
        
        cYesterday.text = "\(self.yesterday.yesterdayBrent!)"

        
        if(self.today.todayBrent > self.yesterday.yesterdayBrent){
            
            
            
            cPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            cArrow.image = UIImage(named: "UpperArrow")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
            
        }else if(self.today.todayBrent < self.yesterday.yesterdayBrent){
            
            cPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayBrent, b: self.today.todayBrent))% "
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            cArrow.image = UIImage(named: "down")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.redColor)
            
        }else{
            cPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent))% "
            
            cPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cView.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
            cArrow.image = UIImage(named: "equal")
            cChanges.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
        }
        
    }
   
    
    func showBottomDetail(){
        
        bottomView.cornerRadius = 20
        
      
        brentValue.text = "\(self.today.todayBrent!)"
        wtivalue.text = "\(self.today.todayWtl!)"
        
        
        if self.today.todayBrent > self.yesterday.yesterdayBrent {
            
            
             brentPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent))% "
             brentPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             brentImg.image = UIImage(named: "UpperArrow")
            self.setBrent(chart:  brentChart,color: "green")
            
        }else if self.today.todayBrent < self.yesterday.yesterdayBrent{
            
             brentPercent.text = "-\(AppUtils.returnPercentage(a: self.yesterday.yesterdayBrent, b: self.today.todayBrent))% "
             brentPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             brentImg.image = UIImage(named: "down")
            self.setBrent(chart:  brentChart,color: "red")
            
            
        }else{
             brentPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayBrent, b: self.yesterday.yesterdayBrent))% "
             brentPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             brentImg.image = UIImage(named: "equal")
            self.setBrent(chart:  brentChart,color: "yellow")
            
        }
        
        
        
        if(self.today.todayWtl > self.yesterday.yesterdayWtl){
            
            
             wtiPercent.text = " +\(AppUtils.returnPercentage(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl))% "
             wtiPercent.backgroundColor = UIColor(cgColor: AppUtils.greenColor)
             wtiImg.image = UIImage(named: "UpperArrow")
            self.setWti(chart:  wtiChart,color: "green")
            
        }else if(self.today.todayWtl < self.yesterday.yesterdayWtl){
            
             wtiPercent.text = " -\(AppUtils.returnPercentage(a: self.yesterday.yesterdayCrude, b: self.today.todayCrude))% "
             wtiPercent.backgroundColor = UIColor(cgColor: AppUtils.redColor)
             wtiImg.image = UIImage(named: "down")
            self.setWti(chart:  wtiChart,color: "red")
            
            
        }else{
             wtiPercent.text = " \(AppUtils.returnPercentage(a: self.today.todayWtl, b: self.yesterday.yesterdayWtl))% "
             wtiPercent.backgroundColor = UIColor(cgColor: AppUtils.yellowColor)
             wtiImg.image = UIImage(named: "equal")
            self.setWti(chart:  wtiChart,color: "yellow")
        }
        
                
                
                
                
        
        
        
    }
    
    
    
    
    
    
    
    
    
    @IBAction func onBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func segmentedControl(_ control: SWSegmentedControl, didSelectItemAtIndex index: Int) {
        selected = index

        if BDefaults.instance.getString(key: "crude") == "brent"{
            
            if(index == 0){
                setBrent(m: week)
                
            }else{
                setBrent(m: month)
            }
            
        }else if BDefaults.instance.getString(key: "crude") == "wti"{
            if(index == 0){
                setWti(m: week)
                
            }else{
                setWti(m: month)
            }
        }
        print("did select \(index)")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func setBrent(m : [GraphDtoWeek] ){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].brent)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    func setWti(m : [GraphDtoWeek]){
        let values = (0..<m.count).map { (i) -> ChartDataEntry in
            let val = Double(m[i].wtl)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: chart)
        setData(values, chart: chart)
        
    }
    
    
    
    
    @IBAction func openWti(_ sender: Any) {
        
        BDefaults.instance.setString(key: "crude", value: "wti")
        showDetail()
    }
    @IBAction func openBrent(_ sender: Any) {
        BDefaults.instance.setString(key: "crude", value: "brent")
        showDetail()
    }
    
    
    
   
    func setUpChart(chart : LineChartView){
        
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = false
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = true
        leftAxis.drawGridLinesEnabled = true
        leftAxis.xOffset = 10
        leftAxis.labelCount = 3
        leftAxis.gridLineWidth = CGFloat(1)
        leftAxis.gridColor = .init(cgColor: AppUtils.grey)
        
        
        leftAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 9) ?? UIFont.boldSystemFont(ofSize: 8)
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        xAxis.labelFont = UIFont(name: "SFProDisplay-Semibold", size: 7) ?? UIFont.boldSystemFont(ofSize: 6)
        
        xAxis.labelCount = 4

        
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = true
        rightAxis.drawGridLinesEnabled = false
        rightAxis.yOffset = 10
        
        
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
     
        
        
        
    }
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        let gradientColors = [ AppUtils.graphColor,
                               AppUtils.graphColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        if selected == 0 {
            
            
            let formater = ChartFormatter( v: self.week )
            
            self.chart.xAxis.valueFormatter = formater
            
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        else{
            
            let formater = ChartFormatter( v: AppUtils.month )
            self.chart.xAxis.valueFormatter = formater
            
            let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                      font: .systemFont(ofSize: 12),
                                      textColor: .black,
                                      insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                      xAxisValueFormatter: formater)
            marker.chartView = chart
            marker.minimumSize = CGSize(width: 80, height: 20)
            chart.marker = marker
            
        }
        
        
        chart.data = data
        
        
        
    }
    
    
    
    func setWti(chart : LineChartView,color : String){
        
        AppUtils.setUpChartLittle(chart: chart)
        
        let values = (0..<month.count).map { (i) -> ChartDataEntry in
            let val = Double(month[i].crude)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    func setBrent(chart : LineChartView,color:  String){
        let values = (0..<self.month.count).map { (i) -> ChartDataEntry in
            let val = Double(self.month[i].brent)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        AppUtils.setUpChartLittle(chart: chart)
        AppUtils.setDataLittle(values,color: color, chart: chart)
    }
    
    
    
    
}
