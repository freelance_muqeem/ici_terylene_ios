//
//  ProfileViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import TextFieldEffects


class ProfileViewController: UIViewController {

    //openDone
    
    
    @IBOutlet weak var confirmPassword: HoshiTextField!
    @IBOutlet weak var oldPassword: HoshiTextField!
    @IBOutlet weak var newPassword: HoshiTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func submit(_ sender: Any) {
        
        
        if check(){
            
            AppUtils.app.showActivityIndicatory(uiView: self.view)
            
            NetworkManager.changePassword(query: newPassword.text!, completion: { (confirm) in
                
                BDefaults.instance.setString(key: "message", value: "Your password has \nbeen changed.")
                self.performSegue(withIdentifier: "openDone", sender: self)
                
                
            }) { (error) in
                 self.present(AppUtils.returnErrorToast(string: "New password and confirm password are not same"), animated: true)
            }
            
        }
        
        
        
       
        
        
    }
    
    func check() -> Bool {
        
        if oldPassword.text == "" || newPassword.text == ""||confirmPassword.text == "" {
            
           self.present(AppUtils.returnErrorToast(string: "Fill All Fields"), animated: true)
            return false
        }else if  newPassword.text != confirmPassword.text   {
            
            self.present(AppUtils.returnErrorToast(string: "New password and confirm password are not same"), animated: true)
            return false
        }
        
        
        
        return true
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
