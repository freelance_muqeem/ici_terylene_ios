//
//  SubmittedController.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 23/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class SubmittedController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var topTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

//        let rectShape = CAShapeLayer()
//        rectShape.bounds = self.backButton.frame
//        rectShape.position = self.backButton.center
//        rectShape.path = UIBezierPath(roundedRect: self.backButton.bounds, byRoundingCorners: [.bottomLeft , .bottomRight ], cornerRadii: CGSize(width: 10, height: 10)).cgPath
//
        
        topTitle.text = BDefaults.instance.getString(key: "message")
    
//        self.backButton.layer.mask = rectShape
        self.backButton.layer.cornerRadius = 10
        self.backButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backClick(_ sender: Any) {
        
        AppUtils.app.startMainScreen()
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
