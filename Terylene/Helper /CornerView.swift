//
//  CornerView.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/25/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation

class CornerView: UIView {
    
}

extension UIView { 
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
}
