//
//  UIViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/28/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation

extension UIViewController {

    /// - Author: Hashim Baloch
    /// - important: This function shows the alert message.
    /// - parameters: userMessage: String this is teh message that shown on alert
    func alertMessage(userMessage : String) {
        let myAlert = UIAlertController(title: "Terylene", message:userMessage, preferredStyle: .alert);
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil);
        myAlert.addAction(okAction);
        self.present(myAlert, animated : true , completion : nil);
    }
}
