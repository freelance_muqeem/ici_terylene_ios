//
//  ApprovalViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/21/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class ApprovalViewController: UIViewController {
    
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var emailLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginBtn.layer.cornerRadius = 10
        loginBtn.layer.borderWidth = 1
        loginBtn.layer.borderColor = UIColor.black.cgColor
        
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    @IBAction func loginBtn(_ sender: Any) {
        
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! UIViewController
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}


/// fetchDashboardData ()

//func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    return  priceArray.count
//}
//
//func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//
//    if  tableView == usdTableView {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "UsdTableViewCell")  as!
//        UsdTableViewCell
//        cell.usdCellLbl.text = priceArray[indexPath.row]
//
//        let today  = self.usdTodayLbl
//        let yesterday = self.usdYesterdayLbl
//        if  today?.text == yesterday?.text {
//
//            self.usdimage.image = UIImage(named: "equal")
//
//        }  else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//            self.usdimage.image = UIImage(named: "down")
//
//        } else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//            self.usdimage.image = UIImage(named: "UpperArrow")
//        } else  {
//
//            print("No Item Data Appear")
//
//        }
//
//        return cell
//
//    }
//
//    else  if tableView == cnyTableView {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cnyTableViewCell")  as!
//        cnyTableViewCell
//        cell.cnylbl.text = priceArray[indexPath.row]
//
//        let today  = self.cnyTodayLbl
//        let yesterday = self.cnyYesterdayLbl
//        if  today?.text == yesterday?.text {
//
//            self.cnyimage.image = UIImage(named: "equal")
//
//        }  else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//            self.cnyimage.image = UIImage(named: "down")
//
//        } else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//            self.cnyimage.image = UIImage(named: "UpperArrow")
//        } else  {
//
//            print("No Item")
//        }
//
//        return cell
//
//
//
//    }
//
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//
//        if tableView == usdTableView {
//            self.usdTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_usd
//            self.usdYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_usd
//            if indexPath.row  == 0 {
//
//                self.usdTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_eur
//                self.usdYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_eur
//
//                let today  = self.usdTodayLbl
//                let yesterday = self.usdYesterdayLbl
//                if  today?.text == yesterday?.text {
//
//                    self.usdimage.image = UIImage(named: "equal")
//
//                }  else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//                    self.usdimage.image = UIImage(named: "down")
//
//                } else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//                    self.usdimage.image = UIImage(named: "UpperArrow")
//                } else  {
//
//
//                    print("No Item Data Appear")
//                }
//
//
//            } else if indexPath.row  == 1 {
//
//                self.usdTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_jpy
//                self.usdYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_jpy
//
//
//            } else  if indexPath.row == 2 {
//
//                self.usdTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_gbp
//                self.usdYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_gbp
//
//            }  else {
//
//                self.usdTableView.deselectRow(at: indexPath, animated: true)
//
//            }
//
//
//
//        } else if tableView == cnyTableView {
//            self.cnyTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_usd
//            self.cnyYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_usd
//            if indexPath.row  == 0 {
//                self.cnyTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_eur
//                self.cnyYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_eur
//
//                let today  = self.usdTodayLbl
//                let yesterday = self.usdYesterdayLbl
//                if  today?.text == yesterday?.text {
//
//                    self.cnyimage.image = UIImage(named: "equal")
//
//                }  else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//                    self.cnyimage.image = UIImage(named: "down")
//
//                } else if UIContentSizeCategory(rawValue:  (today?.text)!) > UIContentSizeCategory(rawValue:  (yesterday?.text)!) {
//
//                    self.cnyimage.image = UIImage(named: "UpperArrow")
//                } else  {
//
//                    print("No Item Data Appear")
//                }
//
//            } else if indexPath.row  == 1 {
//
//                self.cnyTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_jpy
//                self.cnyYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_jpy
//            } else  if indexPath.row == 2 {
//
//                self.cnyTodayLbl.text = self.dashboardmodel.today[indexPath.row].today_gbp
//                self.cnyYesterdayLbl.text = self.dashboardmodel.yesterday[indexPath.row].yesterday_gbp
//
//            }  else {
//
//                print("nil")
//
//            }
//
//            self.cnyTableView.deselectRow(at: indexPath, animated: true)
//
//        }
//
//    }
//
//
//
//
//
//
//
//
//
//}
