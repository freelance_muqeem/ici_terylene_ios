//
//  DashboardViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/22/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import CarbonKit

class HomeViewController: UIViewController {
    
    @IBOutlet var menuBtn: UIBarButtonItem!
    
    var screen : DashboardViewController!
    var screen1 : WeeklyViewController!
    var screen2 : MonthlyViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        screen = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as!  DashboardViewController
        screen1 = self.storyboard?.instantiateViewController(withIdentifier: "WeeklyViewController") as! WeeklyViewController
         screen2 = self.storyboard?.instantiateViewController(withIdentifier: "MonthlyViewController") as! MonthlyViewController
        
        
        let items = ["   Daily","Weekly   ","Monthly"]
       let carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items, delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        carbonTabSwipeNavigation.setTabExtraWidth(65)
        carbonTabSwipeNavigation.setNormalColor(UIColor.gray)
        carbonTabSwipeNavigation.setSelectedColor(UIColor(cgColor: AppUtils.color))
        carbonTabSwipeNavigation.setTabBarHeight(40)
        
        carbonTabSwipeNavigation.setIndicatorHeight(1)
        carbonTabSwipeNavigation.setIndicatorColor(UIColor(cgColor: AppUtils.color))
        menuBtn.target = self.revealViewController()
        menuBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        self.revealViewController().rearViewRevealWidth = 320
        self.revealViewController().rearViewRevealDisplacement = 0
        self.revealViewController()?.frontViewController.viewWillDisappear(false)
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController : CarbonTabSwipeNavigationDelegate {
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController {
        if index == 0 {
           
            
            return screen
            
        } else if index == 1 {
           
            
            return screen1
        }
        else {
            
            
            
            return screen2
            
        }
        
    }
    
    
}

