//
//  AppDelegate.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/20/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import CoreData
import GoogleMaps

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let container : UIView = UIView()
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        //AIzaSyCWEWfJIkr4TYG9QgvFbgvokF4oAgphTS4
       
        
        NetworkManager.initi()
        GMSServices.provideAPIKey("AIzaSyCWEWfJIkr4TYG9QgvFbgvokF4oAgphTS4")
        
        if(BDefaults.instance.getAppState() == BDefaults.LoggedIN)
        {
            
            refresh()
            startMainScreen()
            
            AppUtils.week =  BDefaults.instance.getGraphWeekly()
            AppUtils.month =  BDefaults.instance.getGraphMonth()
        }
        
        
        
        //        refresh()
        return true
    }
    
    
    func refresh(){
        
        NetworkManager.getHomePageVideo(completion: { (dto) in
            
        }) { (S) in
            
        }
        
        NetworkManager.getEvents(completion: { (events) in
            
            BDefaults.instance.saveEvents(array: events.hse)
            
        }) { (a) in
            
        }
        
        NetworkManager.fetchNewsData()
        
        NetworkManager.getDashboardDaily(completion: { (dto) in
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "dailyDashboard"), object: nil)
            
        }) { (s) in
            
        }
        
        NetworkManager.getDashboardMonthly(completion: { (dto) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "weekly"), object: nil)
        }) { (err) in
            
        }
        
        NetworkManager.getDashboardWeekly(completion: { (dto) in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "monthly"), object: nil)
        }) { (err) in
            
        }
        
        NetworkManager.getProducts(completion: { (pr) in
            
        }) { (error) in
            
        }
        
    }
    
    
    
    
    
    func startMainScreen()
    {
        let rootController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "CustomTabBarController") /*as! CustomTabBarController*/
        //        rootController.selectedIndex = 0
        self.window?.rootViewController = rootController
        
        
    }
    
    func startLogin()
    {
        let rootController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "LoginViewController") as! UIViewController
        self.window?.rootViewController = rootController
        
        
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    //MARK: - Starting loader Running
    
    func showActivityIndicatory(uiView: UIView) {
        
        container.frame = uiView.frame
        container.center = uiView.center
        container.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x:0,y:0,width:80,height:80)
        //CGRect(x:0, y:0, w:80, h:80)
        loadingView.center = uiView.center
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        actInd.frame = CGRect(x:0,y:0,width:40,height:40)
        actInd.style =
            UIActivityIndicatorView.Style.whiteLarge
        actInd.center = CGPoint(x:loadingView.frame.width/2,y:loadingView.frame.height/2)
        loadingView.addSubview(actInd)
        container.addSubview(loadingView)
        uiView.addSubview(container)
        actInd.startAnimating()
    }
    func hideActivityIndicatory() {
        container.removeFromSuperview()
    }
    //MARK: - Ending loader Running
    
    
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Terylene")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}
//extension UITabBar {
//
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        var sizeThatFits = super.sizeThatFits(size)
//        sizeThatFits.height = 60 // adjust your size here
//        return sizeThatFits
//    }
//}
