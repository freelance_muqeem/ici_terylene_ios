//
//  CustomTabBar.swift
//  uitabbartutorial
//
//  Created by Alexey Adamovsky on 05/10/2017.
//  Copyright © 2017 Alexey Adamovsky. All rights reserved.
//

import UIKit

class CustomTabBar: UIView {
    
    private var actionHomeTapped:()->() = {}
    private var actionCartTapped:()->() = {}
    private var actionGraphTapped:()->() = {}
    private var actionSettingsTapped:()->() = {}
    
    @IBOutlet weak var btnHome: UIButton!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnGraph: UIButton!
    @IBOutlet weak var btnSettings: UIButton!

    
  
    
   
    
    static func getFromNib()->CustomTabBar{
        let tabBarView = UINib.init(nibName: "CustomTabBar", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomTabBar

        return tabBarView
    }
    
    override func layoutSubviews() {
        self.btnHome.roundCorners(corners: [.topLeft], size: 35)
        self.btnSettings.roundCorners(corners: [.topRight], size: 35)
    }
    
    //MARK: - BUTTON ACTIONS -
    @IBAction func btnHomeTapped(_ sender: UIButton) {
        //change images to unselected to all buttons
        setAllButtonsUnselected()
        //set image selected
        btnHome.setImage(UIImage.init(named: "home icons"), for: .normal)
        actionHomeTapped()
    }
    
    @IBAction func btnCartTapped(_ sender: UIButton) {
        setAllButtonsUnselected()
        btnCart.setImage(UIImage.init(named: "consumption_selected"), for: .normal)
        actionCartTapped()
    }
    
    @IBAction func btnGraphTapped(_ sender: UIButton) {
        setAllButtonsUnselected()
        btnGraph.setImage(UIImage.init(named: "products_selected"), for: .normal)
        actionGraphTapped()
    }
    
    @IBAction func btnSettingsTapped(_ sender: UIButton) {
        setAllButtonsUnselected()
        btnSettings.setImage(UIImage.init(named: "menu_selected"), for: .normal)
        actionSettingsTapped()
    }
    
    
    //MARK: - VIEW SETTERS -
    func setActionHomeTapped(action:@escaping ()->()){
        actionHomeTapped = action
    }
    func setActionCartTapped(action:@escaping ()->()){
        actionCartTapped = action
    }
    func setActionGraphTapped(action:@escaping ()->()){
        actionGraphTapped = action
    }
    func setActionSettingsTapped(action:@escaping ()->()){
        actionSettingsTapped = action
    }
 
    
    //MARK: - VIEW FUNCTIONS -
    func setAllButtonsUnselected(){
        btnHome.setImage(UIImage.init(named: "home"), for: .normal)
        btnCart.setImage(UIImage.init(named: "consumption"), for: .normal)
        btnGraph.setImage(UIImage.init(named: "Products"), for: .normal)
        btnSettings.setImage(UIImage.init(named: "menu-1"), for: .normal)
    }
    
    
    
    
    
}
