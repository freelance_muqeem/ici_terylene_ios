//
//  ContactTableViewCell.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/18/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
 
    @IBOutlet var contactLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
