//
//  ContactViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import GoogleMaps
import DropDown
class ContactViewController: UIViewController {
    
    //openSubmit
    
    
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var sendQuesyButton: UIButton!
    let array = ["Product Related","Service Related"]
    @IBOutlet var selectQuery: UIButton!
    
    
    var i = 0
    var message = ""
    let dropDown = DropDown()
    
    
    
    let detail = "63 Mozang Road, Lahore, Pakistan  \n\n042 111 100 200\n market.research@ici.com.pk\n polyester.sales@ici.com.pk"
    
    @IBOutlet var mapView: GMSMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        selectQuery.layer.borderWidth = 1
        selectQuery.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        selectQuery.layer.cornerRadius = 10
        
        sendQuesyButton.layer.cornerRadius = 10
        
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: 31.555220, longitude: 74.322188)
        marker.title = "ICI"
        marker.snippet = "Pakistan"
        marker.map = mapView
        
        
    
        
        text.text = detail
        
        
        
        dropDown.anchorView = selectQuery
        dropDown.dataSource = self.array
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.selectQuery.setTitle("  \(self.array[index])", for: .normal)
            self.message = self.array[index]
            
            self.i = index + 1
            
            
        }
        
        
        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
            let camera = GMSCameraPosition.camera(withLatitude: 31.555220, longitude: 74.322188, zoom: 14.0)
            mapView.camera = camera
    }
    /// Func to Hide tableView
    
    @IBAction func submitQuery(_ sender: Any) {
        if message == ""{
            
            
            self.present(AppUtils.returnErrorToast(string: "Select a query"), animated: true)
        }else{
            AppUtils.app.showActivityIndicatory(uiView: self.view)
            NetworkManager.sendQuery(index : i, completion: { (result) in
                AppUtils.app.hideActivityIndicatory()
                
                BDefaults.instance.setString(key: "message", value: "Your query has \nbeen forwarded.")
                self.performSegue(withIdentifier: "openSubmit", sender: self)
                
                
                
            }) { (error) in
                AppUtils.app.hideActivityIndicatory()
                self.present(AppUtils.returnErrorToast(string: "Network Error"), animated: true)
            }
            
            
        }
        
        
        
    }
    
    
    @IBAction func querySelection(_ sender: Any) {
        
        
        dropDown.show()
        
        
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    @IBAction func callIci(_ sender: Any) {
        if let url = URL(string: "tel://+9242111000200") {
            UIApplication.shared.openURL(url)
        }
    }
}
