//
//  HSETableViewCell.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class HSETableViewCell: UITableViewCell {
    
    
    @IBOutlet var learnmoreLbl: UILabel!
    @IBOutlet var descriptionLbl: UILabel!
    
    @IBOutlet var imageLbl: UIImageView!
    
    @IBOutlet var titleLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
