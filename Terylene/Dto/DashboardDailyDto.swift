//
//  DashboardDailyDto.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 19/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//
import Foundation
import SwiftyJSON



class DashboardDailyDto  : NSObject, NSCoding{
    
    var message : String!
    var status : Bool!
    var today : DashboardDailyDtoToday!
    var todayRaw : DashboardDailyDtoTodayRaw!
    var yesterday : DashboardDailyDtoYesterday!
    var yesterdayRaw : DashboardDailyDtoYesterdayRaw!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        status = json["status"].boolValue
        let todayJson = json["today"]
        if !todayJson.isEmpty{
            today = DashboardDailyDtoToday(fromJson: todayJson)
        }
        let todayRawJson = json["today_raw"]
        if !todayRawJson.isEmpty{
            todayRaw = DashboardDailyDtoTodayRaw(fromJson: todayRawJson)
        }
        let yesterdayJson = json["yesterday"]
        if !yesterdayJson.isEmpty{
            yesterday = DashboardDailyDtoYesterday(fromJson: yesterdayJson)
        }
        let yesterdayRawJson = json["yesterday_raw"]
        if !yesterdayRawJson.isEmpty{
            yesterdayRaw = DashboardDailyDtoYesterdayRaw(fromJson: yesterdayRawJson)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if today != nil{
            dictionary["today"] = today.toDictionary()
        }
        if todayRaw != nil{
            dictionary["todayRaw"] = todayRaw.toDictionary()
        }
        if yesterday != nil{
            dictionary["yesterday"] = yesterday.toDictionary()
        }
        if yesterdayRaw != nil{
            dictionary["yesterdayRaw"] = yesterdayRaw.toDictionary()
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        today = aDecoder.decodeObject(forKey: "today") as? DashboardDailyDtoToday
        todayRaw = aDecoder.decodeObject(forKey: "today_raw") as? DashboardDailyDtoTodayRaw
        yesterday = aDecoder.decodeObject(forKey: "yesterday") as? DashboardDailyDtoYesterday
        yesterdayRaw = aDecoder.decodeObject(forKey: "yesterday_raw") as? DashboardDailyDtoYesterdayRaw
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if today != nil{
            aCoder.encode(today, forKey: "today")
        }
        if todayRaw != nil{
            aCoder.encode(todayRaw, forKey: "today_raw")
        }
        if yesterday != nil{
            aCoder.encode(yesterday, forKey: "yesterday")
        }
        if yesterdayRaw != nil{
            aCoder.encode(yesterdayRaw, forKey: "yesterday_raw")
        }
        
    }
    
}




class DashboardDailyDtoYesterday : NSObject, NSCoding{
    
    var date : String!
    var yesterdayBrent : Float!
    var yesterdayChinaPsfPrice : Float!
    var yesterdayCottonMaund : Float!
    var yesterdayCrude : Float!
    var yesterdayCyn : Float!
    var yesterdayEur : Float!
    var yesterdayFob : Float!
    var yesterdayGbp : Float!
    var yesterdayIciSemi : Float!
    var yesterdayImportOffer : Float!
    var yesterdayJpy : Float!
    var yesterdayMeg : Float!
    var yesterdayNyf : Float!
    var yesterdayNyfMaund : Float!
    var yesterdayNyfPkr : Float!
    var yesterdayPsfPrice : Float!
    var yesterdayPta : Float!
    var yesterdayPx : Float!
    var yesterdayUsd : Float!
    var yesterdayWtl : Float!
    var yesterdayYarn20S : Float!
    var yesterdayYarn26Pc : Float!
    var yesterdayYarn30Pc : Float!
    var yesterdayYarn36Pc : Float!
    var yesterdayYarn38Pc : Float!
    var yesterdayYarn50Pp : Float!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        date = json["date"].stringValue
        yesterdayBrent = json["yesterday_brent"].floatValue
        yesterdayChinaPsfPrice = json["yesterday_china_psf_price"].floatValue
        yesterdayCottonMaund = json["yesterday_cotton_maund"].floatValue
        yesterdayCrude = json["yesterday_crude"].floatValue
        yesterdayCyn = json["yesterday_cyn"].floatValue
        yesterdayEur = json["yesterday_eur"].floatValue
        yesterdayFob = json["yesterday_fob"].floatValue
        yesterdayGbp = json["yesterday_gbp"].floatValue
        yesterdayIciSemi = json["yesterday_ici_semi"].floatValue
        yesterdayImportOffer = json["yesterday_import_offer"].floatValue
        yesterdayJpy = json["yesterday_jpy"].floatValue
        yesterdayMeg = json["yesterday_meg"].floatValue
        yesterdayNyf = json["yesterday_nyf"].floatValue
        yesterdayNyfMaund = json["yesterday_nyf_maund"].floatValue
        yesterdayNyfPkr = json["yesterday_nyf_pkr"].floatValue
        yesterdayPsfPrice = json["yesterday_psf_price"].floatValue
        yesterdayPta = json["yesterday_pta"].floatValue
        yesterdayPx = json["yesterday_px"].floatValue
        yesterdayUsd = json["yesterday_usd"].floatValue
        yesterdayWtl = json["yesterday_wtl"].floatValue
        yesterdayYarn20S = json["yesterday_yarn_20_s"].floatValue
        yesterdayYarn26Pc = json["yesterday_yarn_26_pc"].floatValue
        yesterdayYarn30Pc = json["yesterday_yarn_30_pc"].floatValue
        yesterdayYarn36Pc = json["yesterday_yarn_36_pc"].floatValue
        yesterdayYarn38Pc = json["yesterday_yarn_38_pc"].floatValue
        yesterdayYarn50Pp = json["yesterday_yarn_50_pp"].floatValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["date"] = date
        }
        if yesterdayBrent != nil{
            dictionary["yesterday_brent"] = yesterdayBrent
        }
        if yesterdayChinaPsfPrice != nil{
            dictionary["yesterday_china_psf_price"] = yesterdayChinaPsfPrice
        }
        if yesterdayCottonMaund != nil{
            dictionary["yesterday_cotton_maund"] = yesterdayCottonMaund
        }
        if yesterdayCrude != nil{
            dictionary["yesterday_crude"] = yesterdayCrude
        }
        if yesterdayCyn != nil{
            dictionary["yesterday_cyn"] = yesterdayCyn
        }
        if yesterdayEur != nil{
            dictionary["yesterday_eur"] = yesterdayEur
        }
        if yesterdayFob != nil{
            dictionary["yesterday_fob"] = yesterdayFob
        }
        if yesterdayGbp != nil{
            dictionary["yesterday_gbp"] = yesterdayGbp
        }
        if yesterdayIciSemi != nil{
            dictionary["yesterday_ici_semi"] = yesterdayIciSemi
        }
        if yesterdayImportOffer != nil{
            dictionary["yesterday_import_offer"] = yesterdayImportOffer
        }
        if yesterdayJpy != nil{
            dictionary["yesterday_jpy"] = yesterdayJpy
        }
        if yesterdayMeg != nil{
            dictionary["yesterday_meg"] = yesterdayMeg
        }
        if yesterdayNyf != nil{
            dictionary["yesterday_nyf"] = yesterdayNyf
        }
        if yesterdayNyfMaund != nil{
            dictionary["yesterday_nyf_maund"] = yesterdayNyfMaund
        }
        if yesterdayNyfPkr != nil{
            dictionary["yesterday_nyf_pkr"] = yesterdayNyfPkr
        }
        if yesterdayPsfPrice != nil{
            dictionary["yesterday_psf_price"] = yesterdayPsfPrice
        }
        if yesterdayPta != nil{
            dictionary["yesterday_pta"] = yesterdayPta
        }
        if yesterdayPx != nil{
            dictionary["yesterday_px"] = yesterdayPx
        }
        if yesterdayUsd != nil{
            dictionary["yesterday_usd"] = yesterdayUsd
        }
        if yesterdayWtl != nil{
            dictionary["yesterday_wtl"] = yesterdayWtl
        }
        if yesterdayYarn20S != nil{
            dictionary["yesterday_yarn_20_s"] = yesterdayYarn20S
        }
        if yesterdayYarn26Pc != nil{
            dictionary["yesterday_yarn_26_pc"] = yesterdayYarn26Pc
        }
        if yesterdayYarn30Pc != nil{
            dictionary["yesterday_yarn_30_pc"] = yesterdayYarn30Pc
        }
        if yesterdayYarn36Pc != nil{
            dictionary["yesterday_yarn_36_pc"] = yesterdayYarn36Pc
        }
        if yesterdayYarn38Pc != nil{
            dictionary["yesterday_yarn_38_pc"] = yesterdayYarn38Pc
        }
        if yesterdayYarn50Pp != nil{
            dictionary["yesterday_yarn_50_pp"] = yesterdayYarn50Pp
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        date = aDecoder.decodeObject(forKey: "date") as? String
        yesterdayBrent = aDecoder.decodeObject(forKey: "yesterday_brent") as? Float
        yesterdayChinaPsfPrice = aDecoder.decodeObject(forKey: "yesterday_china_psf_price") as? Float
        yesterdayCottonMaund = aDecoder.decodeObject(forKey: "yesterday_cotton_maund") as? Float
        yesterdayCrude = aDecoder.decodeObject(forKey: "yesterday_crude") as? Float
        yesterdayCyn = aDecoder.decodeObject(forKey: "yesterday_cyn") as? Float
        yesterdayEur = aDecoder.decodeObject(forKey: "yesterday_eur") as? Float
        yesterdayFob = aDecoder.decodeObject(forKey: "yesterday_fob") as? Float
        yesterdayGbp = aDecoder.decodeObject(forKey: "yesterday_gbp") as? Float
        yesterdayIciSemi = aDecoder.decodeObject(forKey: "yesterday_ici_semi") as? Float
        yesterdayImportOffer = aDecoder.decodeObject(forKey: "yesterday_import_offer") as? Float
        yesterdayJpy = aDecoder.decodeObject(forKey: "yesterday_jpy") as? Float
        yesterdayMeg = aDecoder.decodeObject(forKey: "yesterday_meg") as? Float
        yesterdayNyf = aDecoder.decodeObject(forKey: "yesterday_nyf") as? Float
        yesterdayNyfMaund = aDecoder.decodeObject(forKey: "yesterday_nyf_maund") as? Float
        yesterdayNyfPkr = aDecoder.decodeObject(forKey: "yesterday_nyf_pkr") as? Float
        yesterdayPsfPrice = aDecoder.decodeObject(forKey: "yesterday_psf_price") as? Float
        yesterdayPta = aDecoder.decodeObject(forKey: "yesterday_pta") as? Float
        yesterdayPx = aDecoder.decodeObject(forKey: "yesterday_px") as? Float
        yesterdayUsd = aDecoder.decodeObject(forKey: "yesterday_usd") as? Float
        yesterdayWtl = aDecoder.decodeObject(forKey: "yesterday_wtl") as? Float
        yesterdayYarn20S = aDecoder.decodeObject(forKey: "yesterday_yarn_20_s") as? Float
        yesterdayYarn26Pc = aDecoder.decodeObject(forKey: "yesterday_yarn_26_pc") as? Float
        yesterdayYarn30Pc = aDecoder.decodeObject(forKey: "yesterday_yarn_30_pc") as? Float
        yesterdayYarn36Pc = aDecoder.decodeObject(forKey: "yesterday_yarn_36_pc") as? Float
        yesterdayYarn38Pc = aDecoder.decodeObject(forKey: "yesterday_yarn_38_pc") as? Float
        yesterdayYarn50Pp = aDecoder.decodeObject(forKey: "yesterday_yarn_50_pp") as? Float
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if yesterdayBrent != nil{
            aCoder.encode(yesterdayBrent, forKey: "yesterday_brent")
        }
        if yesterdayChinaPsfPrice != nil{
            aCoder.encode(yesterdayChinaPsfPrice, forKey: "yesterday_china_psf_price")
        }
        if yesterdayCottonMaund != nil{
            aCoder.encode(yesterdayCottonMaund, forKey: "yesterday_cotton_maund")
        }
        if yesterdayCrude != nil{
            aCoder.encode(yesterdayCrude, forKey: "yesterday_crude")
        }
        if yesterdayCyn != nil{
            aCoder.encode(yesterdayCyn, forKey: "yesterday_cyn")
        }
        if yesterdayEur != nil{
            aCoder.encode(yesterdayEur, forKey: "yesterday_eur")
        }
        if yesterdayFob != nil{
            aCoder.encode(yesterdayFob, forKey: "yesterday_fob")
        }
        if yesterdayGbp != nil{
            aCoder.encode(yesterdayGbp, forKey: "yesterday_gbp")
        }
        if yesterdayIciSemi != nil{
            aCoder.encode(yesterdayIciSemi, forKey: "yesterday_ici_semi")
        }
        if yesterdayImportOffer != nil{
            aCoder.encode(yesterdayImportOffer, forKey: "yesterday_import_offer")
        }
        if yesterdayJpy != nil{
            aCoder.encode(yesterdayJpy, forKey: "yesterday_jpy")
        }
        if yesterdayMeg != nil{
            aCoder.encode(yesterdayMeg, forKey: "yesterday_meg")
        }
        if yesterdayNyf != nil{
            aCoder.encode(yesterdayNyf, forKey: "yesterday_nyf")
        }
        if yesterdayNyfMaund != nil{
            aCoder.encode(yesterdayNyfMaund, forKey: "yesterday_nyf_maund")
        }
        if yesterdayNyfPkr != nil{
            aCoder.encode(yesterdayNyfPkr, forKey: "yesterday_nyf_pkr")
        }
        if yesterdayPsfPrice != nil{
            aCoder.encode(yesterdayPsfPrice, forKey: "yesterday_psf_price")
        }
        if yesterdayPta != nil{
            aCoder.encode(yesterdayPta, forKey: "yesterday_pta")
        }
        if yesterdayPx != nil{
            aCoder.encode(yesterdayPx, forKey: "yesterday_px")
        }
        if yesterdayUsd != nil{
            aCoder.encode(yesterdayUsd, forKey: "yesterday_usd")
        }
        if yesterdayWtl != nil{
            aCoder.encode(yesterdayWtl, forKey: "yesterday_wtl")
        }
        if yesterdayYarn20S != nil{
            aCoder.encode(yesterdayYarn20S, forKey: "yesterday_yarn_20_s")
        }
        if yesterdayYarn26Pc != nil{
            aCoder.encode(yesterdayYarn26Pc, forKey: "yesterday_yarn_26_pc")
        }
        if yesterdayYarn30Pc != nil{
            aCoder.encode(yesterdayYarn30Pc, forKey: "yesterday_yarn_30_pc")
        }
        if yesterdayYarn36Pc != nil{
            aCoder.encode(yesterdayYarn36Pc, forKey: "yesterday_yarn_36_pc")
        }
        if yesterdayYarn38Pc != nil{
            aCoder.encode(yesterdayYarn38Pc, forKey: "yesterday_yarn_38_pc")
        }
        if yesterdayYarn50Pp != nil{
            aCoder.encode(yesterdayYarn50Pp, forKey: "yesterday_yarn_50_pp")
        }
        
    }
    
}




class DashboardDailyDtoToday : NSObject, NSCoding{
    
    var date : String!
    var todayBrent : Float!
    var todayChinaPsfPrice : Float!
    var todayCottonMaund : Float!
    var todayCrude : Float!
    var todayCyn : Float!
    var todayEur : Float!
    var todayFob : Float!
    var todayGbp : Float!
    var todayIciSemi : Float!
    var todayImportOffr : Float!
    var todayJpy : Float!
    var todayMeg : Float!
    var todayNyf : Float!
    var todayNyfMaund : Float!
    var todayNyfPkr : Float!
    var todayPsfPrice : Float!
    var todayPta : Float!
    var todayPx : Float!
    var todayUsd : Float!
    var todayWtl : Float!
    var todayYarn20S : Float!
    var todayYarn26Pc : Float!
    var todayYarn30Pc : Float!
    var todayYarn36Pc : Float!
    var todayYarn38Pc : Float!
    var todayYarn50Pp : Float!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        date = json["date"].stringValue
        todayBrent = json["today_brent"].floatValue
        todayChinaPsfPrice = json["today_china_psf_price"].floatValue
        todayCottonMaund = json["today_cotton_maund"].floatValue
        todayCrude = json["today_crude"].floatValue
        todayCyn = json["today_cyn"].floatValue
        todayEur = json["today_eur"].floatValue
        todayFob = json["today_fob"].floatValue
        todayGbp = json["today_gbp"].floatValue
        todayIciSemi = json["today_ici_semi"].floatValue
        todayImportOffr = json["today_import_offr"].floatValue
        todayJpy = json["today_jpy"].floatValue
        todayMeg = json["today_meg"].floatValue
        todayNyf = json["today_nyf"].floatValue
        todayNyfMaund = json["today_nyf_maund"].floatValue
        todayNyfPkr = json["today_nyf_pkr"].floatValue
        todayPsfPrice = json["today_psf_price"].floatValue
        todayPta = json["today_pta"].floatValue
        todayPx = json["today_px"].floatValue
        todayUsd = json["today_usd"].floatValue
        todayWtl = json["today_wtl"].floatValue
        todayYarn20S = json["today_yarn_20_s"].floatValue
        todayYarn26Pc = json["today_yarn_26_pc"].floatValue
        todayYarn30Pc = json["today_yarn_30_pc"].floatValue
        todayYarn36Pc = json["today_yarn_36_pc"].floatValue
        todayYarn38Pc = json["today_yarn_38_pc"].floatValue
        todayYarn50Pp = json["today_yarn_50_pp"].floatValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if date != nil{
            dictionary["date"] = date
        }
        if todayBrent != nil{
            dictionary["today_brent"] = todayBrent
        }
        if todayChinaPsfPrice != nil{
            dictionary["today_china_psf_price"] = todayChinaPsfPrice
        }
        if todayCottonMaund != nil{
            dictionary["today_cotton_maund"] = todayCottonMaund
        }
        if todayCrude != nil{
            dictionary["today_crude"] = todayCrude
        }
        if todayCyn != nil{
            dictionary["today_cyn"] = todayCyn
        }
        if todayEur != nil{
            dictionary["today_eur"] = todayEur
        }
        if todayFob != nil{
            dictionary["today_fob"] = todayFob
        }
        if todayGbp != nil{
            dictionary["today_gbp"] = todayGbp
        }
        if todayIciSemi != nil{
            dictionary["today_ici_semi"] = todayIciSemi
        }
        if todayImportOffr != nil{
            dictionary["today_import_offr"] = todayImportOffr
        }
        if todayJpy != nil{
            dictionary["today_jpy"] = todayJpy
        }
        if todayMeg != nil{
            dictionary["today_meg"] = todayMeg
        }
        if todayNyf != nil{
            dictionary["today_nyf"] = todayNyf
        }
        if todayNyfMaund != nil{
            dictionary["today_nyf_maund"] = todayNyfMaund
        }
        if todayNyfPkr != nil{
            dictionary["today_nyf_pkr"] = todayNyfPkr
        }
        if todayPsfPrice != nil{
            dictionary["today_psf_price"] = todayPsfPrice
        }
        if todayPta != nil{
            dictionary["today_pta"] = todayPta
        }
        if todayPx != nil{
            dictionary["today_px"] = todayPx
        }
        if todayUsd != nil{
            dictionary["today_usd"] = todayUsd
        }
        if todayWtl != nil{
            dictionary["today_wtl"] = todayWtl
        }
        if todayYarn20S != nil{
            dictionary["today_yarn_20_s"] = todayYarn20S
        }
        if todayYarn26Pc != nil{
            dictionary["today_yarn_26_pc"] = todayYarn26Pc
        }
        if todayYarn30Pc != nil{
            dictionary["today_yarn_30_pc"] = todayYarn30Pc
        }
        if todayYarn36Pc != nil{
            dictionary["today_yarn_36_pc"] = todayYarn36Pc
        }
        if todayYarn38Pc != nil{
            dictionary["today_yarn_38_pc"] = todayYarn38Pc
        }
        if todayYarn50Pp != nil{
            dictionary["today_yarn_50_pp"] = todayYarn50Pp
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        date = aDecoder.decodeObject(forKey: "date") as? String
        todayBrent = aDecoder.decodeObject(forKey: "today_brent") as? Float
        todayChinaPsfPrice = aDecoder.decodeObject(forKey: "today_china_psf_price") as? Float
        todayCottonMaund = aDecoder.decodeObject(forKey: "today_cotton_maund") as? Float
        todayCrude = aDecoder.decodeObject(forKey: "today_crude") as? Float
        todayCyn = aDecoder.decodeObject(forKey: "today_cyn") as? Float
        todayEur = aDecoder.decodeObject(forKey: "today_eur") as? Float
        todayFob = aDecoder.decodeObject(forKey: "today_fob") as? Float
        todayGbp = aDecoder.decodeObject(forKey: "today_gbp") as? Float
        todayIciSemi = aDecoder.decodeObject(forKey: "today_ici_semi") as? Float
        todayImportOffr = aDecoder.decodeObject(forKey: "today_import_offr") as? Float
        todayJpy = aDecoder.decodeObject(forKey: "today_jpy") as? Float
        todayMeg = aDecoder.decodeObject(forKey: "today_meg") as? Float
        todayNyf = aDecoder.decodeObject(forKey: "today_nyf") as? Float
        todayNyfMaund = aDecoder.decodeObject(forKey: "today_nyf_maund") as? Float
        todayNyfPkr = aDecoder.decodeObject(forKey: "today_nyf_pkr") as? Float
        todayPsfPrice = aDecoder.decodeObject(forKey: "today_psf_price") as? Float
        todayPta = aDecoder.decodeObject(forKey: "today_pta") as? Float
        todayPx = aDecoder.decodeObject(forKey: "today_px") as? Float
        todayUsd = aDecoder.decodeObject(forKey: "today_usd") as? Float
        todayWtl = aDecoder.decodeObject(forKey: "today_wtl") as? Float
        todayYarn20S = aDecoder.decodeObject(forKey: "today_yarn_20_s") as? Float
        todayYarn26Pc = aDecoder.decodeObject(forKey: "today_yarn_26_pc") as? Float
        todayYarn30Pc = aDecoder.decodeObject(forKey: "today_yarn_30_pc") as? Float
        todayYarn36Pc = aDecoder.decodeObject(forKey: "today_yarn_36_pc") as? Float
        todayYarn38Pc = aDecoder.decodeObject(forKey: "today_yarn_38_pc") as? Float
        todayYarn50Pp = aDecoder.decodeObject(forKey: "today_yarn_50_pp") as? Float
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if todayBrent != nil{
            aCoder.encode(todayBrent, forKey: "today_brent")
        }
        if todayChinaPsfPrice != nil{
            aCoder.encode(todayChinaPsfPrice, forKey: "today_china_psf_price")
        }
        if todayCottonMaund != nil{
            aCoder.encode(todayCottonMaund, forKey: "today_cotton_maund")
        }
        if todayCrude != nil{
            aCoder.encode(todayCrude, forKey: "today_crude")
        }
        if todayCyn != nil{
            aCoder.encode(todayCyn, forKey: "today_cyn")
        }
        if todayEur != nil{
            aCoder.encode(todayEur, forKey: "today_eur")
        }
        if todayFob != nil{
            aCoder.encode(todayFob, forKey: "today_fob")
        }
        if todayGbp != nil{
            aCoder.encode(todayGbp, forKey: "today_gbp")
        }
        if todayIciSemi != nil{
            aCoder.encode(todayIciSemi, forKey: "today_ici_semi")
        }
        if todayImportOffr != nil{
            aCoder.encode(todayImportOffr, forKey: "today_import_offr")
        }
        if todayJpy != nil{
            aCoder.encode(todayJpy, forKey: "today_jpy")
        }
        if todayMeg != nil{
            aCoder.encode(todayMeg, forKey: "today_meg")
        }
        if todayNyf != nil{
            aCoder.encode(todayNyf, forKey: "today_nyf")
        }
        if todayNyfMaund != nil{
            aCoder.encode(todayNyfMaund, forKey: "today_nyf_maund")
        }
        if todayNyfPkr != nil{
            aCoder.encode(todayNyfPkr, forKey: "today_nyf_pkr")
        }
        if todayPsfPrice != nil{
            aCoder.encode(todayPsfPrice, forKey: "today_psf_price")
        }
        if todayPta != nil{
            aCoder.encode(todayPta, forKey: "today_pta")
        }
        if todayPx != nil{
            aCoder.encode(todayPx, forKey: "today_px")
        }
        if todayUsd != nil{
            aCoder.encode(todayUsd, forKey: "today_usd")
        }
        if todayWtl != nil{
            aCoder.encode(todayWtl, forKey: "today_wtl")
        }
        if todayYarn20S != nil{
            aCoder.encode(todayYarn20S, forKey: "today_yarn_20_s")
        }
        if todayYarn26Pc != nil{
            aCoder.encode(todayYarn26Pc, forKey: "today_yarn_26_pc")
        }
        if todayYarn30Pc != nil{
            aCoder.encode(todayYarn30Pc, forKey: "today_yarn_30_pc")
        }
        if todayYarn36Pc != nil{
            aCoder.encode(todayYarn36Pc, forKey: "today_yarn_36_pc")
        }
        if todayYarn38Pc != nil{
            aCoder.encode(todayYarn38Pc, forKey: "today_yarn_38_pc")
        }
        if todayYarn50Pp != nil{
            aCoder.encode(todayYarn50Pp, forKey: "today_yarn_50_pp")
        }
        
    }
    
}


class DashboardDailyDtoYesterdayRaw : NSObject, NSCoding{
    
    var yesterdayMeg : Float!
    var yesterdayPta : Float!
    var yesterdayPx : Float!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        yesterdayMeg = json["yesterday_meg"].floatValue
        yesterdayPta = json["yesterday_pta"].floatValue
        yesterdayPx = json["yesterday_px"].floatValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if yesterdayMeg != nil{
            dictionary["yesterday_meg"] = yesterdayMeg
        }
        if yesterdayPta != nil{
            dictionary["yesterday_pta"] = yesterdayPta
        }
        if yesterdayPx != nil{
            dictionary["yesterday_px"] = yesterdayPx
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        yesterdayMeg = aDecoder.decodeObject(forKey: "yesterday_meg") as? Float
        yesterdayPta = aDecoder.decodeObject(forKey: "yesterday_pta") as? Float
        yesterdayPx = aDecoder.decodeObject(forKey: "yesterday_px") as? Float
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if yesterdayMeg != nil{
            aCoder.encode(yesterdayMeg, forKey: "yesterday_meg")
        }
        if yesterdayPta != nil{
            aCoder.encode(yesterdayPta, forKey: "yesterday_pta")
        }
        if yesterdayPx != nil{
            aCoder.encode(yesterdayPx, forKey: "yesterday_px")
        }
        
    }
    
}



class DashboardDailyDtoTodayRaw : NSObject, NSCoding{
    
    var todayMeg : Float!
    var todayPta : Float!
    var todayPx : Float!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        todayMeg = json["today_meg"].floatValue
        todayPta = json["today_pta"].floatValue
        todayPx = json["today_px"].floatValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if todayMeg != nil{
            dictionary["today_meg"] = todayMeg
        }
        if todayPta != nil{
            dictionary["today_pta"] = todayPta
        }
        if todayPx != nil{
            dictionary["today_px"] = todayPx
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        todayMeg = aDecoder.decodeObject(forKey: "today_meg") as? Float
        todayPta = aDecoder.decodeObject(forKey: "today_pta") as? Float
        todayPx = aDecoder.decodeObject(forKey: "today_px") as? Float
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if todayMeg != nil{
            aCoder.encode(todayMeg, forKey: "today_meg")
        }
        if todayPta != nil{
            aCoder.encode(todayPta, forKey: "today_pta")
        }
        if todayPx != nil{
            aCoder.encode(todayPx, forKey: "today_px")
        }
        
    }
    
}
