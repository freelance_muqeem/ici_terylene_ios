//
//  HomePageDto.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 13/01/2020.
//  Copyright © 2020 Half Tech. All rights reserved.
//

import Foundation
import Foundation
import SwiftyJSON


class HomePageDto  : NSObject, NSCoding{

    var message : String!
    var status : Bool!
    var video : String!

    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        status = json["status"].boolValue
        video = json["video"].stringValue
    }

    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if video != nil{
            dictionary["video"] = video
        }
        return dictionary
    }

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        video = aDecoder.decodeObject(forKey: "video") as? String
    }

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if video != nil{
            aCoder.encode(video, forKey: "video")
        }

    }

}
