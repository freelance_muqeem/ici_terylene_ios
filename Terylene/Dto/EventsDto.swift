//
//  EventsDto.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 18/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation

import Foundation
import SwiftyJSON


class EventsDto  : NSObject, NSCoding{
    
    var hse : [EventsDtoHse]!
    var message : String!
    var status : Bool!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        hse = [EventsDtoHse]()
        let hseArray = json["hse"].arrayValue
        for hseJson in hseArray{
            let value = EventsDtoHse(fromJson: hseJson)
            hse.append(value)
        }
        message = json["message"].stringValue
        status = json["status"].boolValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if hse != nil{
            var dictionaryElements = [[String:Any]]()
            for hseElement in hse {
                dictionaryElements.append(hseElement.toDictionary())
            }
            dictionary["hse"] = dictionaryElements
        }
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        hse = aDecoder.decodeObject(forKey: "hse") as? [EventsDtoHse]
        message = aDecoder.decodeObject(forKey: "message") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if hse != nil{
            aCoder.encode(hse, forKey: "hse")
        }
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}





class EventsDtoHse : NSObject, NSCoding{
    
    var address : String!
    var content : String!
    var createdAt : String!
    var eventDate : String!
    var heading : String!
    var id : Int!
    var image : String!
    var status : String!
    var url : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        address = json["address"].stringValue
        content = json["content"].stringValue
        createdAt = json["created_at"].stringValue
        eventDate = json["event_date"].stringValue
        heading = json["heading"].stringValue
        id = json["id"].intValue
        image = json["image"].stringValue
        status = json["status"].stringValue
        url = json["url"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if address != nil{
            dictionary["address"] = address
        }
        if content != nil{
            dictionary["content"] = content
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if eventDate != nil{
            dictionary["event_date"] = eventDate
        }
        if heading != nil{
            dictionary["heading"] = heading
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if status != nil{
            dictionary["status"] = status
        }
        if url != nil{
            dictionary["url"] = url
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        address = aDecoder.decodeObject(forKey: "address") as? String
        content = aDecoder.decodeObject(forKey: "content") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        eventDate = aDecoder.decodeObject(forKey: "event_date") as? String
        heading = aDecoder.decodeObject(forKey: "heading") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        url = aDecoder.decodeObject(forKey: "url") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if address != nil{
            aCoder.encode(address, forKey: "address")
        }
        if content != nil{
            aCoder.encode(content, forKey: "content")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if eventDate != nil{
            aCoder.encode(eventDate, forKey: "event_date")
        }
        if heading != nil{
            aCoder.encode(heading, forKey: "heading")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if url != nil{
            aCoder.encode(url, forKey: "url")
        }
        
    }
    
}

