//
//  SpecialDto.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 22/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON


class SpecialDto  : NSObject, NSCoding{
    
    var message : String!
    var reports : [SpecialDtoReport]!
    var status : Bool!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        reports = [SpecialDtoReport]()
        let reportsArray = json["Reports"].arrayValue
        for reportsJson in reportsArray{
            let value = SpecialDtoReport(fromJson: reportsJson)
            reports.append(value)
        }
        status = json["status"].boolValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if reports != nil{
            var dictionaryElements = [[String:Any]]()
            for reportsElement in reports {
                dictionaryElements.append(reportsElement.toDictionary())
            }
            dictionary["reports"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        reports = aDecoder.decodeObject(forKey: "Reports") as? [SpecialDtoReport]
        status = aDecoder.decodeObject(forKey: "status") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if reports != nil{
            aCoder.encode(reports, forKey: "Reports")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}




class SpecialDtoReport : NSObject, NSCoding{
    
    var datetime : String!
    var id : Int!
    var image : String!
    var pdf : String!
    var status : String!
    var title : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        datetime = json["datetime"].stringValue
        id = json["id"].intValue
        image = json["image"].stringValue
        pdf = json["pdf"].stringValue
        status = json["status"].stringValue
        title = json["title"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if datetime != nil{
            dictionary["datetime"] = datetime
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if pdf != nil{
            dictionary["pdf"] = pdf
        }
        if status != nil{
            dictionary["status"] = status
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        datetime = aDecoder.decodeObject(forKey: "datetime") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        pdf = aDecoder.decodeObject(forKey: "pdf") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        title = aDecoder.decodeObject(forKey: "title") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if datetime != nil{
            aCoder.encode(datetime, forKey: "datetime")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if pdf != nil{
            aCoder.encode(pdf, forKey: "pdf")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if title != nil{
            aCoder.encode(title, forKey: "title")
        }
        
    }
    
}
