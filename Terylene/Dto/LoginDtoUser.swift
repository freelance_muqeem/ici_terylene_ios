//
//  LoginDtoUser.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 18/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON


class LoginDtoUser : NSObject, NSCoding{
    
    var fcmToken : String!
    var status : String!
    var userCompany : String!
    var userEmail : String!
    var userId : Int!
    var userName : String!
    var userPhone : String!
    var userToken : String!
    var userType : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        fcmToken = json["fcm_token"].stringValue
        status = json["status"].stringValue
        userCompany = json["user_company"].stringValue
        userEmail = json["user_email"].stringValue
        userId = json["user_id"].intValue
        userName = json["user_name"].stringValue
        userPhone = json["user_phone"].stringValue
        userToken = json["user_token"].stringValue
        userType = json["user_type"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if fcmToken != nil{
            dictionary["fcm_token"] = fcmToken
        }
        if status != nil{
            dictionary["status"] = status
        }
        if userCompany != nil{
            dictionary["user_company"] = userCompany
        }
        if userEmail != nil{
            dictionary["user_email"] = userEmail
        }
        if userId != nil{
            dictionary["user_id"] = userId
        }
        if userName != nil{
            dictionary["user_name"] = userName
        }
        if userPhone != nil{
            dictionary["user_phone"] = userPhone
        }
        if userToken != nil{
            dictionary["user_token"] = userToken
        }
        if userType != nil{
            dictionary["user_type"] = userType
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        fcmToken = aDecoder.decodeObject(forKey: "fcm_token") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        userCompany = aDecoder.decodeObject(forKey: "user_company") as? String
        userEmail = aDecoder.decodeObject(forKey: "user_email") as? String
        userId = aDecoder.decodeObject(forKey: "user_id") as? Int
        userName = aDecoder.decodeObject(forKey: "user_name") as? String
        userPhone = aDecoder.decodeObject(forKey: "user_phone") as? String
        userToken = aDecoder.decodeObject(forKey: "user_token") as? String
        userType = aDecoder.decodeObject(forKey: "user_type") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if fcmToken != nil{
            aCoder.encode(fcmToken, forKey: "fcm_token")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if userCompany != nil{
            aCoder.encode(userCompany, forKey: "user_company")
        }
        if userEmail != nil{
            aCoder.encode(userEmail, forKey: "user_email")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "user_id")
        }
        if userName != nil{
            aCoder.encode(userName, forKey: "user_name")
        }
        if userPhone != nil{
            aCoder.encode(userPhone, forKey: "user_phone")
        }
        if userToken != nil{
            aCoder.encode(userToken, forKey: "user_token")
        }
        if userType != nil{
            aCoder.encode(userType, forKey: "user_type")
        }
        
    }
    
}
