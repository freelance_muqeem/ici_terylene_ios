//
//  GraphDto.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 20/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON


class GraphDto  : NSObject, NSCoding{
    
    var message : String!
    var status : Bool!
    var week : [GraphDtoWeek]!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        status = json["status"].boolValue
        week = [GraphDtoWeek]()
        let weekArray = json["week"].arrayValue
        for weekJson in weekArray{
            let value = GraphDtoWeek(fromJson: weekJson)
            week.append(value)
        }
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if status != nil{
            dictionary["status"] = status
        }
        if week != nil{
            var dictionaryElements = [[String:Any]]()
            for weekElement in week {
                dictionaryElements.append(weekElement.toDictionary())
            }
            dictionary["week"] = dictionaryElements
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        status = aDecoder.decodeObject(forKey: "status") as? Bool
        week = aDecoder.decodeObject(forKey: "week") as? [GraphDtoWeek]
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if week != nil{
            aCoder.encode(week, forKey: "week")
        }
        
    }
    
}


class GraphDtoWeek : NSObject, NSCoding{
    
    var brent : Float!
    var chinaPsfPrice : Float!
    var cny : Float!
    var cottonMaund : Float!
    var crude : Float!
    var date : String!
    var eur : Float!
    var gbp : Float!
    var iciSemi : Float!
    var importOffer : Float!
    var jpy : Float!
    var meg : Float!
    var nyf : Float!
    var nyfMaund : Float!
    var nyfPkr : Float!
    var psfPrice : Float!
    var pta : Float!
    var px : Float!
    var usd : Float!
    var wtl : Float!
    var yarn20S : Float!
    var yarn26Pc : Float!
    var yarn30Pc : Float!
    var yarn36Pc : Float!
    var yarn38Pc : Float!
    var yarn50Pp : Float!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        brent = json["brent"].floatValue
        chinaPsfPrice = json["china_psf_price"].floatValue
        cny = json["cny"].floatValue
        cottonMaund = json["cotton_maund"].floatValue
        crude = json["crude"].floatValue
        date = json["date"].stringValue
        eur = json["eur"].floatValue
        gbp = json["gbp"].floatValue
        iciSemi = json["ici_semi"].floatValue
        importOffer = json["import offer"].floatValue
        jpy = json["jpy"].floatValue
        meg = json["meg"].floatValue
        nyf = json["nyf"].floatValue
        nyfMaund = json["nyf_maund"].floatValue
        nyfPkr = json["nyf_pkr"].floatValue
        psfPrice = json["psf_price"].floatValue
        pta = json["pta"].floatValue
        px = json["px"].floatValue
        usd = json["usd"].floatValue
        wtl = json["wtl"].floatValue
        yarn20S = json["yarn_20_s"].floatValue
        yarn26Pc = json["yarn_26_pc"].floatValue
        yarn30Pc = json["yarn_30_pc"].floatValue
        yarn36Pc = json["yarn_36_pc"].floatValue
        yarn38Pc = json["yarn_38_pc"].floatValue
        yarn50Pp = json["yarn_50_pp"].floatValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if brent != nil{
            dictionary["brent"] = brent
        }
        if chinaPsfPrice != nil{
            dictionary["china_psf_price"] = chinaPsfPrice
        }
        if cny != nil{
            dictionary["cny"] = cny
        }
        if cottonMaund != nil{
            dictionary["cotton_maund"] = cottonMaund
        }
        if crude != nil{
            dictionary["crude"] = crude
        }
        if date != nil{
            dictionary["date"] = date
        }
        if eur != nil{
            dictionary["eur"] = eur
        }
        if gbp != nil{
            dictionary["gbp"] = gbp
        }
        if iciSemi != nil{
            dictionary["ici_semi"] = iciSemi
        }
        if importOffer != nil{
            dictionary["import offer"] = importOffer
        }
        if jpy != nil{
            dictionary["jpy"] = jpy
        }
        if meg != nil{
            dictionary["meg"] = meg
        }
        if nyf != nil{
            dictionary["nyf"] = nyf
        }
        if nyfMaund != nil{
            dictionary["nyf_maund"] = nyfMaund
        }
        if nyfPkr != nil{
            dictionary["nyf_pkr"] = nyfPkr
        }
        if psfPrice != nil{
            dictionary["psf_price"] = psfPrice
        }
        if pta != nil{
            dictionary["pta"] = pta
        }
        if px != nil{
            dictionary["px"] = px
        }
        if usd != nil{
            dictionary["usd"] = usd
        }
        if wtl != nil{
            dictionary["wtl"] = wtl
        }
        if yarn20S != nil{
            dictionary["yarn_20_s"] = yarn20S
        }
        if yarn26Pc != nil{
            dictionary["yarn_26_pc"] = yarn26Pc
        }
        if yarn30Pc != nil{
            dictionary["yarn_30_pc"] = yarn30Pc
        }
        if yarn36Pc != nil{
            dictionary["yarn_36_pc"] = yarn36Pc
        }
        if yarn38Pc != nil{
            dictionary["yarn_38_pc"] = yarn38Pc
        }
        if yarn50Pp != nil{
            dictionary["yarn_50_pp"] = yarn50Pp
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        brent = aDecoder.decodeObject(forKey: "brent") as? Float
        chinaPsfPrice = aDecoder.decodeObject(forKey: "china_psf_price") as? Float
        cny = aDecoder.decodeObject(forKey: "cny") as? Float
        cottonMaund = aDecoder.decodeObject(forKey: "cotton_maund") as? Float
        crude = aDecoder.decodeObject(forKey: "crude") as? Float
        date = aDecoder.decodeObject(forKey: "date") as? String
        eur = aDecoder.decodeObject(forKey: "eur") as? Float
        gbp = aDecoder.decodeObject(forKey: "gbp") as? Float
        iciSemi = aDecoder.decodeObject(forKey: "ici_semi") as? Float
        importOffer = aDecoder.decodeObject(forKey: "import offer") as? Float
        jpy = aDecoder.decodeObject(forKey: "jpy") as? Float
        meg = aDecoder.decodeObject(forKey: "meg") as? Float
        nyf = aDecoder.decodeObject(forKey: "nyf") as? Float
        nyfMaund = aDecoder.decodeObject(forKey: "nyf_maund") as? Float
        nyfPkr = aDecoder.decodeObject(forKey: "nyf_pkr") as? Float
        psfPrice = aDecoder.decodeObject(forKey: "psf_price") as? Float
        pta = aDecoder.decodeObject(forKey: "pta") as? Float
        px = aDecoder.decodeObject(forKey: "px") as? Float
        usd = aDecoder.decodeObject(forKey: "usd") as? Float
        wtl = aDecoder.decodeObject(forKey: "wtl") as? Float
        yarn20S = aDecoder.decodeObject(forKey: "yarn_20_s") as? Float
        yarn26Pc = aDecoder.decodeObject(forKey: "yarn_26_pc") as? Float
        yarn30Pc = aDecoder.decodeObject(forKey: "yarn_30_pc") as? Float
        yarn36Pc = aDecoder.decodeObject(forKey: "yarn_36_pc") as? Float
        yarn38Pc = aDecoder.decodeObject(forKey: "yarn_38_pc") as? Float
        yarn50Pp = aDecoder.decodeObject(forKey: "yarn_50_pp") as? Float
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if brent != nil{
            aCoder.encode(brent, forKey: "brent")
        }
        if chinaPsfPrice != nil{
            aCoder.encode(chinaPsfPrice, forKey: "china_psf_price")
        }
        if cny != nil{
            aCoder.encode(cny, forKey: "cny")
        }
        if cottonMaund != nil{
            aCoder.encode(cottonMaund, forKey: "cotton_maund")
        }
        if crude != nil{
            aCoder.encode(crude, forKey: "crude")
        }
        if date != nil{
            aCoder.encode(date, forKey: "date")
        }
        if eur != nil{
            aCoder.encode(eur, forKey: "eur")
        }
        if gbp != nil{
            aCoder.encode(gbp, forKey: "gbp")
        }
        if iciSemi != nil{
            aCoder.encode(iciSemi, forKey: "ici_semi")
        }
        if importOffer != nil{
            aCoder.encode(importOffer, forKey: "import offer")
        }
        if jpy != nil{
            aCoder.encode(jpy, forKey: "jpy")
        }
        if meg != nil{
            aCoder.encode(meg, forKey: "meg")
        }
        if nyf != nil{
            aCoder.encode(nyf, forKey: "nyf")
        }
        if nyfMaund != nil{
            aCoder.encode(nyfMaund, forKey: "nyf_maund")
        }
        if nyfPkr != nil{
            aCoder.encode(nyfPkr, forKey: "nyf_pkr")
        }
        if psfPrice != nil{
            aCoder.encode(psfPrice, forKey: "psf_price")
        }
        if pta != nil{
            aCoder.encode(pta, forKey: "pta")
        }
        if px != nil{
            aCoder.encode(px, forKey: "px")
        }
        if usd != nil{
            aCoder.encode(usd, forKey: "usd")
        }
        if wtl != nil{
            aCoder.encode(wtl, forKey: "wtl")
        }
        if yarn20S != nil{
            aCoder.encode(yarn20S, forKey: "yarn_20_s")
        }
        if yarn26Pc != nil{
            aCoder.encode(yarn26Pc, forKey: "yarn_26_pc")
        }
        if yarn30Pc != nil{
            aCoder.encode(yarn30Pc, forKey: "yarn_30_pc")
        }
        if yarn36Pc != nil{
            aCoder.encode(yarn36Pc, forKey: "yarn_36_pc")
        }
        if yarn38Pc != nil{
            aCoder.encode(yarn38Pc, forKey: "yarn_38_pc")
        }
        if yarn50Pp != nil{
            aCoder.encode(yarn50Pp, forKey: "yarn_50_pp")
        }
        
    }
    
}
