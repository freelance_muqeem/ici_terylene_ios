//
//  ProductDto.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 22/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import Foundation
import SwiftyJSON


class ProductDto  : NSObject, NSCoding{
    
    var message : String!
    var products : [ProductDtoProduct]!
    var status : Bool!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        message = json["message"].stringValue
        products = [ProductDtoProduct]()
        let productsArray = json["products"].arrayValue
        for productsJson in productsArray{
            let value = ProductDtoProduct(fromJson: productsJson)
            products.append(value)
        }
        status = json["status"].boolValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if message != nil{
            dictionary["message"] = message
        }
        if products != nil{
            var dictionaryElements = [[String:Any]]()
            for productsElement in products {
                dictionaryElements.append(productsElement.toDictionary())
            }
            dictionary["products"] = dictionaryElements
        }
        if status != nil{
            dictionary["status"] = status
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        message = aDecoder.decodeObject(forKey: "message") as? String
        products = aDecoder.decodeObject(forKey: "products") as? [ProductDtoProduct]
        status = aDecoder.decodeObject(forKey: "status") as? Bool
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if message != nil{
            aCoder.encode(message, forKey: "message")
        }
        if products != nil{
            aCoder.encode(products, forKey: "products")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        
    }
    
}

class ProductDtoProduct : NSObject, NSCoding{
    
    var content : String!
    var createdAt : String!
    var descriptionField : String!
    var heading : String!
    var id : Int!
    var image : String!
    var status : String!
    var video : String!
    var specifications : [ Specification]!

    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        content = json["content"].stringValue
        createdAt = json["created_at"].stringValue
        descriptionField = json["description"].stringValue
        heading = json["heading"].stringValue
        id = json["id"].intValue
        image = json["image"].stringValue
        status = json["status"].stringValue
        video = json["video"].stringValue
          specifications = [ Specification]()
        let specificationsArray = json["specifications"].arrayValue
        for specificationsJson in specificationsArray{
            let value =  Specification(fromJson: specificationsJson)
            specifications.append(value)
        }
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if content != nil{
            dictionary["content"] = content
        }
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if descriptionField != nil{
            dictionary["description"] = descriptionField
        }
        if heading != nil{
            dictionary["heading"] = heading
        }
        if id != nil{
            dictionary["id"] = id
        }
        if image != nil{
            dictionary["image"] = image
        }
        if status != nil{
            dictionary["status"] = status
        }
        if video != nil{
            dictionary["video"] = video
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        content = aDecoder.decodeObject(forKey: "content") as? String
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        descriptionField = aDecoder.decodeObject(forKey: "description") as? String
        heading = aDecoder.decodeObject(forKey: "heading") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        image = aDecoder.decodeObject(forKey: "image") as? String
        status = aDecoder.decodeObject(forKey: "status") as? String
        video = aDecoder.decodeObject(forKey: "video") as? String
        specifications = aDecoder.decodeObject(forKey: "specifications") as? [ Specification]

    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if content != nil{
            aCoder.encode(content, forKey: "content")
        }
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if descriptionField != nil{
            aCoder.encode(descriptionField, forKey: "description")
        }
        if heading != nil{
            aCoder.encode(heading, forKey: "heading")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if image != nil{
            aCoder.encode(image, forKey: "image")
        }
        if status != nil{
            aCoder.encode(status, forKey: "status")
        }
        if video != nil{
            aCoder.encode(video, forKey: "video")
        }
        if specifications != nil{
            aCoder.encode(specifications, forKey: "specifications")
        }
        
    }
    
}

class  Specification : NSObject, NSCoding{
    
    var createdAt : String!
    var id : Int!
    var name : String!
    var productId : Int!
    var value : String!
    
    /**
     * Instantiate the instance using the passed json values to set the properties values
     */
    init(fromJson json: JSON!){
        if json.isEmpty{
            return
        }
        createdAt = json["created_at"].stringValue
        id = json["id"].intValue
        name = json["name"].stringValue
        productId = json["product_id"].intValue
        value = json["value"].stringValue
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if createdAt != nil{
            dictionary["created_at"] = createdAt
        }
        if id != nil{
            dictionary["id"] = id
        }
        if name != nil{
            dictionary["name"] = name
        }
        if productId != nil{
            dictionary["product_id"] = productId
        }
        if value != nil{
            dictionary["value"] = value
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        createdAt = aDecoder.decodeObject(forKey: "created_at") as? String
        id = aDecoder.decodeObject(forKey: "id") as? Int
        name = aDecoder.decodeObject(forKey: "name") as? String
        productId = aDecoder.decodeObject(forKey: "product_id") as? Int
        value = aDecoder.decodeObject(forKey: "value") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    func encode(with aCoder: NSCoder)
    {
        if createdAt != nil{
            aCoder.encode(createdAt, forKey: "created_at")
        }
        if id != nil{
            aCoder.encode(id, forKey: "id")
        }
        if name != nil{
            aCoder.encode(name, forKey: "name")
        }
        if productId != nil{
            aCoder.encode(productId, forKey: "product_id")
        }
        if value != nil{
            aCoder.encode(value, forKey: "value")
        }
        
    }
    
}
