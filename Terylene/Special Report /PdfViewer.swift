//
//  PdfViewer.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 23/06/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import Alamofire
class PdfViewer: UIViewController,UIWebViewDelegate {
    
    let documentInteractionController = UIDocumentInteractionController()
    func share(url: URL) {
        documentInteractionController.url = url
        documentInteractionController.uti = url.typeIdentifier ?? "public.data, public.content"
        documentInteractionController.name = url.localizedName ?? url.lastPathComponent
        documentInteractionController.presentOptionsMenu(from: view.frame, in: view, animated: true)
    }
    
    @IBOutlet weak var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        AppUtils.app.showActivityIndicatory(uiView: self.view)

       if let url = URL(string: BDefaults.instance.getString(key: "pdfUrl"))
       {
        webview.loadRequest(URLRequest(url: url))
        webview.delegate = self
        }
        
        // Do any additional setup after loading the view.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        AppUtils.app.hideActivityIndicatory()
    }
    
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
    @IBAction func downloadPdf(_ sender: Any) {
//      let destination: DownloadRequest.DownloadFileDestination = { _, _ in
//           let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
//        let fileURL = documentsURL.appendingPathComponent("\(AppUtils.getCurrentTimeStamp()).pdf")
//
//           return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
//       }
//        Alamofire.download( BDefaults.instance.getString(key: "pdfUrl"), to: destination).response { (response) in
//            print("res ",response.destinationURL);
//
//        }
        AppUtils.app.showActivityIndicatory(uiView: self.view)

        
        guard let url = URL(string: BDefaults.instance.getString(key: "pdfUrl")) else { return }
               URLSession.shared.dataTask(with: url) { data, response, error in
                   guard let data = data, error == nil else { return }
                

                   let tmpURL = FileManager.default.temporaryDirectory
                    .appendingPathComponent(response?.suggestedFilename ?? "\(AppUtils.getCurrentTimeStamp()).pdf")
                   do {
                       try data.write(to: tmpURL)
                       DispatchQueue.main.async {
                        AppUtils.app.hideActivityIndicatory()

                           self.share(url: tmpURL)
                       }
                   } catch {
                       print(error)
                   }

               }.resume()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
//extension PdfViewer:  URLSessionDownloadDelegate {
//    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
//        print("downloadLocation:", location)
//        // create destination URL with the original pdf name
//        guard let url = downloadTask.originalRequest?.url else { return }
//        let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
//        let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
//        // delete original copy
//        try? FileManager.default.removeItem(at: destinationURL)
//        // copy from temp to Document
//        do {
//            try FileManager.default.copyItem(at: location, to: destinationURL)
//            self.pdfURL = destinationURL
//        } catch let error {
//            print("Copy Error: \(error.localizedDescription)")
//        }
//    }
//}
extension URL {
    var typeIdentifier: String? {
        return (try? resourceValues(forKeys: [.typeIdentifierKey]))?.typeIdentifier
    }
    var localizedName: String? {
        return (try? resourceValues(forKeys: [.localizedNameKey]))?.localizedName
    }
}
