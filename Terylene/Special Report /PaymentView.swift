//
//  PaymentView.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 24/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class PaymentView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back(_ sender: Any) {
        
        if BDefaults.instance.getString(key: "navig") == "pta"{
            AppUtils.app.startMainScreen()
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
