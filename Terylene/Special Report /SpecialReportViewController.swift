//
//  SpecialReportViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 4/9/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit


class SpecialReportViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if indexPath.row == 0
        {
//            topCellReports
            let cell =  tableView.dequeueReusableCell(withIdentifier: "topCellReports") as! TopReportsCell
            
            cell.reportTitle.text = self.reports[indexPath.row].title
            cell.img.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(self.reports[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
            
            if self.reports[indexPath.row].status == "paid"{
                cell.lockImg.image = UIImage(named: "lock_grey")
                cell.lockTitle.text = "Purchase to unlock"
            }else{
                cell.lockImg.image = UIImage(named: "eye_grey")
                cell.lockTitle.text = "view PDF"
            }
            
            
            
            
            
            return cell
        }
        else{
        let cell =  tableView.dequeueReusableCell(withIdentifier: "SpecialCellView") as! SpecialCell
        
        cell.reportTitle.text = self.reports[indexPath.row].title
        cell.img.sd_setImage(with: URL(string: "\(AppUtils.Image_BASE_URL)/\(self.reports[indexPath.row].image!)"), placeholderImage: UIImage(named: "picture.png"))
            if self.reports[indexPath.row].status == "paid"{
                cell.lockImg.image = UIImage(named: "lock_grey")
                cell.lockTitle.text = "Purchase to unlock"
            }else{
                cell.lockImg.image = UIImage(named: "eye_grey")
                cell.lockTitle.text = "view PDF"
            }
            
            
            
        return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.reports[indexPath.row].status == "paid"{
            performSegue(withIdentifier: "openPayment", sender: self)

        }else{
            performSegue(withIdentifier: "openPdf", sender: self)
            BDefaults.instance.setString(key: "pdfUrl", value: "\(AppUtils.Image_BASE_URL)/\(self.reports[indexPath.row].pdf!)")
            performSegue(withIdentifier: "openPdf", sender: self)
        }
        
        
        
        
       
    }
    
    
    @IBOutlet weak var reportsView: UITableView!
   
    
    var reports = [SpecialDtoReport]()
    var reports1 = [SpecialDtoReport]()
    override func viewDidLoad() {
        super.viewDidLoad()
        reportsView.backgroundColor = .clear
       
        
    reportsView.delegate = self
        reportsView.dataSource = self
        setData()
        // Do any additional setup after loading the view.
    }
    
  
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func setData(){
        AppUtils.app.showActivityIndicatory(uiView: self.view)
        NetworkManager.getSpecialReports(completion: { (dto) in
            AppUtils.app.hideActivityIndicatory()
            
            var dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            
            
            if(dto.reports.count>0){
                self.reports = dto.reports
                self.reports.sort{dateFormatter.date(from: $0.datetime)?.compare(dateFormatter.date(from: $1.datetime)!) == .orderedDescending
                    
                }
                
                self.reportsView.reloadData()

                
            }
            
           
            
            
        }) { (err) in
            AppUtils.app.hideActivityIndicatory()
        }
        
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
