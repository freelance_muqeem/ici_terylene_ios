//
//  TopReportsCell.swift
//  Terylene
//
//  Created by Muhammad Bilal Aslam on 17/07/2019.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit

class TopReportsCell: UITableViewCell {
    @IBOutlet weak var lockTitle: UILabel!
    @IBOutlet weak var lockImg: UIImageView!
    @IBOutlet weak var reportTitle: UILabel!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
