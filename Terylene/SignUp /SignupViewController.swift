//
//  SignupViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/21/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import SwiftyJSON

class SignupViewController: UIViewController {
    
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet var signupbtn: UIButton!
    var s = true

    @IBOutlet var name: HoshiTextField!
    @IBOutlet var email: HoshiTextField!
    @IBOutlet var phone: HoshiTextField!
    @IBOutlet var company: HoshiTextField!
    @IBOutlet var password: HoshiTextField!
    
    var signupmodel: SignupModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = ""
        email.text = ""
        phone.text = ""
        company.text = ""
        password.text = ""
        password.isSecureTextEntry = true
        phone.delegate = self
        signupbtn.layer.cornerRadius = 10
        signupbtn.layer.borderWidth = 1
        signupbtn.layer.borderColor = UIColor.black.cgColor
        phone.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
         
        // Do any additional setup after loading the view.
    }
    
    
    
    //// checking Fields are Empty
    
    func areTextFieldsEmpty() -> Bool {
        
        if (name.text?.isEmpty)! || (email.text?.isEmpty)! || (phone.text?.isEmpty)! ||  (company.text?.isEmpty)! || (password.text?.isEmpty)! || (password.text?.isEmpty)!{
            //Some of field is left empty
            return false
        } else {
            // All fields are filled
            return true
        }
    }
    
    // Function for gmail Validation
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    /// SignUp Button
    @IBAction func signUpBtn(_ sender: Any) {
        
        
        if  isValidEmail(testStr: email.text!) == false {
            alertMessage(userMessage: "Please Enter your Correct Email")
            return
        }
        if ((password.text?.count)! < 8) {
            alertMessage(userMessage: "Enter 8 Character Password")
            return
        }
        
        if areTextFieldsEmpty() {
            //send data to sever
            fetchSignupData ()
        } else {
            alertMessage(userMessage: "Please Complete all the Fields")
        }
        
        
        
    }
    
    
    @IBAction func cancelBtn(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //// Login Btn
    @IBAction func loginBtn(_ sender: Any) {
        
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! UIViewController
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    /// Call to Login Screen
    //    func callLoginScreen()  {
    //
    //        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
    //        self.navigationController?.pushViewController(loginVC, animated: true)
    //    }
    
    
    
    
}

extension SignupViewController {
    
    func  fetchSignupData () {
        
        
        
        let header = ["Client_Service": "AppSkNoCkQr-#$%",
                      "Auth_Key":"tIsfoRItIsfoR^S@fTy!@).(@^S@fTy!@).(@"
        ]
        
        let parameter =  [
            
            
            "email":         email.text! ,
            "name":           name.text! ,
            "password" :     password.text! ,
            "phone":          phone.text! ,
            "company":     company.text!
            
        ]
        
        appdelegate.showActivityIndicatory(uiView: self.view)
        
        Alamofire.request("\(AppUtils.BASE_URL)/Signup", method: .post, parameters: parameter as Parameters , encoding: URLEncoding.default, headers: AppUtils.loginHeader).responseJSON { (response) in
            self.appdelegate.hideActivityIndicatory()
            print(response)
            let jsonResponse = JSON(response.result.value!)
            self.signupmodel = SignupModel(response:jsonResponse)
            if self.signupmodel.status == "False"{
                
                
                
                let toast = AppUtils.returnErrorToast(string: "Email already registered")
                self.present(toast, animated: true)
                
            } else {
                
                //show Alter
                self.performSegue(withIdentifier: "openConfirmaion", sender: self)
                
                
            }
            
        }
        
    }
    
}
extension SignupViewController : UITextFieldDelegate {
    
    
    
    @objc  func textFieldDidChange(_ textField: UITextField){
                
            
                if textField.text?.count ?? 0 == 4{
                    if s{
                        textField.text = "\(textField.text!)-"
                        s = false
                    }
                  
                   
                }
         
        if textField.text?.count ?? 0 < 4 {
                s = true
        
        }
         
     }

    
    
}
