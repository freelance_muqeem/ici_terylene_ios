//
//  AppUtils.swift
//  WbmLogisticsUser
//
//  Created by Muhammad Bilal Aslam on 04/12/2018.
//  Copyright © 2018 Muhammad Bilal Aslam. All rights reserved.
//

import Foundation
import SwiftToast
import SwiftyJSON
import Charts
import CoreData
import Alamofire
class AppUtils{
    
    static let APPOINTMENT_REMINDER = "appointment_reminder";
    static let AuthKey = "ItIsfoRItIsfoR^S@fTy!@).(@^S@fTy!@).(@";
    static let ClientService = "AppSkNoCkQr-#$%";
    //API Links
    
    //old
    //    public static let BASE_URL = "https://half-tech.com/terelyne/";
    //    public static let Image_BASE_URL = "https://half-tech.com/terelyn/";
    
    //new
    public static let BASE_URL = "http://api.teryleneapp.com/";
    public static let Image_BASE_URL = "https://teryleneapp.com/";
    
    
    public static var companies = [Company]()
    public static var companiesData = [CompanyData]()
    static var month =  [GraphDtoWeek]()
    static var week =  [GraphDtoWeek]()
    
    
    
    static let graphColor = UIColor(red: 0/255, green: 10/255, blue: 100/255, alpha: 0.15).cgColor
    
    static let color = UIColor(red: 0/255, green: 10/255, blue: 100/255, alpha: 1.0).cgColor
    static let lightColor = UIColor(red: 17/255, green: 108/255, blue: 214/255, alpha: 1.0).cgColor
    static let redColor = UIColor(red: 255/255, green: 99/255, blue: 93/255, alpha: 1.0).cgColor
    static let greenColor = UIColor(red: 52/255, green: 199/255, blue: 89/255, alpha: 1.0).cgColor
    static let yellowColor = UIColor(red: 232/255, green: 176/255, blue: 25/255, alpha: 1.0).cgColor
    
    
    static let b1 = UIColor(red: 47/255, green: 47/255, blue: 47/255, alpha: 1.0).cgColor
    static let b2 = UIColor(red: 18/255, green: 18/255, blue: 18/255, alpha: 1.0).cgColor
    
    static let grey = UIColor(red: 182/255, green: 182/255, blue: 182/255, alpha: 0.2).cgColor
    
    static let darkGrey = UIColor(red: 182/255, green: 182/255, blue: 182/255, alpha: 1).cgColor
    static let context = (UIApplication.shared.delegate as! AppDelegate ).persistentContainer.viewContext
    static let app = (UIApplication.shared.delegate as! AppDelegate )
    static func returnBaseUrl() ->String{
        return
            AppUtils.BASE_URL
    }
    static let googleAPIKey = "AIzaSyAoYdOt0Xv8Up93Hgt5kAKt107UU8uQATg"
    static let loginHeader = ["ClientService":AppUtils.ClientService,"AuthKey":AppUtils.AuthKey]
    static  let headers = ["ClientService": "AppSkNoCkQr-#$%",
                           "AuthKey":"ItIsfoRItIsfoR^S@fTy!@).(@^S@fTy!@).(@",
                           "Token" :  BDefaults.instance.getUsers()[0].userToken!
    ]
    
    
    
    static  func deleteEntityAllData(entity : String) {
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do{
            let result = try AppUtils.context.execute(request)
        }
        catch{
            print(error)
        }
        
        
    }
    
    
    
    static func returnPercentage(a : Float,b : Float) -> String{
        
        let ro = ((a-b)*100)/b
        return String(format: "%.2f", ro)
    }
    
    static func sub(a : Float,b : Float) -> String{
        
        let ro = a-b
        
        if ro > 0
        {return String(format: "+%.2f", ro)}
        else
        {
            return String(format: "%.2f", ro)
        }}
    
    
    
    static func saveChanges()  {
        do{
            try self.context.save()
        }
        catch{
            print("some error to save this")
        }
    }
    
    
    static func returnErrorToast(string : String) -> SwiftToast {
        return SwiftToast(
            text: string,
            textAlignment: .center,
            image: UIImage(named: "icAlert"),
            backgroundColor: UIColor.darkGray,
            textColor: .white,
            font: .boldSystemFont(ofSize: 15.0),
            duration: 1.0,
            minimumHeight: CGFloat(100.0),
            statusBarStyle: .lightContent,
            aboveStatusBar: true,
            target: nil,
            style: .navigationBar)
    }
    
    //
    //    static func sortOrderArray(orders: [Order]) -> [Order]{
    //        var convertedOrders = [Order]()
    //
    //        var dateFormatter = DateFormatter()
    //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"// yyyy-MM-dd"
    //
    //
    //
    //        //        var ready = orders.sorted(by: { $0.compare($1) == .orderedDescending })
    //        let ready = orders.sorted { (order, order1) -> Bool in
    //
    //            return dateFormatter.date(from: order.created)?.compare(dateFormatter.date(from: order1.created)!) == ComparisonResult.orderedDescending
    //
    //        }
    //        print(ready)
    //        return ready
    //
    //    }
    //
    
    static func returnToast(string : String) -> SwiftToast {
        return SwiftToast(
            text: string,
            textAlignment: .center,
            image: UIImage(named: "done"),
            backgroundColor: UIColor.white,
            textColor: UIColor.green,
            font: .boldSystemFont(ofSize: 15.0),
            duration: 1.0,
            minimumHeight: CGFloat(100.0),
            statusBarStyle: .lightContent,
            aboveStatusBar: true,
            target: nil,
            style: .navigationBar)
    }
    
    
    
    
    
    static func returnBase64Image(image : UIImage) -> String {
        let imageData:NSData = image.pngData()! as NSData
        let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print(imageStr)
        return imageStr
    }
    
    static func returnBase64Image(imageData : Data) -> String {
        
        let imageStr = imageData.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
        print(imageStr)
        return imageStr
    }
    
    
    
    
    
    
    
    
    static func addHours(hours : Int) -> String{
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .hour, value: hours, to: Date())
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        print(dateFormatterGet.string(from: date!))
        return dateFormatterGet.string(from: date!)
    }
    static func addMonth(months : Int) -> String{
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .month, value: months, to: Date())
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        print(dateFormatterGet.string(from: date!))
        return dateFormatterGet.string(from: date!)
    }
    
    static func addDays(d : Int) -> String{
        let calendar = Calendar.current
        let date = calendar.date(byAdding: .day, value: d, to: Date())
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy"
        print(dateFormatterGet.string(from: date!))
        return dateFormatterGet.string(from: date!)
    }
    
    
    static func getDatefromString(date : String) ->Date{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let date = dateFormatterGet.date(from: date) {
            return date
        } else {
            return Date()
        }
    }
    
    
    
    static func formateDate(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatterGet.timeZone = TimeZone.init(abbreviation: "UTC")
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM d, yyyy"
        dateFormatterPrint.timeZone = TimeZone.current
        
        if let date = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: date))
            return dateFormatterPrint.string(from: date)
        } else {
            print("There was an error decoding the string")
            return "nul"
        }
    }
    
    
    static func formateDateNews(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM d, yyyy"
        
        if let date = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: date))
            return dateFormatterPrint.string(from: date)
        } else {
            print("There was an error decoding the string")
            return "nul"
        }
    }
    
    static func formateGraph(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM"
        
        
        if let date = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: date))
            return dateFormatterPrint.string(from: date)
        } else {
            print("There was an error decoding the string")
            return "0"
        }
    }
    
    static func saleGraph(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd/MM/yyyy"
        
        
        if let date = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: date))
            return dateFormatterPrint.string(from: date)
        } else {
            print("There was an error decoding the string")
            return "0"
        }
    }
    
    
    static func getMonth(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM"
        
        
        if let date = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: date))
            return dateFormatterPrint.string(from: date)
        } else {
            print("There was an error decoding the string")
            return "0"
        }
    }
    
    
    
    static func formateEvent(date : String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM d, yyyy "
        
        
        if let date = dateFormatterGet.date(from: date) {
            print(dateFormatterPrint.string(from: date))
            return dateFormatterPrint.string(from: date)
        } else {
            print("There was an error decoding the string")
            return "nul"
        }
    }
    
    
    
    
    static  func getCurrentTimeStamp() -> String {
        
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        print(dateFormatterGet.string(from: date))
        return dateFormatterGet.string(from: date)
        
    }
    
    static  func getCurrentTimeShort() -> String {
        
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "dd/MM/yyyy"
        print(dateFormatterGet.string(from: date))
        return dateFormatterGet.string(from: date)
        
    }
    
    
    static  func getGraphDate() -> String {
        
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy"
        print(dateFormatterGet.string(from: date))
        return dateFormatterGet.string(from: date)
        
    }
    
    
    
    static  func getMainDate() -> String {
        
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "EEEE, d MMMM"
        print(dateFormatterGet.string(from: date))
        return dateFormatterGet.string(from: date)
        
    }
    
    static  func getCurrentDateTime() -> String {
        
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "EEEE, MMMM d, h:mm aa"
        print(dateFormatterGet.string(from: date))
        return dateFormatterGet.string(from: date)
        
    }
    
    static  func getCurrencies() -> [String] {
        let arr = ["USD - $",            "CNY - ¥"
            ,"EUR - €"
            ,"JPY - ¥"
            ,"GBP - £"]
        return arr
        
    }
    
    static func getSection5() -> [String]{
        let arr = ["NYF cents/lb",            "NYF PKR/maund"
            ,"NYF PKR/kg"
        ]
        return arr
        
        
    }
    
    static func getSection4() -> [String]{
        let arr = ["China PSF Price",
                   "Import Offer"
        ]
        return arr
        
    }
    
    
    static func getSection6Graph() -> [String]{
        let arr = [
            
            "Yarn Rates - 50PC",
            "Yarn Rates - 38PC",
            "Yarn Rates - 36PC"
            ,"Yarn Rates - 30PC"
            ,"Yarn Rates - 26PC"
            ,"Yarn Rates - 20PC"
        ]
        return arr
        
    }
    
    
    static func getSection5Graph() -> [String]{
        let arr = [
            
            "Cotton",
            "NYF cents/lb",
            "NYF PKR/maund"
            ,"NYF PKR/kg"
        ]
        return arr
        
    }
    
    
    static func getSection4Graph() -> [String]{
        let arr = [
            
            "ICI Semi Dull",
            "China PSF Price",
            "Import Offer"
        ]
        return arr
        
    }
    static func getWeeklyCrude() -> [String]{
        let arr = [
            
            "CRUDE(WTI)",
            "CRUDE(BRENT)"        ]
        return arr
        
    }
    
    
    static func getWeeklyPx() -> [String]{
        let arr = [
            
            "PX",
            "PTA","MEG"        ]
        return arr
        
    }
    
    
    
    
    static  func setUpLablel(label : UILabel){
        label.backgroundColor = .clear
        label.layer.cornerRadius = 5
        label.layer.borderWidth = 1
        label.layer.borderColor =
            AppUtils.lightColor
        label.layer.backgroundColor = UIColor.white.cgColor
        
    }
    
    
    static   func setUpButton(btn : UIButton){
        btn.backgroundColor = .clear
        btn.layer.cornerRadius = 5
        btn.layer.borderWidth = 1
        btn.layer.borderColor =
            AppUtils.lightColor
        btn.layer.backgroundColor = UIColor.white.cgColor
        
    }
    
    
    static   func setUpChartLittle(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = false
        chart.setScaleEnabled(false)
        chart.pinchZoomEnabled = false
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = false
        
        let xAxis = chart.xAxis
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        chart.rightAxis.enabled = false
        chart.xAxis.enabled = false
        chart.leftAxis.enabled = false
        chart.legend.enabled = false
        
        
    }
    static  func setDataLittle(_ values: [ChartDataEntry],color : String ,chart : LineChartView) {
        
        
        
        
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        if(color == "red"){
            set1.setColor(UIColor(cgColor: AppUtils.redColor))
        }
        else if(color == "green"){
            set1.setColor(UIColor(cgColor: AppUtils.greenColor))
        }else{
            set1.setColor(UIColor(cgColor: AppUtils.yellowColor))
        }
        
        set1.lineWidth = 1
        //        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        
        
        let data = LineChartData(dataSet: set1)
        
        
        
        
        
        chart.data = data
        
        
        //        chart.xAxis.valueFormatter = ChartFormatter()
        
    }
    
    
    
    
}


//SaleModel

public class ChartFormatter: NSObject, IAxisValueFormatter {
    
    var val = [GraphDtoWeek]()
    
    init(v: [GraphDtoWeek]  ) {
        val = v
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        var i = 0
        
        if value < 0{
            i = 0
            
        }else{
            i = Int(value)
        }
        
        
        if i >= val.count
        {
            return ""
        }
        return AppUtils.formateGraph(date: val[Int(value)].date!)
    }
    
}
public class SaleFormatter: NSObject, IAxisValueFormatter {
    
    var val = [SaleModel]()
    
    init(v: [SaleModel]  ) {
        val = v
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        
        
        var i = 0
        
        if value < 0{
            i = 0
            
        }else{
            i = Int(value)
        }
        
        
        if i >= val.count
        {
            return ""
        }
        
        
        return AppUtils.saleGraph(date: val[Int(value)].BillingDate)
    }
    
}
public class SaleFormatter2: NSObject, IAxisValueFormatter {
    
    var val = [SaleModel]()
    
    init(v: [SaleModel]  ) {
        val = v
    }
    
    public func stringForValue( _ value: Double, axis: AxisBase?) -> String {
        
        
        var i = 0
        
        if value < 0{
            i = 0
            
        }else{
            i = Int(value)
        }
        
        
        if i >= val.count
        {
            return ""
        }
        else{
            return  val[i].BillingDate
        }
    }
    
}
