//
//  NetworkManager.swift
//  ZApp ios
//
//  Created by Muhammad Bilal Aslam on 24/01/2019.
//  Copyright © 2019 Muhammad Bilal Aslam. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import SwiftyJSON
class NetworkManager {
   static var AM : SessionManager?
     static func initi(){
       let configuration = URLSessionConfiguration.default
       configuration.timeoutIntervalForRequest = 60
       configuration.timeoutIntervalForResource = 30
       AM = Alamofire.SessionManager(configuration: configuration)
     }
    
    
    static func login(username : String,password : String,completion: @escaping (_ result: Bool)->(),failure: @escaping (_ error: String)->())  {
        
        let parameters = ["email" : username,"password" : password
            
        ]
        print(parameters)
        let url = "\(AppUtils.returnBaseUrl())Login"
        print(url)
        NetworkManager.AM?.request(url, method: .post, parameters: parameters, headers: AppUtils.loginHeader).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    
                    let dto = LoginDto(fromJson: j)
                    
//                    if(dto.user.status == "inactive"){
//                        failure("Your account is not active yet")
//
//                    }
//                    else{
                        BDefaults.instance.setAppState(value: BDefaults.LoggedIN)
                        BDefaults.instance.saveUser(user: dto.user!)
                        completion(true)
//                    }
                }else{
                    failure("Wrong email or password")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    static func forgotPassword(email : String,completion: @escaping (_ result: Bool)->(),failure: @escaping (_ error: String)->())  {
        
        let parameters = ["email" : email]
        print(parameters)
        let url = "\(AppUtils.returnBaseUrl())forgetPassword"
        print(url)
        NetworkManager.AM?.request(url, method: .post, parameters: parameters, headers: AppUtils.loginHeader).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                BDefaults.instance.setAppState(value: BDefaults.LoggedIN)
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    
                    
                    
                    
                    completion(true)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    
    
    
    static func getEvents(completion: @escaping (_ result: EventsDto)->(),failure: @escaping (_ error: String)->())  {
        
        
        let url = "\(AppUtils.returnBaseUrl())hse"
        print(url)
        print(AppUtils.headers)
        NetworkManager.AM?.request(url, method: .get, parameters: nil, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                BDefaults.instance.setAppState(value: BDefaults.LoggedIN)
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = EventsDto(fromJson: j)
                    
                    
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    static func getDashboardDaily(completion: @escaping (_ result: DashboardDailyDto)->(),failure: @escaping (_ error: String)->())  {
        
        
        var h = AppUtils.headers
        h["userid"] = "\(BDefaults.instance.getUsers()[0].userId!)"
        
        
        
        let url = "\(AppUtils.returnBaseUrl())dashboard"
        print(url)
        print(h)
        
        NetworkManager.AM?.request(url, method: .get, parameters: nil, headers: h).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = DashboardDailyDto(fromJson: j)
                    
                    BDefaults.instance.saveDashboardDail(user: dto)
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    static func getDashboardWeekly(completion: @escaping (_ result: GraphDto)->(),failure: @escaping (_ error: String)->())  {
        
        
        let url = "\(AppUtils.returnBaseUrl())weekly"
        print(url)
        print(AppUtils.headers)
        NetworkManager.AM?.request(url, method: .get, parameters: nil, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = GraphDto(fromJson: j)
                    
                    
                    
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    
                    
                    
                    
                    dto.week.sort{dateFormatter.date(from: $0.date!)! < dateFormatter.date(from: $1.date!)!
                        
                    }
                    
                    
                    
                    BDefaults.instance.saveGraphWeekly(user: dto)
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    static func getDashboardMonthly(completion: @escaping (_ result: GraphDto)->(),failure: @escaping (_ error: String)->())  {
        
        
        let url = "\(AppUtils.returnBaseUrl())monthly"
        print(url)
        print(AppUtils.headers)
        NetworkManager.AM?.request(url, method: .get, parameters: nil, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = GraphDto(fromJson: j)
                    
                    
                    
                    var dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "yyyy-MM-dd"
                    
                    
                    
                    
                    
                    dto.week.sort{dateFormatter.date(from: $0.date!)! < dateFormatter.date(from: $1.date!)!
                        
                    }
                    
                    
                    
                    
                    
                    BDefaults.instance.saveGraphMonth(user: dto)
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    static func getProducts(completion: @escaping (_ result: ProductDto)->(),failure: @escaping (_ error: String)->())  {
        
        
        let url = "\(AppUtils.returnBaseUrl())products"
        print(url)
        print(AppUtils.headers)
        NetworkManager.AM?.request(url, method: .get, parameters: nil, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = ProductDto(fromJson: j)
                    
                    BDefaults.instance.saveProducts(user: dto.products)
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    static func getSpecialReports(completion: @escaping (_ result: SpecialDto)->(),failure: @escaping (_ error: String)->())  {
        
        let body = ["user_id":"\(BDefaults.instance.getUsers()[0].userId!)"]
        
        let url = "\(AppUtils.returnBaseUrl())specialReports"
        print(url)
        print(AppUtils.headers)
        print(body)
        
        
        
        NetworkManager.AM?.request(url, method: .post, parameters: body, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = SpecialDto(fromJson: j)
                    
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    static func getHomePageVideo(completion: @escaping (_ result: HomePageDto)->(),failure: @escaping (_ error: String)->())  {
        
        let body = ["user_id":"\(BDefaults.instance.getUsers()[0].userId!)"]
        
        let url = "\(AppUtils.returnBaseUrl())video"
        print(url)
        print(AppUtils.headers)
        print(body)
        
        
        
        NetworkManager.AM?.request(url, method: .get, parameters: body, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    let dto = HomePageDto(fromJson: j)
                    BDefaults.instance.saveHomeVideo(user: dto)
                    completion(dto)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    static   func  fetchNewsData () {
        
        NetworkManager.AM?.request("\(AppUtils.BASE_URL)/news",  method: .get
            , parameters: nil, encoding: URLEncoding.default, headers: AppUtils.headers).responseJSON { (response) in
                print(response)
                
                switch response.result {
                    
                case .success(_):
                    print(response)
                    let jsonResponse = JSON(response.result.value!)
                    var newsmodel = NewsModel(response:jsonResponse)
                    if newsmodel.status == "True"{
                        
                        
                        var dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        
                        
                        
                        
                        newsmodel.news.sort{dateFormatter.date(from: $0.createdAt!)! > dateFormatter.date(from: $1.createdAt!)!
                            
                        }
                        
                        
                        
                        BDefaults.instance.saveAllNews(array: newsmodel.news)
                        
                        
                        
                        
                        
                    } else {
                        
                        
                        
                    }
                case .failure(let error):
                    break
                    
                }
                
                
        }
    }
    
    
    
    
    
    
    static func sendQuery(index : Int,completion: @escaping (_ result: Bool)->(),failure: @escaping (_ error: String)->())  {
        
        let body = ["user_id":"\(BDefaults.instance.getUsers()[0].userId!)","query":"\(index)"]
        
        let url = "\(AppUtils.returnBaseUrl())sendQuery"
        print(url)
        print(AppUtils.headers)
        print(body)
        
        
        
        NetworkManager.AM?.request(url, method: .post, parameters: body, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    
                    completion(true)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    static func changePassword(query : String,completion: @escaping (_ result: Bool)->(),failure: @escaping (_ error: String)->())  {
        
        let body = ["user_id":"\(BDefaults.instance.getUsers()[0].userId!)","password":query]
        
        let url = "\(AppUtils.returnBaseUrl())userChangePassword"
        print(url)
        print(AppUtils.headers)
        print(body)
        
        
        
        NetworkManager.AM?.request(url, method: .post, parameters: body, headers: AppUtils.headers).responseJSON { response in
            if response.result.isSuccess{
                print("Success \(response.result.value!)")
                
                
                
                let j : JSON = JSON(response.result.value!)
                
                if(j["status"].stringValue != "False"){
                    
                    completion(true)
                }else{
                    failure("Network Error")
                    
                }
                
            }else{
                print(response.result.error)
                print("some error")
                failure("Network Error")
            }
            
            
            
            
        }
        
        
        
        
        
    }
    
    
    
    //    static func sendOrder(order : Order,completion: @escaping (_ result: Bool)->(),failure: @escaping (_ error: String)->())  {
    //
    //
    //        print(order.returnParams())
    //        let url = "\(AppUtils.returnBaseUrl())order"
    //
    //        NetworkManager.AM?.request(url, method: .post, parameters: order.returnParams(),encoding: JSONEncoding.default, headers: AppUtils.getHeader()).responseJSON {
    //            response in
    //            if response.result.isSuccess{
    //                print("Success \(response.result.value!)")
    //
    //
    //
    //                let j : JSON = JSON(response.result.value!)
    //
    //                if !j["error"].bool!{ WbmDefaults.instance.setString(key: "orderId", value: j["orderId"].string!)
    //                }
    //                completion( !j["error"].bool!)
    //
    //            }else{
    //                print("some error")
    //                failure("Network Error")
    //            }
    //
    //
    //
    //
    //        }
    //
    //
    //    }
    //
    //
    
}
