//
//  StringsUtils.swift
//  ZApp ios
//
//  Created by Muhammad Bilal Aslam on 01/02/2019.
//  Copyright © 2019 Muhammad Bilal Aslam. All rights reserved.
//

import Foundation
class StringsUtils {
    
    static let deliveryTimeheading = NSLocalizedString("Select Delivery Time", comment: "")
    static let logoutHeading = NSLocalizedString("Do you want really Logout", comment: "")
    static let deliveryTimeDetail = NSLocalizedString("Select time when you want you should recieve your order", comment: "")
    static let oneHour = NSLocalizedString("1 Hour", comment: "")
    static let twoHour = NSLocalizedString("2 Hours", comment: "")
    static let threeHour = NSLocalizedString("3 Hours", comment: "")
    static let oneDay = NSLocalizedString("1 Day Time", comment: "")
    static let twoDay = NSLocalizedString("2 Day Time", comment: "")
    static let threeDay = NSLocalizedString("3 Day Time", comment: "")
    static let cancel = NSLocalizedString("Cancel", comment: "")
    static let yes = NSLocalizedString("Yes", comment: "")
    
    static let choosePhoto = NSLocalizedString("Choose Photo", comment: "")
    static let cancelOrder = NSLocalizedString("Cancel Order", comment: "")
    static let shareLocation = NSLocalizedString("Share Location", comment: "")
    static let fileAcomplain = NSLocalizedString("File A Complain", comment: "")
    static let changeCourier = NSLocalizedString("Change Courier", comment: "")
    static let completeOrder = NSLocalizedString("Complete Order", comment: "")
    static let startOrder = NSLocalizedString("Start Order", comment: "")
    static let reachedOrder = NSLocalizedString("Reached Order", comment: "")
    static let arrivedOrder = NSLocalizedString("Arrived Order", comment: "")
    
}
