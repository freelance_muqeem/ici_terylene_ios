//
//  BDefaults.swift
//  WbmLogisticsUser
//
//  Created by Muhammad Bilal Aslam on 05/12/2018.
//  Copyright © 2018 Muhammad Bilal Aslam. All rights reserved.
//

import Foundation

class BDefaults{
    
    static let ClientService = "AppSkNoCkQr-#$%"
    static let AuthKey = "ItIsfoRItIsfoR^S@fTy!@).(@^S@fTy!@).(@"
    static var addMemberFlag = false
    static let instance = BDefaults()
    static let defaults = UserDefaults.standard
    static let VEHICLE_TYPE = "VEHICLE_TYPE"
    static let AppState = "AppState"
    static let UserRequest = "User_Request"
    static let SendPackage = "Send_Package"
    static let START = "START"
    static let OTP = "OTP"
    static let RequestAccepted = "Request_Accepted"
    static let DriverArrived = "DRIVER_ARRIVED"
    static let LoggedIN = "LoggedIN"
    static let RideStarted = "RIDE_STARTED"
    static let RideCompleted = "RIDE_COMPLETE"
    
    func setAppState( value : String )
    {
        BDefaults.defaults.set(value,forKey: BDefaults.AppState)
    }
    
    func getAppState()-> String {
        return BDefaults.defaults.string(forKey: BDefaults.AppState) ?? ""
    }
    func setString( key : String,value : String )
    {
        BDefaults.defaults.set(value,forKey: key)
    }
    
    func getString(key : String) -> String {
        return BDefaults.defaults.string(forKey: key) ?? ""
    }
    
    func setInt( key : String,value : Int )
    {
        BDefaults.defaults.set(value,forKey: key)
    }
    func getInt(key : String) -> Int {
        return BDefaults.defaults.integer(forKey: key) ?? 0
    }
    func setDouble( key : String,value : Double )
    {
        BDefaults.defaults.set(value,forKey: key)
    }
    func getDouble(key : String) -> Double {
        return BDefaults.defaults.double(forKey: key) ?? 0.0
    }
    func setBool( key : String,value : Bool )
    {
        BDefaults.defaults.set(value,forKey: key)
    }
    func getBool(key : String) -> Bool {
        return BDefaults.defaults.bool(forKey: key) ?? false
    }
    func setDate( key : String,value : Date )
    {
        BDefaults.defaults.set(value,forKey: key)
    }
    func getDate(key : String) -> Date {
        return BDefaults.defaults.object(forKey: key) as! Date
    }
    
    //    func setLocation( key : String,value : CLLocationCoordinate2D )
    //    {
    //        BDefaults.defaults.set(value,forKey: key)
    //    }
    //
    //    func getLocation(key : String) -> CLLocationCoordinate2D {
    //        return BDefaults.defaults.object(forKey: key) as! CLLocationCoordinate2D
    //    }
    
    func setRecentRequest( value : String )
    {
        BDefaults.defaults.set(value,forKey: "RecentRequest")
    }
    func getRecentRequest() -> String {
        return BDefaults.defaults.string(forKey: "RecentRequest") ?? ""
    }
    func setFireBaseToken( value : String )
    {
        BDefaults.defaults.set(value,forKey: "FireBaseToken")
    }
    func getFireBaseToken() -> String {
        return BDefaults.defaults.string(forKey: "FireBaseToken") ?? ""
    }
    
    func setGroupListTimeStamp()
    {
        BDefaults.defaults.set(getCurrentTimeStamp(), forKey: "GroupListTimeStamp")
    }
    func getGroupListTimeStamp() -> String {
        return BDefaults.defaults.string(forKey: "GroupListTimeStamp") ?? "0000-00-00 00:00:00"
    }
    
    func getCurrentTimeStamp() -> String {
        
        let date = Date()
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        print(dateFormatterGet.string(from: date))
        return dateFormatterGet.string(from: date)
        
    }
    
    
    func getFormattedTime(millies : Int) -> String{
        let calendar = Calendar.current
        let time =  "h:mm aa"
        let dateTimeFormatString = "EEEE, MMMM d, h:mm aa"
        let dateFormatterGet = DateFormatter()
        
        
        let date = Date(timeIntervalSince1970: (TimeInterval(millies/1000)))
        
        if calendar.isDateInYesterday(date) {
            dateFormatterGet.dateFormat = time
            
            return "Yesterday \(dateFormatterGet.string(from: date))" }
        else if calendar.isDateInToday(date) {
            dateFormatterGet.dateFormat = time
            
            return "Today \(dateFormatterGet.string(from: date))"
            
        }
        else{
            dateFormatterGet.dateFormat = dateTimeFormatString
            return "\(dateFormatterGet.string(from: date))"
        }
    }
    
    
    static func returnMillies() -> Int {
        return Int(Date().timeIntervalSince1970*1000)
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    
    func saveUser(user : LoginDtoUser){
        
        var array = [LoginDtoUser]()
        array.append(user)
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "persons")
        BDefaults.defaults.synchronize()
    }
    
    func getUsers() -> [LoginDtoUser] {
        let array : [LoginDtoUser]
        array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "persons") as! NSData) as Data) as! [LoginDtoUser]
        return array
        
    }
    
    
    
    
    func saveAllNews(array :  [News]){
        
        
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "saveAllNews")
        BDefaults.defaults.synchronize()
    }
    
    func getAllNews() -> [News] {
        var array = [News]()
        if(BDefaults.defaults.object(forKey: "saveAllNews") != nil){
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "saveAllNews") as! NSData) as Data) as! [News]
        }
        return array
        
    }
    
    
    
    
    func saveNews(user : News){
        
        var array = [News]()
        array.append(user)
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "newsDetail")
        BDefaults.defaults.synchronize()
    }
    
    func getNews() -> [News] {
        let array : [News]
        array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "newsDetail") as! NSData) as Data) as! [News]
        return array
        
    }
    
    
    
    
    func saveHomeVideo(user : HomePageDto){
        
        var array = [HomePageDto]()
        array.append(user)
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "HomePageDto")
        BDefaults.defaults.synchronize()
    }
    
    func getHomeVideo() -> [HomePageDto] {
        
        var array = [HomePageDto]()
        
        if(BDefaults.defaults.object(forKey: "HomePageDto") != nil){
            
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "HomePageDto") as! NSData) as Data) as! [HomePageDto]
            
        }
        return array
        
        
        
        
    }
    
    
    
    
    func saveEvents(array : [EventsDtoHse]){
        
        
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "saveAllEvents")
        BDefaults.defaults.synchronize()
    }
    
    func getAllEvents() -> [EventsDtoHse] {
        var array = [EventsDtoHse]()
        if(BDefaults.defaults.object(forKey: "saveAllEvents") != nil){
            
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "saveAllEvents") as! NSData) as Data) as! [EventsDtoHse]
        }
        return array
        
    }
    
    
    func saveEvents(user : EventsDtoHse){
        
        var array = [EventsDtoHse]()
        array.append(user)
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "EventsDtoHse")
        BDefaults.defaults.synchronize()
    }
    
    func getEvents() -> [EventsDtoHse] {
        let array : [EventsDtoHse]
        array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "EventsDtoHse") as! NSData) as Data) as! [EventsDtoHse]
        return array
        
    }
    
    func saveDashboardDail(user : DashboardDailyDto){
        
        var array = [DashboardDailyDto]()
        array.append(user)
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "DashboardDailyDto")
        BDefaults.defaults.synchronize()
    }
    
    func getDashboardDaily() -> [DashboardDailyDto] {
        var array = [DashboardDailyDto]()
        
        if(BDefaults.defaults.object(forKey: "DashboardDailyDto") != nil){
            
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "DashboardDailyDto") as! NSData) as Data) as! [DashboardDailyDto]
            
        }
        return array
        
    }
    
    
    func saveGraphWeekly(user : GraphDto){
        
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: user.week), forKey: "saveGraphWeekly")
        BDefaults.defaults.synchronize()
    }
    
    func getGraphWeekly() -> [GraphDtoWeek] {
        var array = [GraphDtoWeek]()
        
        if(BDefaults.defaults.object(forKey: "saveGraphWeekly") != nil){
            
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "saveGraphWeekly") as! NSData) as Data) as! [GraphDtoWeek]
            
            //            var dateFormatter = DateFormatter()
            //            dateFormatter.dateFormat = "yyyy-MM-dd"
            //
            //            array.sort{dateFormatter.date(from: $0.date!)! > dateFormatter.date(from: $1.date!)!
            //
            //
            //            }
            
        }
        return array
        
    }
    func saveGraphMonth(user : GraphDto){
        
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: user.week), forKey: "saveGraphMonth")
        BDefaults.defaults.synchronize()
    }
    
    func getGraphMonth() -> [GraphDtoWeek] {
        var array = [GraphDtoWeek]()
        
        if(BDefaults.defaults.object(forKey: "saveGraphMonth") != nil){
            
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "saveGraphMonth") as! NSData) as Data) as! [GraphDtoWeek]
            //            var dateFormatter = DateFormatter()
            //            dateFormatter.dateFormat = "yyyy-MM-dd"
            //
            //            array.sort{dateFormatter.date(from: $0.date!)! > dateFormatter.date(from: $1.date!)!
            //            }
            
        }
        
        
        
        return array
        
    }
    
    func saveProducts(user : [ProductDtoProduct]){
        
        BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: user), forKey: "saveProducts")
        BDefaults.defaults.synchronize()
    }
    
    func getProducts() -> [ProductDtoProduct] {
        var array = [ProductDtoProduct]()
        
        if(BDefaults.defaults.object(forKey: "saveProducts") != nil){
            
            array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "saveProducts") as! NSData) as Data) as! [ProductDtoProduct]
            
        }
        return array
        
    }
    
    //
    //   func saveGroups(array : [GroupFcm]){
    //    BDefaults.defaults.setValue(NSKeyedArchiver.archivedData(withRootObject: array), forKey: "persons")
    //        BDefaults.defaults.synchronize()
    //    }
    //
    //   func getGroupFcms() -> [GroupFcm] {
    //        let array : [GroupFcm]
    //    array = NSKeyedUnarchiver.unarchiveObject(with: (BDefaults.defaults.object(forKey: "persons") as! NSData) as Data) as! [GroupFcm]
    //    return array
    //
    //    }
    //
    
    
    
    //
    //    static func setUser(UserArg:User){
    //        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("User.plist")
    //
    //
    //        let encoder = PropertyListEncoder()
    //        do{
    //            try  encoder.encode(UserArg).write(to: filePath!)
    //        }catch{
    //        }
    //
    //    }
    //
    //
    //
    //   static func getUser() -> User {
    //
    //        var UserArg:User?
    //         let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("User.plist")
    //        if let data = try? Data(contentsOf: filePath!)
    //        {
    //            let decoder = PropertyListDecoder()
    //            UserArg = try! decoder.decode(User.self,from: data)
    //
    //        }
    //        return UserArg!;
    //    }
    //
    //
    //    static func deleteUser(){
    //        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("User.plist")
    //
    //        let UserArg = User()
    //        let encoder = PropertyListEncoder()
    //        do{
    //            try  encoder.encode(UserArg).write(to: filePath!)
    //        }catch{
    //        }
    //
    //    }
    //
    //    /////////////for list add multiple records to user defaults User
    //
    //
    //    static func getUserList() -> [User] {
    //
    //        var UserArgs = [User]()
    //        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("User.plist")
    //        if let data = try? Data(contentsOf: filePath!)
    //        {
    //            let decoder = PropertyListDecoder()
    //            UserArgs = try! decoder.decode([User].self,from: data)
    //
    //        }
    //        return UserArgs;
    //    }
    //
    //
    //
    //    static func addUserToList(UserArgs:User){
    //        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("user.plist")
    //        var users = getUserList()
    //        users.append(UserArgs)
    //        let encoder = PropertyListEncoder()
    //        do{
    //            try  encoder.encode(users).write(to: filePath!)
    //        }catch{
    //        }
    //
    //    }
    //    static func deleteUserFromList(UserArgs:User){
    //        let filePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("user.plist")
    //        var users = getUserList()
    //        var usersCount = users.count
    //
    //
    //        for var a in 0..<users.count
    //        {
    //            let userTemp = users[a]
    //            if userTemp.email == UserArgs.email
    //            {
    //                users.remove(at: a)
    //                break
    //            }
    //        }
    //
    //            let encoder = PropertyListEncoder()
    //        do{
    //            try  encoder.encode(users).write(to: filePath!)
    //        }catch{
    //        }
    //
    //    }
    //
    
}
