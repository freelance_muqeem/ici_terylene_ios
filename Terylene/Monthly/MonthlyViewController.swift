//
//  MonthlyViewController.swift
//  Terylene
//
//  Created by Ali Ahsan on 3/25/19.
//  Copyright © 2019 Half Tech. All rights reserved.
//

import UIKit
import DropDown
import Charts

class MonthlyViewController: UIViewController {
    
    @IBOutlet weak var usdButton: UIButton!
    
    @IBOutlet weak var usdChart: LineChartView!
    
    @IBOutlet weak var crudeButton: UIButton!
    @IBOutlet weak var crudeChartt: LineChartView!
    
    @IBOutlet weak var pxButton: UIButton!
    
    @IBOutlet weak var pxChart: LineChartView!
    
    @IBOutlet weak var iciButton: UIButton!
    @IBOutlet weak var iciChart: LineChartView!
    
    @IBOutlet weak var cottonButton: UIButton!
    
    @IBOutlet weak var cottonChart: LineChartView!
    
    @IBOutlet weak var yarnButton: UIButton!
    
    @IBOutlet weak var yarnChart: LineChartView!
    
    let dropDown = DropDown()
    let dropDown2 = DropDown()
    let dropDown3 = DropDown()
    
    let dropDown4 = DropDown()
    let dropDown5 = DropDown()
    let dropDown6 = DropDown()
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
//        AppUtils.setUpButton(btn: usdButton)
//        AppUtils.setUpButton(btn: crudeButton)
//        AppUtils.setUpButton(btn: pxButton)
//        AppUtils.setUpButton(btn: iciButton)
//        AppUtils.setUpButton(btn: cottonButton)
//        AppUtils.setUpButton(btn: yarnButton)
        
        setDropDowns()
        DispatchQueue.global(qos: .background).async {
            DispatchQueue.main.async {
                
                if   self.usdChart.data == nil{
                
                self.setUsd()
                self.setWti()
                self.setPx()
                self.setIciSemi()
                self.setCotton()
                self.setYarn50()
                }
                
            }
            
        }
    }
    
    
    func setDropDowns(){
        
        
        
        setUsdDropDown()
        setCrudeDropDown()
        setPxDropDown()
        setIciDropDown()
        setCottonDropDown()
        setYarnRatesDropDown()
    }
    
    func setUsdDropDown(){
        
        dropDown.anchorView = usdButton
        dropDown.dataSource = AppUtils.getCurrencies()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.usdButton.setTitle("             \(AppUtils.getCurrencies()[index])", for: .normal)
            
            if index == 0{
                self.setUsd()
            }else if index == 1{
                self.setCny()
            }
            else if index == 2{
                self.setEur()
            }else if index == 3{
                self.setJpy()
            }else if index == 4{
                self.setGbp()
            }
            
        }
        
    }
    
    func setCrudeDropDown(){
        
        dropDown2.anchorView = crudeButton
        dropDown2.dataSource = AppUtils.getWeeklyCrude()
        dropDown2.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.crudeButton.setTitle("             \(AppUtils.getWeeklyCrude()[index])", for: .normal)
            
            if index == 0{
                self.setWti()
            }else if index == 1{
                self.setBrent()
            }
            
        }
        
    }
    
    func setPxDropDown(){
        
        dropDown3.anchorView = pxButton
        dropDown3.dataSource = AppUtils.getWeeklyPx()
        dropDown3.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.pxButton.setTitle("             \(AppUtils.getWeeklyPx()[index])", for: .normal)
            
            if index == 0{
                self.setPx()
            }else if index == 1{
                self.setPta()
            }else if index == 2{
                self.setMeg()
            }
            
        }
        
    }
    
    func setIciDropDown(){
        
        dropDown4.anchorView = iciButton
        dropDown4.dataSource = AppUtils.getSection4Graph()
        dropDown4.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.iciButton.setTitle("             \(AppUtils.getSection4Graph()[index])", for: .normal)
            
            if index == 0{
                self.setIciSemi()
            }else if index == 1{
                self.setChina()
            }else if index == 2{
                self.setImport()
            }
            
        }
        
    }
    
    func setCottonDropDown(){
        
        dropDown5.anchorView = cottonButton
        dropDown5.dataSource = AppUtils.getSection5Graph()
        dropDown5.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.cottonButton.setTitle("             \(AppUtils.getSection5Graph()[index])", for: .normal)
            
            if index == 0{
                self.setCotton()
            }else if index == 1{
                self.setNyfLbs()
            }else if index == 2{
                self.setNyfPkrMaund()
            }else if index == 3{
                self.setNyfPkrKg()
            }
            
        }
        
    }
    
    func setYarnRatesDropDown(){
        
        dropDown6.anchorView = yarnButton
        dropDown6.dataSource = AppUtils.getSection6Graph()
        dropDown6.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.yarnButton.setTitle("             \(AppUtils.getSection6Graph()[index])", for: .normal)
            
            if index == 0{
                self.setYarn50()
            }else if index == 1{
                self.setYarn38()
            }else if index == 2{
                self.setyarn36()
            }else if index == 3{
                self.setYarn30()
            }else if index == 4{
                self.setYarn26()
            }else if index == 5{
                self.setYarn20()
            }
            
        }
        
    }
    
    
    
    @IBAction func openUsd(_ sender: Any) {
        dropDown.show()
    }
    
    @IBAction func openCrude(_ sender: Any) {
        dropDown2.show()
    }
    
    @IBAction func openPx(_ sender: Any) {
        dropDown3.show()
    }
    
    @IBAction func openICI(_ sender: Any) {
        dropDown4.show()
    }
    
    @IBAction func openCotton(_ sender: Any) {
        dropDown5.show()
    }
    
    @IBAction func openYarn(_ sender: Any) {
        dropDown6.show()
    }
    
    
    func setUpButtons(btn : UIButton){
        btn.backgroundColor = .clear
        btn.layer.cornerRadius = 5
        btn.layer.borderWidth = 1
        btn.layer.borderColor = UIColor.blue.cgColor
        btn.layer.backgroundColor = UIColor.white.cgColor
        
        
    }
    
    
    func setUsd(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].usd)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: usdChart)
        setData(values, chart: usdChart)
        
    }
    func setCny(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].cny)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: usdChart)
        setData(values, chart: usdChart)
        
    }
    func setEur(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].eur)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: usdChart)
        setData(values, chart: usdChart)
        
    }
    
    func setJpy(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].jpy)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: usdChart)
        setData(values, chart: usdChart)
        
    }
    func setGbp(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].gbp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: usdChart)
        setData(values, chart: usdChart)
        
    }
    
    func setWti(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].crude)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: crudeChartt)
        setData(values, chart: crudeChartt)
        
    }
    func setBrent(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].brent)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: crudeChartt)
        setData(values, chart: crudeChartt)
        
    }
    
    func setPx(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].px)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: pxChart)
        setData(values, chart: pxChart)
        
    }
    
    func setPta(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].pta)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: pxChart)
        setData(values, chart: pxChart)
        
    }
    
    func setMeg(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].meg)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: pxChart)
        setData(values, chart: pxChart)
        
    }
    
    func setIciSemi(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].iciSemi)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: iciChart)
        setData(values, chart: iciChart)
        
    }
    func setChina(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].chinaPsfPrice)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: iciChart)
        setData(values, chart: iciChart)
        
    }
    
    func setImport(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].importOffer)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: iciChart)
        setData(values, chart: iciChart)
        
    }
    
    func setCotton(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].cottonMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: cottonChart)
        setData(values, chart: cottonChart)
        
    }
    
    func setNyfLbs(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].nyf)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: cottonChart)
        setData(values, chart: cottonChart)
        
    }
    
    func setNyfPkrMaund(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].nyfMaund)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: cottonChart)
        setData(values, chart: cottonChart)
        
    }
    
    func setNyfPkrKg(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].nyfPkr)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: cottonChart)
        setData(values, chart: cottonChart)
        
    }
    
    func setYarn50(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].yarn50Pp)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: yarnChart)
        setData(values, chart: yarnChart)
        
    }
    func setYarn38(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].yarn38Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: yarnChart)
        setData(values, chart: yarnChart)
        
    }
    func setyarn36(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].yarn36Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: yarnChart)
        setData(values, chart: yarnChart)
        
    }
    
    
    func setYarn30(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].yarn30Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: yarnChart)
        setData(values, chart: yarnChart)
        
    }
    
    func setYarn26(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].yarn26Pc)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: yarnChart)
        setData(values, chart: yarnChart)
        
    }
    
    func setYarn20(){
        let values = (0..<BDefaults.instance.getGraphMonth().count).map { (i) -> ChartDataEntry in
            let val = Double(BDefaults.instance.getGraphMonth()[i].yarn20S)
            return ChartDataEntry(x: Double(i), y: val, icon: #imageLiteral(resourceName: "ic_back"))
        }
        setUpChart(chart: yarnChart)
        setData(values, chart: yarnChart)
        
    }
    
    
    func setUpChart(chart : LineChartView){
        
        chart.chartDescription?.enabled = false
        chart.dragEnabled = true
        chart.setScaleEnabled(true)
        chart.pinchZoomEnabled = true
        
        let leftAxis = chart.leftAxis
        leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = false
        leftAxis.xOffset = 20
        
        
        
        
        
        let xAxis = chart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = false
        xAxis.drawGridLinesEnabled = false
        
        //        xAxis.xOffset = 40
        let rightAxis = chart.xAxis
        rightAxis.drawAxisLineEnabled = false
        rightAxis.drawGridLinesEnabled = false
        
        chart.rightAxis.enabled = false
        chart.legend.form = .circle
        chart.legend.enabled = false
        
        
        
        
        let marker = XYMarkerView(color: #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1),
                                  font: .systemFont(ofSize: 12),
                                  textColor: .black,
                                  insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                  xAxisValueFormatter: chart.xAxis.valueFormatter!)
        marker.chartView = chart
        marker.minimumSize = CGSize(width: 80, height: 20)
        chart.marker = marker
        
        
        
        
        
        chart.animate(xAxisDuration: 1)
        
    }
    func setData(_ values: [ChartDataEntry],chart : LineChartView) {
        
        let set1 = LineChartDataSet(entries: values, label: "")
        
        //        let set1 = LineChartDataSet(values)
        
        set1.drawIconsEnabled = false
        set1.mode = .linear
        
        set1.setColor(UIColor(cgColor: AppUtils.lightColor))
        set1.lineWidth = 2.5
        set1.setCircleColor(UIColor(cgColor: AppUtils.lightColor))
        set1.circleRadius = 0
        set1.circleHoleRadius = 0
        set1.fillColor = UIColor.white
        
        set1.drawValuesEnabled = false
        set1.valueFont = .systemFont(ofSize: 10)
        set1.valueTextColor = UIColor.blue
        
        
        let gradientColors = [ChartColorTemplates.colorFromString("#0042c5f4").cgColor,
                              ChartColorTemplates.colorFromString("#42c5f4").cgColor]
        let gradient = CGGradient(colorsSpace: nil, colors: gradientColors as CFArray, locations: nil)!
        
        set1.fillAlpha = 1
        set1.fill = Fill(linearGradient: gradient, angle: 90) //.linearGradient(gradient, angle: 90)
        set1.drawFilledEnabled = true
        
        
        let data = LineChartData(dataSet: set1)
        
        
        
        
        
        chart.data = data

        
//        chart.xAxis.valueFormatter = ChartFormatter()
        
    }
    
    
}


